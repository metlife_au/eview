/**
 * 
 */
package com.metlife.au.eViewGateway.filters;

import com.netflix.zuul.ZuulFilter;

/**
 * @author 672381
 *
 */
public class PostFilter extends ZuulFilter {

	@Override
	public String filterType() {
		return "post";
	}

	@Override
	public int filterOrder() {
		return 1;
	}

	@Override
	public boolean shouldFilter() {
		return true;
	}

	@Override
	public Object run() {

		return null;
	}
}
