/**
 * 
 */
package com.metlife.au.eViewGateway.controller;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.metlife.au.eViewGateway.entity.LoginUser;

/**
 * @author 672381
 *
 */

@RestController
@RequestMapping("/api/v1")
public class GatewayController {
	
	public static final Logger logger = LoggerFactory.getLogger(GatewayController.class);
	
	@RequestMapping("/user")
	public ResponseEntity<LoginUser> getUser(Principal principal, HttpServletRequest request) {
		LoginUser user=new LoginUser();
		ResponseEntity<LoginUser> resEntity = new ResponseEntity<>(HttpStatus.NO_CONTENT);
		try {
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			user.setUserName(principal.getName());
			List<String> rolesList=new ArrayList<String>();
			List<String> fundList=new ArrayList<String>();
			List<String> menuList=new ArrayList<String>();
			if(auth!=null && auth.getAuthorities()!=null) {				
				for(GrantedAuthority role: auth.getAuthorities()) {
					if(role.getAuthority()!=null) {
						if(role.getAuthority().startsWith("ROLE_FUND_")) {
							fundList.add(role.getAuthority().replace("ROLE_FUND_", ""));
						}else if(role.getAuthority().startsWith("ROLE_MENU_")) {
							menuList.add(role.getAuthority().replace("ROLE_MENU_", ""));
						}else if(role.getAuthority().startsWith("ROLE_PASSWORD_")) {
							user.setPasswordInforce(role.getAuthority().replace("ROLE_PASSWORD_", ""));
						}else {
							rolesList.add(role.getAuthority().replace("ROLE_", ""));
						}
					}
				}
			}
			if(rolesList.size()>0) {
				user.setRoles(rolesList);
			}
			if(fundList.size()>0) {
				user.setFunds(fundList);
			}
			if(menuList.size()>0) {
				user.setMenus(menuList);
			}
		
			resEntity = new ResponseEntity<>(user,HttpStatus.OK);
		}catch(Exception ex) {
			resEntity = new ResponseEntity<>(user,HttpStatus.EXPECTATION_FAILED);
		}		
		logger.info("principal" + principal.getName());		
		return resEntity;
	}

}
