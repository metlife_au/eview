package com.metlife.au.eViewGateway.config;

import org.springframework.web.filter.CorsFilter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
import org.springframework.security.config.annotation.web.configurers.FormLoginConfigurer;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.ldap.DefaultSpringSecurityContextSource;
import org.springframework.security.ldap.userdetails.LdapAuthoritiesPopulator;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

/**
 * Provides AuthenticationManager with some simple in-memory users, used to
 * authenticate users for {@link AuthorizationServer}
 *
 * In a real application, this should be replaced by an
 * {@link AbstractLdapAuthenticationProvider} or something similar
 *
 */
@EnableWebSecurity
@Configuration
public class AuthenticationProvider extends WebSecurityConfigurerAdapter {

	@Value("${auth.ldap.uri}")
	private String LDAP_URI;
	@Value("${auth.ldap.dn}")
	private String LDAP_DN;
	@Value("${auth.ldap.manager.id}")
	private String LDAP_MGR_ID;
	@Value("${auth.ldap.manager.password}")
	private String LDAP_MGR_PAS;

	public static Authentication getAuthentication() {
		return SecurityContextHolder.getContext().getAuthentication();
	}

	public void configure(AuthenticationManagerBuilder auth) throws Exception {
		
		/*  auth .inMemoryAuthentication()
		  .withUser("user").password("{noop}user$").roles("USER").and()
		  .withUser("user1").password("{noop}user$").roles("USER", "ADMIN");*/
		 

		auth.ldapAuthentication().contextSource().url(this.LDAP_URI).managerDn(this.LDAP_MGR_ID)
				.managerPassword(this.LDAP_MGR_PAS).and().userSearchBase(this.LDAP_DN).userSearchFilter("(uid={0})")
				.ldapAuthoritiesPopulator(ldapAuthoritiesPopulator()).passwordCompare()
				.passwordAttribute("userPassword");
	}

	@Bean
	public DefaultSpringSecurityContextSource contextSource() {
		return new DefaultSpringSecurityContextSource(this.LDAP_URI);
	}

	@Bean
	public LdapTemplate ldapTemplate() {
		return new LdapTemplate(contextSource());
	}

	@Bean
	public LdapAuthoritiesPopulator ldapAuthoritiesPopulator() {
		LdapAuthoritiesPopulator populator = new CustomAuthorization(contextSource(), "ou=Customer");

		return populator;
	}

	@Bean
	@Override
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}
	
	@Bean
	  public CorsFilter corsFilter()
	  {
	    UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
	    CorsConfiguration config = new CorsConfiguration();
	    config.setAllowCredentials(Boolean.valueOf(true));
	    config.addAllowedOrigin("*");
	    config.addAllowedHeader("*");
	    config.addAllowedMethod("*");
	    source.registerCorsConfiguration("/logout", config);
	    source.registerCorsConfiguration("/login", config);
	    source.registerCorsConfiguration("/oauth/authorize", config);
	    return new CorsFilter(source);
	  }
}