package com.metlife.au.eViewGateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.metlife.au.eViewGateway.filters.ErrorFilter;
import com.metlife.au.eViewGateway.filters.PostFilter;
import com.metlife.au.eViewGateway.filters.PreFilter;
import com.metlife.au.eViewGateway.filters.RouteFilter;

@SpringBootApplication
@EnableAspectJAutoProxy
@EnableZuulProxy
@RestController
@EnableEurekaClient
@CrossOrigin(origins = "*")
public class EViewGatewayApplication {

	@Bean
	public PreFilter preFilter() {
		return new PreFilter();
	}

	@Bean
	public PostFilter postFilter() {
		return new PostFilter();
	}

	@Bean
	public ErrorFilter errorFilter() {
		return new ErrorFilter();
	}

	@Bean
	public RouteFilter routeFilter() {
		return new RouteFilter();
	}

	@LoadBalanced 
    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }
	public static void main(String[] args) {
		SpringApplication.run(EViewGatewayApplication.class, args);
	}

}
