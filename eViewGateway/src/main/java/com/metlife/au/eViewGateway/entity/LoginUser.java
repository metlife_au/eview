/**
 * 
 */
package com.metlife.au.eViewGateway.entity;

import java.util.List;

/**
 * @author 672381
 *
 */
public class LoginUser {

private String userName;
	
	private List<String> roles;
	
	private List<String> funds;
	
	private List<String> menus;
	
	private String passwordInforce;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}	

	public String getPasswordInforce() {
		return passwordInforce;
	}

	public void setPasswordInforce(String passwordInforce) {
		this.passwordInforce = passwordInforce;
	}

	public List<String> getRoles() {
		return roles;
	}

	public void setRoles(List<String> roles) {
		this.roles = roles;
	}

	public List<String> getFunds() {
		return funds;
	}

	public void setFunds(List<String> funds) {
		this.funds = funds;
	}

	public List<String> getMenus() {
		return menus;
	}

	public void setMenus(List<String> menus) {
		this.menus = menus;
	}
}
