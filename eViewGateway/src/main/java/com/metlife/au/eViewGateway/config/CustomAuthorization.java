/**
 * 
 */
package com.metlife.au.eViewGateway.config;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.ldap.core.ContextSource;
import org.springframework.ldap.core.DirContextOperations;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.ldap.SpringSecurityLdapTemplate;
import org.springframework.security.ldap.userdetails.LdapAuthoritiesPopulator;
import org.springframework.util.Assert;

/**
 * @author 672381
 *
 */

public class CustomAuthorization implements LdapAuthoritiesPopulator {
	public static final Logger logger = LoggerFactory.getLogger(CustomAuthorization.class);
	@Value("${auth.ldap.dn}")
	private String LDAP_DN;
	private String groupSearchBase;

	private SpringSecurityLdapTemplate ldapTemplate;

	protected SpringSecurityLdapTemplate getLdapTemplate() {
		return this.ldapTemplate;
	}

	protected ContextSource getContextSource() {
		return getLdapTemplate().getContextSource();
	}

	protected String getGroupSearchBase() {
		return this.groupSearchBase;
	}

	public CustomAuthorization(ContextSource contextSource, String groupSearchBase) {
		Assert.notNull(contextSource, "contextSource must not be null");
		this.ldapTemplate = new SpringSecurityLdapTemplate(contextSource);
		this.groupSearchBase = groupSearchBase;

		if (groupSearchBase == null) {
			logger.info("groupSearchBase is null. No group search will be performed.");
		} else if (groupSearchBase.length() == 0) {
			logger.info("groupSearchBase is empty. Searches will be performed from the context source base");
		}
	}

	@Override
	public final Collection<GrantedAuthority> getGrantedAuthorities(DirContextOperations user, String username) {
		String userDn = user.getNameInNamespace();
		user.getStringAttribute(username);
		logger.info("Getting authorities for user " + userDn);
		List<GrantedAuthority> result = new ArrayList<GrantedAuthority>();
		String[] organizationalUnit = userDn.split(",");
		HashMap<String, String> map = new HashMap<>();
		// To DO few more changes to make the code editable at any point of time
		for (String s : organizationalUnit) {
			String Split[] = s.split("=");
			if (!map.containsKey("o")) {
				map.put(Split[0], Split[1]);
			}
		}
		if ((map.get("o").equalsIgnoreCase("affiliates")) || (map.get("o").equalsIgnoreCase("customers"))) {
			String[] groups = user.getStringAttributes("metrolename");
			String[] funds = user.getStringAttributes("FUNDLIST");
			String[] menus = user.getStringAttributes("groupMembership");
			String password = user.getStringAttribute("pwdForceChange");
			logger.info("Roles of user " + username + " is" + Arrays.toString(groups));
			result = new ArrayList<GrantedAuthority>(groups.length);
			Set<GrantedAuthority> authorities = new HashSet<GrantedAuthority>();
			for (String role : groups) {
				String company = user.getStringAttribute("Company");
				if (company != null && (role.replaceAll("\\s", "").equalsIgnoreCase("eserviceext")
						|| role.replaceAll("\\s", "").equalsIgnoreCase("indadministrator"))) {
					authorities.add(new SimpleGrantedAuthority(
							role.toLowerCase() + new StringBuilder().append(":").append(company)));
				} else {
					authorities.add(new SimpleGrantedAuthority(role.toLowerCase()));
				}

			}
			if(funds!=null) {
				for (String fund : funds) {
					authorities.add(new SimpleGrantedAuthority("FUND_"+fund));
				}
			}
			if(menus!=null) {
				for (String menu : menus) {
					authorities.add(new SimpleGrantedAuthority("MENU_"+menu));
				}
			}
			if(password!=null) {
				authorities.add(new SimpleGrantedAuthority("PASSWORD_"+password));
			}
			result.addAll(authorities);
		}
		logger.info("Final granted Authorities " + result);
		return result;
	}

	public Set<GrantedAuthority> getGroupMembershipRoles(String userDn, String username) {
		if (getGroupSearchBase() == null) {
			return new HashSet<GrantedAuthority>();
		}
		logger.info("getGroupMembershipRoles");
		Set<GrantedAuthority> authorities = new HashSet<GrantedAuthority>();
		logger.info("Searching for roles for user '" + username + "', DN = " + "'" + userDn + "', with filter "
				+ " in search base '" + getGroupSearchBase() + "'");
		Set<String> userRoles = getLdapTemplate().searchForSingleAttributeValues(LDAP_DN, "(uid={0})",
				new String[] { username }, "metrolename");
		logger.info("Roles from search: " + userRoles);
		SecurityContextHolder.getContext().getAuthentication();
		for (String role : userRoles) {
			authorities.add(new SimpleGrantedAuthority(role));
		}

		return authorities;
	}

}
