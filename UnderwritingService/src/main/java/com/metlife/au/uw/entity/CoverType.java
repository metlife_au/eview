/**
 * 
 */
package com.metlife.au.uw.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author 672381
 *
 */
@Entity
@Table(name = "COVER_TYPE", schema = "MTLWPS")
public class CoverType implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8242362092491702092L;

	@Id	
	@Column(name = "COVER_ID", nullable=false)
	private int coverId;
	
	@Column(name = "COVER_CODE", nullable=false)
	private String covercode;

	@Column(name = "COVER_NAME")
	private String coverName;

	public CoverType() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getCovercode() {
		return covercode;
	}

	public void setCovercode(String covercode) {
		this.covercode = covercode;
	}

	public String getCoverName() {
		return coverName;
	}

	public void setCoverName(String coverName) {
		this.coverName = coverName;
	}

	public int getCoverId() {
		return coverId;
	}

	public void setCoverId(int coverId) {
		this.coverId = coverId;
	}
	
	
}
