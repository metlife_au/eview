/**
 * 
 */
package com.metlife.au.uw.service;

import java.util.List;

import com.metlife.au.uw.dto.CategoryDto;
import com.metlife.au.uw.dto.CoverTypeDto;
import com.metlife.au.uw.dto.DocumentTypeDto;
import com.metlife.au.uw.dto.UWApplicantInfoDto;
import com.metlife.au.uw.exception.ApplicationException;

/**
 * @author 672381
 *
 */
public interface UnderwritingService {

	public UWApplicantInfoDto saveUnderwirrting(UWApplicantInfoDto uWApplicantInfo) throws ApplicationException;
	
	public List<CategoryDto> categoryDetails() throws ApplicationException;

	public List<DocumentTypeDto> documentTypeDetails() throws ApplicationException;
	
	public List<CoverTypeDto> coverTypeDetails() throws ApplicationException;
}
