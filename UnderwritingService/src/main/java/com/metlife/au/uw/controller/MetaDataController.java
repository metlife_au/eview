/**
 * 
 */
package com.metlife.au.uw.controller;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import com.metlife.au.uw.dto.CategoryDto;
import com.metlife.au.uw.dto.CoverTypeDto;
import com.metlife.au.uw.dto.DocumentTypeDto;
import com.metlife.au.uw.exception.ApplicationException;
import com.metlife.au.uw.service.UnderwritingService;
import com.metlife.au.uw.utils.MapperUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @author 672381
 *
 */
@Api(value = "/api/v1/application/metadata")
@RestController
@RequestMapping("/api/v1/application/metadata")
public class MetaDataController {

	private final Logger logger = LoggerFactory.getLogger(MetaDataController.class.getName());

	private UnderwritingService underwritingService;	

	/**
	 * @param eLodgementService
	 */
	@Autowired
	public MetaDataController(UnderwritingService underwritingService) {
		super();
		this.underwritingService = underwritingService;		
	}
	
	@ApiOperation(value = "/category", notes = "To retrive the category details")
	@GetMapping("/category")
	public ResponseEntity<List<CategoryDto>> categoryDetails() {
		logger.info("MetaDataController :  categoryDetails start");
		List<CategoryDto> categoryList = new ArrayList<CategoryDto>();
		ResponseEntity<List<CategoryDto>> resEntity = new ResponseEntity<>(HttpStatus.NO_CONTENT);
		try {			
			categoryList=underwritingService.categoryDetails();
			resEntity = new ResponseEntity<>(categoryList, HttpStatus.OK);
		} catch (ApplicationException ex) {
			resEntity = new ResponseEntity<>(categoryList, HttpStatus.EXPECTATION_FAILED);
			logger.info("MetaDataController :  categoryDetails catch block : " + ex.getMessage());
		} catch (Exception ex) {
			resEntity = new ResponseEntity<>(categoryList, HttpStatus.EXPECTATION_FAILED);
			logger.info("MetaDataController :  categoryDetails catch block : " + ex.getMessage());
		}
		logger.info("MetaDataController :  categoryDetails end");
		return resEntity;
	}
	
	@ApiOperation(value = "/documenttype", notes = "To retrive the document type details")
	@GetMapping("/documenttype")
	public ResponseEntity<List<DocumentTypeDto>> documentTypeDetails() {
		logger.info("MetaDataController :  documentTypeDetails start");
		List<DocumentTypeDto> docTypeList = new ArrayList<DocumentTypeDto>();
		ResponseEntity<List<DocumentTypeDto>> resEntity = new ResponseEntity<>(HttpStatus.NO_CONTENT);
		try {			
			docTypeList=underwritingService.documentTypeDetails();
			resEntity = new ResponseEntity<>(docTypeList, HttpStatus.OK);
		} catch (ApplicationException ex) {
			resEntity = new ResponseEntity<>(docTypeList, HttpStatus.EXPECTATION_FAILED);
			logger.info("MetaDataController :  documentTypeDetails catch block : " + ex.getMessage());
		} catch (Exception ex) {
			resEntity = new ResponseEntity<>(docTypeList, HttpStatus.EXPECTATION_FAILED);
			logger.info("MetaDataController :  documentTypeDetails catch block : " + ex.getMessage());
		}
		logger.info("MetaDataController :  documentTypeDetails end");
		return resEntity;
	}
	
	@ApiOperation(value = "/covertype", notes = "To retrive the cover type details")
	@GetMapping("/covertype")
	public ResponseEntity<List<CoverTypeDto>> coverTypeDetails() {
		logger.info("MetaDataController :  coverTypeDetails start");
		List<CoverTypeDto> coverTypeList = new ArrayList<CoverTypeDto>();
		ResponseEntity<List<CoverTypeDto>> resEntity = new ResponseEntity<>(HttpStatus.NO_CONTENT);
		try {			
			coverTypeList=underwritingService.coverTypeDetails();
			resEntity = new ResponseEntity<>(coverTypeList, HttpStatus.OK);
		} catch (ApplicationException ex) {
			resEntity = new ResponseEntity<>(coverTypeList, HttpStatus.EXPECTATION_FAILED);
			logger.info("MetaDataController :  coverTypeDetails catch block : " + ex.getMessage());
		} catch (Exception ex) {
			resEntity = new ResponseEntity<>(coverTypeList, HttpStatus.EXPECTATION_FAILED);
			logger.info("MetaDataController :  coverTypeDetails catch block : " + ex.getMessage());
		}
		logger.info("MetaDataController :  coverTypeDetails end");
		return resEntity;
	}
}
