/**
 * 
 */
package com.metlife.au.uw.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * @author 672381
 *
 */

@Entity
@Table(name = "ELODGECONTACTINFO", schema = "MTLWPS")
public class UWContactInfo implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3268007313893103376L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "contactInfo")
	@SequenceGenerator(name = "contactInfo", sequenceName = "MTLWPS.SEQ_ELODGECONTACTINFO", allocationSize = 1)
	@Column(name = "ID")
	private long contactId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ELODGEMENTAPPLICANTID", insertable = false, updatable = false)
	private UWApplicantInfo uWApplicantInfo;

	@Column(name = "ADDRESS1")
	private String address1;

	@Column(name = "ADDRESS2")
	private String address2;

	@Column(name = "SUBURB")
	private String suburb;

	@Column(name = "STATE")
	private String state;

	@Column(name = "POSTCODE")
	private Short postcode;

	@Column(name = "APPLICANTEMAIL")
	private String applicantEmail;

	@Column(name = "MOBILEPHONE")
	private long mobilePhone;

	@Column(name = "HOMEPHONE")
	private long homePhone;

	@Column(name = "ROLLOVERPENDING")
	private long rolloverPending;

	@Column(name = "INSURANCESCALE")
	private long insuranceScale;

	@Column(name = "PREFTIME")
	private long prefTime;

	/**
	 * 
	 */
	public UWContactInfo() {
		super();
		// TODO Auto-generated constructor stub
	}

	public long getContactId() {
		return contactId;
	}

	public void setContactId(long contactId) {
		this.contactId = contactId;
	}

	public UWApplicantInfo getuWApplicantInfo() {
		return uWApplicantInfo;
	}

	public void setuWApplicantInfo(UWApplicantInfo uWApplicantInfo) {
		this.uWApplicantInfo = uWApplicantInfo;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getSuburb() {
		return suburb;
	}

	public void setSuburb(String suburb) {
		this.suburb = suburb;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public Short getPostcode() {
		return postcode;
	}

	public void setPostcode(Short postcode) {
		this.postcode = postcode;
	}

	public String getApplicantEmail() {
		return applicantEmail;
	}

	public void setApplicantEmail(String applicantEmail) {
		this.applicantEmail = applicantEmail;
	}

	public long getMobilePhone() {
		return mobilePhone;
	}

	public void setMobilePhone(long mobilePhone) {
		this.mobilePhone = mobilePhone;
	}

	public long getHomePhone() {
		return homePhone;
	}

	public void setHomePhone(long homePhone) {
		this.homePhone = homePhone;
	}

	public long getRolloverPending() {
		return rolloverPending;
	}

	public void setRolloverPending(long rolloverPending) {
		this.rolloverPending = rolloverPending;
	}

	public long getInsuranceScale() {
		return insuranceScale;
	}

	public void setInsuranceScale(long insuranceScale) {
		this.insuranceScale = insuranceScale;
	}

	public long getPrefTime() {
		return prefTime;
	}

	public void setPrefTime(long prefTime) {
		this.prefTime = prefTime;
	}

}
