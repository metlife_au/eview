/**
 * 
 */
package com.metlife.au.uw.dto;

import java.io.Serializable;

/**
 * @author 672381
 *
 */
public class UWDocumentInfoDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6055643910978727757L;
	private long documentInfoId;
	private String documentLocation;
	private String documentStatus;
	private String docType;
	public UWDocumentInfoDto() {
		super();
		// TODO Auto-generated constructor stub
	}


	public long getDocumentInfoId() {
		return documentInfoId;
	}


	public void setDocumentInfoId(long documentInfoId) {
		this.documentInfoId = documentInfoId;
	}


	public String getDocumentLocation() {
		return documentLocation;
	}
	public void setDocumentLocation(String documentLocation) {
		this.documentLocation = documentLocation;
	}
	public String getDocumentStatus() {
		return documentStatus;
	}
	public void setDocumentStatus(String documentStatus) {
		this.documentStatus = documentStatus;
	}
	public String getDocType() {
		return docType;
	}
	public void setDocType(String docType) {
		this.docType = docType;
	}
	
	
}
