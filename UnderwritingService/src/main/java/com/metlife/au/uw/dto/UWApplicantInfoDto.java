/**
 * 
 */
package com.metlife.au.uw.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

/**
 * @author 672381
 *
 */
public class UWApplicantInfoDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3609902405807465330L;
	private long applicationId;
	private String firstName;
	private String lastName;
	private String categoryId;
	private Date dateOfBirth;
	private String status="NewUW";	
	private Date dateJoinedFund;
	private String dateJoinedCompany;
	private String clientRefNumber;
	private String fundCode;
	private String addItionalInfo;	
	private String userName;	
	private String title;
	private String gender;
	private String submittedEmail;
	private long submittedPhone;	
	private Date createDate=new Date();
	private Date lastUpdateDate=new Date();
	private Set<UWCoversInfoDto> coversInfo;
	private Set<UWContactInfoDto> contactInfo;
	private Set<UWDocumentInfoDto> documentInfo;
	private String applicationType="NewUW";
	

	/**
	 * 
	 */
	public UWApplicantInfoDto() {
		super();
		// TODO Auto-generated constructor stub
	}


	public long getApplicationId() {
		return applicationId;
	}


	public void setApplicationId(long applicationId) {
		this.applicationId = applicationId;
	}


	public String getFirstName() {
		return firstName;
	}


	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


	public String getLastName() {
		return lastName;
	}


	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	public String getCategoryId() {
		return categoryId;
	}


	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}


	public Date getDateOfBirth() {
		return dateOfBirth;
	}


	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}


	public Date getDateJoinedFund() {
		return dateJoinedFund;
	}


	public void setDateJoinedFund(Date dateJoinedFund) {
		this.dateJoinedFund = dateJoinedFund;
	}


	public String getDateJoinedCompany() {
		return dateJoinedCompany;
	}


	public void setDateJoinedCompany(String dateJoinedCompany) {
		this.dateJoinedCompany = dateJoinedCompany;
	}


	public String getClientRefNumber() {
		return clientRefNumber;
	}


	public void setClientRefNumber(String clientRefNumber) {
		this.clientRefNumber = clientRefNumber;
	}


	public String getFundCode() {
		return fundCode;
	}


	public void setFundCode(String fundCode) {
		this.fundCode = fundCode;
	}


	public String getAddItionalInfo() {
		return addItionalInfo;
	}


	public void setAddItionalInfo(String addItionalInfo) {
		this.addItionalInfo = addItionalInfo;
	}


	public String getUserName() {
		return userName;
	}


	public void setUserName(String userName) {
		this.userName = userName;
	}


	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	public String getGender() {
		return gender;
	}


	public void setGender(String gender) {
		this.gender = gender;
	}


	public String getSubmittedEmail() {
		return submittedEmail;
	}


	public void setSubmittedEmail(String submittedEmail) {
		this.submittedEmail = submittedEmail;
	}


	public long getSubmittedPhone() {
		return submittedPhone;
	}


	public void setSubmittedPhone(long submittedPhone) {
		this.submittedPhone = submittedPhone;
	}


	public Date getCreateDate() {
		return createDate;
	}


	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}


	public Date getLastUpdateDate() {
		return lastUpdateDate;
	}


	public void setLastUpdateDate(Date lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}


	public Set<UWCoversInfoDto> getCoversInfo() {
		return coversInfo;
	}


	public void setCoversInfo(Set<UWCoversInfoDto> coversInfo) {
		this.coversInfo = coversInfo;
	}


	public Set<UWContactInfoDto> getContactInfo() {
		return contactInfo;
	}


	public void setContactInfo(Set<UWContactInfoDto> contactInfo) {
		this.contactInfo = contactInfo;
	}


	public Set<UWDocumentInfoDto> getDocumentInfo() {
		return documentInfo;
	}


	public void setDocumentInfo(Set<UWDocumentInfoDto> documentInfo) {
		this.documentInfo = documentInfo;
	}


	public String getApplicationType() {
		return applicationType;
	}


	public void setApplicationType(String applicationType) {
		this.applicationType = applicationType;
	}

	
	
}
