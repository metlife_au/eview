package com.metlife.au.uw.dto;

import java.io.Serializable;

/**
 * This class contains all the details of any report.
 * 
 * @author 390092
 *
 */
public class UWSearchFilter implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String fundName;
	private String appRcvdDateFrom;
	private String appRcvdDateTo;
	private String applicationNo;
	private String firstName;
	private String surName;
	private String dob;
	private String clientRefNo;
	private String status;
	private String source;
	private String action;
	private String sortColm;
	private String fundOwnerId;
	private String claimNo;
	private String dateSubmitted;
	private String category;

	/**
	 * @return the fundName
	 */
	public String getFundName() {
		return fundName;
	}

	/**
	 * @param fundName the fundName to set
	 */
	public void setFundName(String fundName) {
		this.fundName = fundName;
	}

	/**
	 * @return the appRcvdDateFrom
	 */
	public String getAppRcvdDateFrom() {
		return appRcvdDateFrom;
	}

	/**
	 * @param appRcvdDateFrom the appRcvdDateFrom to set
	 */
	public void setAppRcvdDateFrom(String appRcvdDateFrom) {
		this.appRcvdDateFrom = appRcvdDateFrom;
	}

	/**
	 * @return the appRcvdDateTo
	 */
	public String getAppRcvdDateTo() {
		return appRcvdDateTo;
	}

	/**
	 * @param appRcvdDateTo the appRcvdDateTo to set
	 */
	public void setAppRcvdDateTo(String appRcvdDateTo) {
		this.appRcvdDateTo = appRcvdDateTo;
	}

	/**
	 * @return the applicationNo
	 */
	public String getApplicationNo() {
		return applicationNo;
	}

	/**
	 * @param applicationNo the applicationNo to set
	 */
	public void setApplicationNo(String applicationNo) {
		this.applicationNo = applicationNo;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the surName
	 */
	public String getSurName() {
		return surName;
	}

	/**
	 * @param surName the surName to set
	 */
	public void setSurName(String surName) {
		this.surName = surName;
	}

	/**
	 * @return the dob
	 */
	public String getDob() {
		return dob;
	}

	/**
	 * @param dob the dob to set
	 */
	public void setDob(String dob) {
		this.dob = dob;
	}

	/**
	 * @return the clientRefNo
	 */
	public String getClientRefNo() {
		return clientRefNo;
	}

	/**
	 * @param clientRefNo the clientRefNo to set
	 */
	public void setClientRefNo(String clientRefNo) {
		this.clientRefNo = clientRefNo;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the source
	 */
	public String getSource() {
		return source;
	}

	/**
	 * @param source the source to set
	 */
	public void setSource(String source) {
		this.source = source;
	}

	/**
	 * @return the action
	 */
	public String getAction() {
		return action;
	}

	/**
	 * @param action the action to set
	 */
	public void setAction(String action) {
		this.action = action;
	}

	/**
	 * @return the sortColm
	 */
	public String getSortColm() {
		return sortColm;
	}

	/**
	 * @param sortColm the sortColm to set
	 */
	public void setSortColm(String sortColm) {
		this.sortColm = sortColm;
	}

	/**
	 * @return the fundOwnerId
	 */
	public String getFundOwnerId() {
		return fundOwnerId;
	}

	/**
	 * @param fundOwnerId the fundOwnerId to set
	 */
	public void setFundOwnerId(String fundOwnerId) {
		this.fundOwnerId = fundOwnerId;
	}

	/**
	 * @return the claimNo
	 */
	public String getClaimNo() {
		return claimNo;
	}

	/**
	 * @param claimNo the claimNo to set
	 */
	public void setClaimNo(String claimNo) {
		this.claimNo = claimNo;
	}

	/**
	 * @return the dateSubmitted
	 */
	public String getDateSubmitted() {
		return dateSubmitted;
	}

	/**
	 * @param dateSubmitted the dateSubmitted to set
	 */
	public void setDateSubmitted(String dateSubmitted) {
		this.dateSubmitted = dateSubmitted;
	}

	/**
	 * @return the category
	 */
	public String getCategory() {
		return category;
	}

	/**
	 * @param category the category to set
	 */
	public void setCategory(String category) {
		this.category = category;
	}

}
