/**
 * 
 */
package com.metlife.au.uw.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.metlife.au.uw.dto.CategoryDto;
import com.metlife.au.uw.dto.CoverTypeDto;
import com.metlife.au.uw.dto.DocumentTypeDto;
import com.metlife.au.uw.dto.UWApplicantInfoDto;
import com.metlife.au.uw.entity.Category;
import com.metlife.au.uw.entity.CoverType;
import com.metlife.au.uw.entity.DocumentType;
import com.metlife.au.uw.entity.UWApplicantInfo;
import com.metlife.au.uw.exception.ApplicationException;
import com.metlife.au.uw.repository.UnderwritingRepository;
import com.metlife.au.uw.utils.MapperUtils;

/**
 * @author 672381
 *
 */

@Service
public class UnderwritingServiceImpl implements UnderwritingService {

	private UnderwritingRepository underwritingRepository;

	private MapperUtils mapperUtils;
	
	/**
	 * @param underwritingRepository
	 */
	@Autowired
	public UnderwritingServiceImpl(UnderwritingRepository underwritingRepository,MapperUtils mapperUtils) {
		super();
		this.underwritingRepository = underwritingRepository;
		this.mapperUtils = mapperUtils;
	}

	public UWApplicantInfoDto saveUnderwirrting(UWApplicantInfoDto uWApplicantInfo) throws ApplicationException {
		try {
			UWApplicantInfo uWApptInfoEntity=mapperUtils.convertDtoToEntity(uWApplicantInfo);
			UWApplicantInfo applicantInfo = underwritingRepository.saveUnderwirrting(uWApptInfoEntity);
			UWApplicantInfoDto applicationInfoDto=mapperUtils.convertEntityToDto(applicantInfo);
			return applicationInfoDto;
		}catch(Exception e) {
			throw new ApplicationException("Exception in saveUnderwirrting");
		}		
	}
	
	public List<CategoryDto> categoryDetails() throws ApplicationException{		
		List<CategoryDto> categoryDtoList=new ArrayList<CategoryDto>();
		try {
			List<Category> categoryDetails= underwritingRepository.categoryDetails();
			if(categoryDetails.size()>0) {
				categoryDtoList=mapperUtils.categoryConversion(categoryDetails);
			}
		}catch(Exception e) {
			throw new ApplicationException("Exception in category");
		}
		return categoryDtoList;
	}
	
	public List<DocumentTypeDto> documentTypeDetails() throws ApplicationException{
		List<DocumentTypeDto> docDtoList=new ArrayList<DocumentTypeDto>();
		try {
			List<DocumentType> docuTypeDetails= underwritingRepository.documentTypeDetails();
			if(docuTypeDetails.size()>0) {
				docDtoList=mapperUtils.documentConversion(docuTypeDetails);
			}
		}catch(Exception e) {
			throw new ApplicationException("Exception in documenttype");
		}
		return docDtoList;
	}
	
	public List<CoverTypeDto> coverTypeDetails() throws ApplicationException{
		List<CoverTypeDto> coverDtoList=new ArrayList<CoverTypeDto>();
		try {
			List<CoverType> coverTypeDetails= underwritingRepository.coverTypeDetails();
			if(coverTypeDetails.size()>0) {
				coverDtoList=mapperUtils.coverConversion(coverTypeDetails);
			}
		}catch(Exception e) {
			throw new ApplicationException("Exception in documenttype");
		}
		return coverDtoList;
	}
}
