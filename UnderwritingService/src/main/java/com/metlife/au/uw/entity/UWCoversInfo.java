/**
 * 
 */
package com.metlife.au.uw.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * @author 672381
 *
 */
@Entity
@Table(name = "ELODGEUWCOVERSINFO", schema = "MTLWPS")
public class UWCoversInfo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -197017409487186302L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "coverInfo")
	@SequenceGenerator(name = "coverInfo", sequenceName = "MTLWPS.SEQ_ELODGEUWCOVERSINFO", allocationSize = 1)
	@Column(name = "ID")
	private long coverId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ELODGEMENTAPPLICANTID", insertable = false, updatable = false)
	private UWApplicantInfo uWApplicantInfo;

	@Column(name = "EXISTINGCOVERUNITS")
	private Short existingCoverunits;

	@Column(name = "EXISTINGCOVERAMOUNT")
	private BigDecimal existingCoverAmount;

	@Column(name = "ADDITIONALCOVERUNITS")
	private Short additionalCoverUnits;

	@Column(name = "ADDITIONALCOVERAMOUNT")
	private BigDecimal additionalCoverAmount;

	@Column(name = "TOTALREQUESTEDCOVER")
	private BigDecimal totalRequestedCover;

	@Column(name = "COVERCODE")
	private String coverCode;

	@Column(name = "CREATEDATE")
	private Date createDate;

	@Column(name = "LASTUPDATEDATE")
	private Date lastUpdateDate;

	@Column(name = "EXISTINGCOVEROPTION")
	private String existingCoverOption;

	@Column(name = "ADDITIONALCOVEROPTION")
	private String additionalCoverOption;

	/**
	 * 
	 */
	public UWCoversInfo() {
		super();
		// TODO Auto-generated constructor stub
	}

	public long getCoverId() {
		return coverId;
	}

	public void setCoverId(long coverId) {
		this.coverId = coverId;
	}

	public UWApplicantInfo getuWApplicantInfo() {
		return uWApplicantInfo;
	}

	public void setuWApplicantInfo(UWApplicantInfo uWApplicantInfo) {
		this.uWApplicantInfo = uWApplicantInfo;
	}

	public Short getExistingCoverunits() {
		return existingCoverunits;
	}

	public void setExistingCoverunits(Short existingCoverunits) {
		this.existingCoverunits = existingCoverunits;
	}

	public BigDecimal getExistingCoverAmount() {
		return existingCoverAmount;
	}

	public void setExistingCoverAmount(BigDecimal existingCoverAmount) {
		this.existingCoverAmount = existingCoverAmount;
	}

	public Short getAdditionalCoverUnits() {
		return additionalCoverUnits;
	}

	public void setAdditionalCoverUnits(Short additionalCoverUnits) {
		this.additionalCoverUnits = additionalCoverUnits;
	}

	public BigDecimal getAdditionalCoverAmount() {
		return additionalCoverAmount;
	}

	public void setAdditionalCoverAmount(BigDecimal additionalCoverAmount) {
		this.additionalCoverAmount = additionalCoverAmount;
	}

	public BigDecimal getTotalRequestedCover() {
		return totalRequestedCover;
	}

	public void setTotalRequestedCover(BigDecimal totalRequestedCover) {
		this.totalRequestedCover = totalRequestedCover;
	}

	public String getCoverCode() {
		return coverCode;
	}

	public void setCoverCode(String coverCode) {
		this.coverCode = coverCode;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getLastUpdateDate() {
		return lastUpdateDate;
	}

	public void setLastUpdateDate(Date lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}

	public String getExistingCoverOption() {
		return existingCoverOption;
	}

	public void setExistingCoverOption(String existingCoverOption) {
		this.existingCoverOption = existingCoverOption;
	}

	public String getAdditionalCoverOption() {
		return additionalCoverOption;
	}

	public void setAdditionalCoverOption(String additionalCoverOption) {
		this.additionalCoverOption = additionalCoverOption;
	}

	
}
