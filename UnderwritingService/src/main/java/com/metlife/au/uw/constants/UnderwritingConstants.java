/**
 * 
 */
package com.metlife.au.uw.constants;

/**
 * @author 672381
 *
 */
public class UnderwritingConstants {

	public static final String SQL_TIMESTAMP_FORMAT = "yyyy-MM-dd HH:mm:ss";
	public static final String SQL_TIMESTAMP_FORMAT_BEGIN = "yyyy-MM-dd 00:00:00";
	public static final String SQL_TIMESTAMP_FORMAT_END = "yyyy-MM-dd 23:59:59";

	public static final String UW_CATEGORY = "Underwriting";
	public static final String CLAIM_CATEGORY = "Claim";

	public static final String EMPTY_STRING = "";
	public static final String EMPTY_SPACE = " ";
	public static final String SMART_QUOTE = "�";
	public static final String SINGLE_QUOTE = "'";
	public static final String DOUBLE_QUOTES = "''";
	public static final String BACKWARD_SLASH = "\\";
	public static final String FORWARD_SLASH = "/";
	public static final char DOT = '.';
	public static final String UNDER_SCORE = "_";
	public static final String DELIMITER = "\\|";
	public static final String DELIMITER_DASH = "-";
}
