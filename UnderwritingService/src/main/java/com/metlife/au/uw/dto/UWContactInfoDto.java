/**
 * 
 */
package com.metlife.au.uw.dto;

import java.io.Serializable;

/**
 * @author 672381
 *
 */
public class UWContactInfoDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4037127001698297956L;
	private long contactId;
	private UWApplicantInfoDto elodgeApplicantInfo;
	private String address1;
	private String address2;
	private String suburb;
	private String state;
	private Short postcode;
	private String applicantEmail;
	private long mobilePhone;	
	private long prefTime;

	/**
	 * 
	 */
	public UWContactInfoDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public long getContactId() {
		return contactId;
	}

	public void setContactId(long contactId) {
		this.contactId = contactId;
	}

	public UWApplicantInfoDto getElodgeApplicantInfo() {
		return elodgeApplicantInfo;
	}

	public void setElodgeApplicantInfo(UWApplicantInfoDto elodgeApplicantInfo) {
		this.elodgeApplicantInfo = elodgeApplicantInfo;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getSuburb() {
		return suburb;
	}

	public void setSuburb(String suburb) {
		this.suburb = suburb;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public Short getPostcode() {
		return postcode;
	}

	public void setPostcode(Short postcode) {
		this.postcode = postcode;
	}

	public String getApplicantEmail() {
		return applicantEmail;
	}

	public void setApplicantEmail(String applicantEmail) {
		this.applicantEmail = applicantEmail;
	}

	public long getMobilePhone() {
		return mobilePhone;
	}

	public void setMobilePhone(long mobilePhone) {
		this.mobilePhone = mobilePhone;
	}

	public long getPrefTime() {
		return prefTime;
	}

	public void setPrefTime(long prefTime) {
		this.prefTime = prefTime;
	}

	
}
