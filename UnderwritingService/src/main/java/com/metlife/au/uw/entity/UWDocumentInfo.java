/**
 * 
 */
package com.metlife.au.uw.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * @author 672381
 *
 */
@Entity
@Table(name = "ELODGEDOCUMENTINFO", schema = "MTLWPS")
public class UWDocumentInfo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5927778137129136209L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "docInfo")
	@SequenceGenerator(name = "docInfo", sequenceName = "MTLWPS.SEQ_ELODGEDOCUMENTINFO", allocationSize = 1)
	@Column(name = "ID")
	private long documentInfoId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ELODGEMENTAPPLICANTID", insertable = false, updatable = false)
	private UWApplicantInfo uWApplicantInfo;

	@Column(name = "DOCUMENTLOCATION")
	private String documentLocation;

	@Column(name = "DOCUMENTSTATUS")
	private String documentStatus;

	@Column(name = "DOCTYPE")
	private String docType;

	public UWDocumentInfo() {
		super();
		// TODO Auto-generated constructor stub
	}

	public long getDocumentInfoId() {
		return documentInfoId;
	}

	public void setDocumentInfoId(long documentInfoId) {
		this.documentInfoId = documentInfoId;
	}

	public UWApplicantInfo getuWApplicantInfo() {
		return uWApplicantInfo;
	}

	public void setuWApplicantInfo(UWApplicantInfo uWApplicantInfo) {
		this.uWApplicantInfo = uWApplicantInfo;
	}

	public String getDocumentLocation() {
		return documentLocation;
	}

	public void setDocumentLocation(String documentLocation) {
		this.documentLocation = documentLocation;
	}

	public String getDocumentStatus() {
		return documentStatus;
	}

	public void setDocumentStatus(String documentStatus) {
		this.documentStatus = documentStatus;
	}

	public String getDocType() {
		return docType;
	}

	public void setDocType(String docType) {
		this.docType = docType;
	}
	
	
}
