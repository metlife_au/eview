/**
 * 
 */
package com.metlife.au.uw.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * @author 672381
 *
 */

@Entity
@Table(name = "ELODGECLAIMCOVERSINFO", schema = "MTLWPS")
public class ClaimCoversInfo {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "claimInfo")
	@SequenceGenerator(name = "claimInfo", sequenceName = "MTLWPS.SEQ_ELODGECLAIMCOVERSINFO", allocationSize = 1)
	@Column(name = "ID", updatable = false, nullable = false)
	private long id;

	@Column(name = "SUMINSCLAIM")
	private BigDecimal suminsClaim;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ELODGEMENTAPPLICANTID", insertable = false, updatable = false)
	private UWApplicantInfo uWApplicantInfo;

	@Column(name = "INSCOVCUR")
	private String insCovcur;

	@Column(name = "INSCOVCURCOMENTS")
	private String insCovcurComents;

	@Column(name = "COVERCODE")
	private String coverCode;

	@Column(name = "DATEOFEVENT")
	private Date dateOfEvent;

	@Column(name = "CLAIMCONDITION")
	private String claimCondition;

	@Column(name = "CLAIMCAUSE")
	private String claimCause;

	@Column(name = "DATEPREMIUMSTARTED")
	private Date datePrimiumStarted;

	@Column(name = "UNDERWRITTENYORN")
	private String undewrittenyorn;

	/**
	 * 
	 */
	public ClaimCoversInfo() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param id
	 * @param suminsClaim
	 * @param uWApplicantInfo
	 * @param insCovcur
	 * @param insCovcurComents
	 * @param coverCode
	 * @param dateOfEvent
	 * @param claimCondition
	 * @param claimCause
	 * @param datePrimiumStarted
	 * @param undewrittenyorn
	 */
	public ClaimCoversInfo(long id, BigDecimal suminsClaim, UWApplicantInfo uWApplicantInfo, String insCovcur,
			String insCovcurComents, String coverCode, Date dateOfEvent, String claimCondition, String claimCause,
			Date datePrimiumStarted, String undewrittenyorn) {
		super();
		this.id = id;
		this.suminsClaim = suminsClaim;
		this.uWApplicantInfo = uWApplicantInfo;
		this.insCovcur = insCovcur;
		this.insCovcurComents = insCovcurComents;
		this.coverCode = coverCode;
		this.dateOfEvent = dateOfEvent;
		this.claimCondition = claimCondition;
		this.claimCause = claimCause;
		this.datePrimiumStarted = datePrimiumStarted;
		this.undewrittenyorn = undewrittenyorn;
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the suminsClaim
	 */
	public BigDecimal getSuminsClaim() {
		return suminsClaim;
	}

	/**
	 * @param suminsClaim the suminsClaim to set
	 */
	public void setSuminsClaim(BigDecimal suminsClaim) {
		this.suminsClaim = suminsClaim;
	}

	/**
	 * @return the elodgeApplicantInfo
	 */
	public UWApplicantInfo getElodgeApplicantInfo() {
		return uWApplicantInfo;
	}

	/**
	 * @param uWApplicantInfo the elodgeApplicantInfo to set
	 */
	public void setElodgeApplicantInfo(UWApplicantInfo uWApplicantInfo) {
		this.uWApplicantInfo = uWApplicantInfo;
	}

	/**
	 * @return the insCovcur
	 */
	public String getInsCovcur() {
		return insCovcur;
	}

	/**
	 * @param insCovcur the insCovcur to set
	 */
	public void setInsCovcur(String insCovcur) {
		this.insCovcur = insCovcur;
	}

	/**
	 * @return the insCovcurComents
	 */
	public String getInsCovcurComents() {
		return insCovcurComents;
	}

	/**
	 * @param insCovcurComents the insCovcurComents to set
	 */
	public void setInsCovcurComents(String insCovcurComents) {
		this.insCovcurComents = insCovcurComents;
	}

	/**
	 * @return the coverCode
	 */
	public String getCoverCode() {
		return coverCode;
	}

	/**
	 * @param coverCode the coverCode to set
	 */
	public void setCoverCode(String coverCode) {
		this.coverCode = coverCode;
	}

	/**
	 * @return the dateOfEvent
	 */
	public Date getDateOfEvent() {
		return dateOfEvent;
	}

	/**
	 * @param dateOfEvent the dateOfEvent to set
	 */
	public void setDateOfEvent(Date dateOfEvent) {
		this.dateOfEvent = dateOfEvent;
	}

	/**
	 * @return the claimCondition
	 */
	public String getClaimCondition() {
		return claimCondition;
	}

	/**
	 * @param claimCondition the claimCondition to set
	 */
	public void setClaimCondition(String claimCondition) {
		this.claimCondition = claimCondition;
	}

	/**
	 * @return the claimCause
	 */
	public String getClaimCause() {
		return claimCause;
	}

	/**
	 * @param claimCause the claimCause to set
	 */
	public void setClaimCause(String claimCause) {
		this.claimCause = claimCause;
	}

	/**
	 * @return the datePrimiumStarted
	 */
	public Date getDatePrimiumStarted() {
		return datePrimiumStarted;
	}

	/**
	 * @param datePrimiumStarted the datePrimiumStarted to set
	 */
	public void setDatePrimiumStarted(Date datePrimiumStarted) {
		this.datePrimiumStarted = datePrimiumStarted;
	}

	/**
	 * @return the undewrittenyorn
	 */
	public String getUndewrittenyorn() {
		return undewrittenyorn;
	}

	/**
	 * @param undewrittenyorn the undewrittenyorn to set
	 */
	public void setUndewrittenyorn(String undewrittenyorn) {
		this.undewrittenyorn = undewrittenyorn;
	}

}
