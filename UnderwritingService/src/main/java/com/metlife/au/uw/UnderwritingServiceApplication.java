package com.metlife.au.uw;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@SpringBootApplication
@EnableAspectJAutoProxy
@EnableEurekaClient
public class UnderwritingServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(UnderwritingServiceApplication.class, args);
	}

}
