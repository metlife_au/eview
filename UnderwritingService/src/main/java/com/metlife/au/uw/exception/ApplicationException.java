/**
 * 
 */
package com.metlife.au.uw.exception;

/**
 * @author 672381
 *
 */
public class ApplicationException extends Exception {

	private static final long serialVersionUID = 1L;

	public ApplicationException(String message) {
		super(message);
	}
}
