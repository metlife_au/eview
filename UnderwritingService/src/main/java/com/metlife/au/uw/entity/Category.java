/**
 * 
 */
package com.metlife.au.uw.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author 672381
 *
 */
@Entity
@Table(name = "CATEGORY", schema = "MTLWPS")
public class Category implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1621664317536101632L;

	@Id	
	@Column(name = "CATEGORY_ID", nullable=false)
	private int categoryId;

	@Column(name = "CATEGORY_NAME")
	private String categoryName;

	public Category() {
		super();
		// TODO Auto-generated constructor stub
	}

	public int getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	
	
	
}
