/**
 * 
 */
package com.metlife.au.uw.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * @author 672381
 *
 */

@Entity
@Table(name = "ELODGEAPPLICANTINFO", schema = "MTLWPS")
public class UWApplicantInfo implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1052006683438379521L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "applicationInfo")
	@SequenceGenerator(name = "applicationInfo", sequenceName = "MTLWPS.SEQ_ELODGEAPPLICANTINFO", allocationSize = 1)
	@Column(name = "ELODGEMENTAPPLICANTID", updatable = false, nullable = false)
	private long applicationId;

	@Column(name = "FIRSTNAME")
	private String firstName;

	@Column(name = "LASTNAME")
	private String lastName;

	@Column(name = "APPLICATIONCATEGORY")
	private String categoryId;

	@Column(name = "STATUS")
	private String status;

	@Column(name = "CREATEDATE")
	private Date createDate;

	@Column(name = "LASTUPDATEDATE")
	private Date lastUpdateDate;

	@Column(name = "DOB")
	private Date dateOfBirth;

	@Column(name = "DATEJOINEDFUND")
	private Date dateJoinedFund;

	@Column(name = "DATEJOINEDCOMPANY")
	private String dateJoinedCompany;

	@Column(name = "CLIENTREFNUMBER")
	private String clientRefNumber;

	@Column(name = "FUNDCODE")
	private String fundCode;

	@Column(name = "ADDITIONALINFO")
	private String addItionalInfo;

	@Column(name = "COMMENTS")
	private String comments;

	@Column(name = "APPLICATIONTYPE")
	private String applicationType;

	@Column(name = "USERNAME")
	private String userName;

	@Column(name = "PICSCASEID")
	private String picsCaseId;

	@Column(name = "TITLE")
	private String title;

	@Column(name = "GENDER")
	private String gender;

	@Column(name = "SUBMITTEDEMAIL")
	private String submittedEmail;

	@Column(name = "SUBMITTEDPHONE")
	private long submittedPhone;

	@Column(name = "MEMBERTYPE")
	private String memberType;

	@Column(name = "ACCOUNTBALANCE")
	private BigDecimal accountBalance;

	@Column(name = "LASTCONTRIBUTIONDATE")
	private Date lastContributionDate;

	@Column(name = "EMPLOYERNAME")
	private String employerName;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "ELODGEMENTAPPLICANTID", referencedColumnName = "ELODGEMENTAPPLICANTID", nullable = false)
	private Set<UWCoversInfo> coversInfo;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "ELODGEMENTAPPLICANTID", referencedColumnName = "ELODGEMENTAPPLICANTID", nullable = false)
	private Set<UWContactInfo> contactInfo;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "ELODGEMENTAPPLICANTID", referencedColumnName = "ELODGEMENTAPPLICANTID", nullable = false)
	private Set<UWDocumentInfo> documentInfo;

	/**
	 * 
	 */
	public UWApplicantInfo() {
		super();
		// TODO Auto-generated constructor stub
	}

	public long getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(long applicationId) {
		this.applicationId = applicationId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getLastUpdateDate() {
		return lastUpdateDate;
	}

	public void setLastUpdateDate(Date lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public Date getDateJoinedFund() {
		return dateJoinedFund;
	}

	public void setDateJoinedFund(Date dateJoinedFund) {
		this.dateJoinedFund = dateJoinedFund;
	}

	public String getDateJoinedCompany() {
		return dateJoinedCompany;
	}

	public void setDateJoinedCompany(String dateJoinedCompany) {
		this.dateJoinedCompany = dateJoinedCompany;
	}

	public String getClientRefNumber() {
		return clientRefNumber;
	}

	public void setClientRefNumber(String clientRefNumber) {
		this.clientRefNumber = clientRefNumber;
	}

	public String getFundCode() {
		return fundCode;
	}

	public void setFundCode(String fundCode) {
		this.fundCode = fundCode;
	}

	public String getAddItionalInfo() {
		return addItionalInfo;
	}

	public void setAddItionalInfo(String addItionalInfo) {
		this.addItionalInfo = addItionalInfo;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getApplicationType() {
		return applicationType;
	}

	public void setApplicationType(String applicationType) {
		this.applicationType = applicationType;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPicsCaseId() {
		return picsCaseId;
	}

	public void setPicsCaseId(String picsCaseId) {
		this.picsCaseId = picsCaseId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getSubmittedEmail() {
		return submittedEmail;
	}

	public void setSubmittedEmail(String submittedEmail) {
		this.submittedEmail = submittedEmail;
	}

	public long getSubmittedPhone() {
		return submittedPhone;
	}

	public void setSubmittedPhone(long submittedPhone) {
		this.submittedPhone = submittedPhone;
	}

	public String getMemberType() {
		return memberType;
	}

	public void setMemberType(String memberType) {
		this.memberType = memberType;
	}

	public BigDecimal getAccountBalance() {
		return accountBalance;
	}

	public void setAccountBalance(BigDecimal accountBalance) {
		this.accountBalance = accountBalance;
	}

	public Date getLastContributionDate() {
		return lastContributionDate;
	}

	public void setLastContributionDate(Date lastContributionDate) {
		this.lastContributionDate = lastContributionDate;
	}

	public String getEmployerName() {
		return employerName;
	}

	public void setEmployerName(String employerName) {
		this.employerName = employerName;
	}

	public Set<UWCoversInfo> getCoversInfo() {
		return coversInfo;
	}

	public void setCoversInfo(Set<UWCoversInfo> coversInfo) {
		this.coversInfo = coversInfo;
	}

	public Set<UWContactInfo> getContactInfo() {
		return contactInfo;
	}

	public void setContactInfo(Set<UWContactInfo> contactInfo) {
		this.contactInfo = contactInfo;
	}

	public Set<UWDocumentInfo> getDocumentInfo() {
		return documentInfo;
	}

	public void setDocumentInfo(Set<UWDocumentInfo> documentInfo) {
		this.documentInfo = documentInfo;
	}

	

	
}
