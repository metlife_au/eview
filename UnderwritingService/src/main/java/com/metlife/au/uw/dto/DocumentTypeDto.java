/**
 * 
 */
package com.metlife.au.uw.dto;

import java.io.Serializable;

import javax.persistence.Column;

/**
 * @author 672381
 *
 */
public class DocumentTypeDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2868486056630676165L;

	private int documentTypeID;
	private String documentTypeName;

	public DocumentTypeDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public int getDocumentTypeID() {
		return documentTypeID;
	}

	public void setDocumentTypeID(int documentTypeID) {
		this.documentTypeID = documentTypeID;
	}

	public String getDocumentTypeName() {
		return documentTypeName;
	}

	public void setDocumentTypeName(String documentTypeName) {
		this.documentTypeName = documentTypeName;
	}
}
