/**
 * 
 */
package com.metlife.au.uw.repository;

import java.util.List;

import com.metlife.au.uw.entity.Category;
import com.metlife.au.uw.entity.CoverType;
import com.metlife.au.uw.entity.DocumentType;
import com.metlife.au.uw.entity.UWApplicantInfo;
import com.metlife.au.uw.exception.ApplicationException;

/**
 * @author 672381
 *
 */
public interface UnderwritingRepository {

	public UWApplicantInfo saveUnderwirrting(UWApplicantInfo uwApplicantInfo) throws ApplicationException;
	
	public List<Category> categoryDetails() throws ApplicationException;
	
	public List<DocumentType> documentTypeDetails() throws ApplicationException;
	
	public List<CoverType> coverTypeDetails() throws ApplicationException;

}
