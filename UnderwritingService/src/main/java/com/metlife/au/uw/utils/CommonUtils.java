/**
 * 
 */
package com.metlife.au.uw.utils;

import org.springframework.util.StringUtils;

import com.metlife.au.uw.constants.UnderwritingConstants;

/**
 * @author 672381
 *
 */
public class CommonUtils {

	public static String addEscapeApostrophe(String name) {
		String updatedName = name;
		if (!StringUtils.isEmpty(updatedName) && updatedName.length() > 0
				&& updatedName.contains(UnderwritingConstants.SINGLE_QUOTE)) {
			updatedName = updatedName.replace(UnderwritingConstants.SINGLE_QUOTE, UnderwritingConstants.DOUBLE_QUOTES);
		}
		return updatedName;
	}

}
