/**
 * 
 */
package com.metlife.au.uw.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.Conditions;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.metlife.au.uw.dto.CategoryDto;
import com.metlife.au.uw.dto.CoverTypeDto;
import com.metlife.au.uw.dto.DocumentTypeDto;
import com.metlife.au.uw.dto.UWApplicantInfoDto;
import com.metlife.au.uw.entity.Category;
import com.metlife.au.uw.entity.CoverType;
import com.metlife.au.uw.entity.DocumentType;
import com.metlife.au.uw.entity.UWApplicantInfo;
import com.metlife.au.uw.exception.ApplicationException;

/**
 * @author 672381
 *
 */
@Component
public class MapperUtils {

	private final Logger logger = LoggerFactory.getLogger(MapperUtils.class.getName());
	private ModelMapper modelMapper;

	/**
	 * 
	 */
	public MapperUtils() {
		modelMapper = new ModelMapper();
		modelMapper.getConfiguration().setPropertyCondition(Conditions.isNotNull());
		modelMapper.getConfiguration().setAmbiguityIgnored(true);
	}

	public UWApplicantInfo convertDtoToEntity(UWApplicantInfoDto uWApplicantInfoDto) {
		UWApplicantInfo uWApplicantInfo = new UWApplicantInfo();
		try {
			uWApplicantInfo = modelMapper.map(uWApplicantInfoDto, UWApplicantInfo.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return uWApplicantInfo;
	}
	
	public UWApplicantInfoDto convertEntityToDto(UWApplicantInfo uwApplicantInfo) {
		UWApplicantInfoDto uWApplicantInfo = new UWApplicantInfoDto();
		try {
			uWApplicantInfo = modelMapper.map(uwApplicantInfo, UWApplicantInfoDto.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return uWApplicantInfo;
	}
	
	public List<CategoryDto> categoryConversion(List<Category> categorys)
			throws ApplicationException {
		logger.info("Inside the CategoryConversion method from Mapper");
		List<CategoryDto> categoryDtoList = new ArrayList<CategoryDto>();
		try {
			categoryDtoList = categorys.stream().map(detail -> {				
					return modelMapper.map(detail, CategoryDto.class);				
			}).collect(Collectors.toList());
		} catch (Exception e) {
			logger.info("In CategoryConversion method catch block from Mapper" + e.getMessage());
			throw new ApplicationException("Exception in Mapper module");
		}
		logger.info("end of the CategoryConversion method from Mapper");
		return categoryDtoList;
	}
	
	public List<DocumentTypeDto> documentConversion(List<DocumentType> documentTypes)
			throws ApplicationException {
		logger.info("Inside the documentConversion method from Mapper");
		List<DocumentTypeDto> docDtoList = new ArrayList<DocumentTypeDto>();
		try {
			docDtoList = documentTypes.stream().map(detail -> {				
					return modelMapper.map(detail, DocumentTypeDto.class);
				
			}).collect(Collectors.toList());
		} catch (Exception e) {
			logger.info("In documentConversion method catch block from Mapper" + e.getMessage());
			throw new ApplicationException("Exception in Mapper module");
		}
		logger.info("end of the documentConversion method from Mapper");
		return docDtoList;
	}
	
	public List<CoverTypeDto> coverConversion(List<CoverType> coverTypes)
			throws ApplicationException {
		logger.info("Inside the coverConversion method from Mapper");
		List<CoverTypeDto> coverDtoList = new ArrayList<CoverTypeDto>();
		try {
			coverDtoList = coverTypes.stream().map(detail -> {				
					return modelMapper.map(detail, CoverTypeDto.class);
				
			}).collect(Collectors.toList());
		} catch (Exception e) {
			logger.info("In coverConversion method catch block from Mapper" + e.getMessage());
			throw new ApplicationException("Exception in Mapper module");
		}
		logger.info("end of the coverConversion method from Mapper");
		return coverDtoList;
	}

}
