/**
 * 
 */
package com.metlife.au.uw.repository;

import java.text.SimpleDateFormat;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.metlife.au.uw.constants.UnderwritingConstants;
import com.metlife.au.uw.dto.CoverTypeDto;
import com.metlife.au.uw.dto.DocumentTypeDto;
import com.metlife.au.uw.entity.Category;
import com.metlife.au.uw.entity.CoverType;
import com.metlife.au.uw.entity.DocumentType;
import com.metlife.au.uw.entity.UWApplicantInfo;
import com.metlife.au.uw.exception.ApplicationException;
import com.metlife.au.uw.utils.MapperUtils;

/**
 * @author 672381
 *
 */

@Transactional
@Repository
public class UnderwritingRepositoryImpl implements UnderwritingRepository {

	private final Logger logger = LoggerFactory.getLogger(UnderwritingRepositoryImpl.class);

	public static final SimpleDateFormat tsFormatSQL = new SimpleDateFormat(UnderwritingConstants.SQL_TIMESTAMP_FORMAT);
	public static final SimpleDateFormat tsFormatBeginSQL = new SimpleDateFormat(
			UnderwritingConstants.SQL_TIMESTAMP_FORMAT_BEGIN);
	public static final SimpleDateFormat tsFormatEndSQL = new SimpleDateFormat(
			UnderwritingConstants.SQL_TIMESTAMP_FORMAT_END);

	@PersistenceContext
	EntityManager entityManager;

	private MapperUtils mapperUtils;

	/**
	 * @param mapperUtils
	 */
	@Autowired
	public UnderwritingRepositoryImpl(MapperUtils mapperUtils) {
		super();
		this.mapperUtils = mapperUtils;
	}

	public UWApplicantInfo saveUnderwirrting(UWApplicantInfo uwApplicantInfo) throws ApplicationException {
		UWApplicantInfo applicantInfo=new UWApplicantInfo();
		try {
			if (uwApplicantInfo != null) {
				//UWApplicantInfo uWApplicantInfo = mapperUtils.convertDtoToEntity(uWApplicantInfoDto);
				entityManager.persist(uwApplicantInfo);
				entityManager.flush();
				applicantInfo=entityManager.find(UWApplicantInfo.class, uwApplicantInfo.getApplicationId());
			}

		} catch (Exception e) {
			logger.info("Catch Block " + e.getMessage());
			throw new ApplicationException("Application Exception");
		}

		return applicantInfo;
	}
	
	public List<Category> categoryDetails() throws ApplicationException{
		try {
			CriteriaBuilder cb = entityManager.getCriteriaBuilder();
			CriteriaQuery<Category> query = cb.createQuery(Category.class);
			Root<Category> root = query.from(Category.class);
			query.select(root);			
			query.orderBy(cb.asc(root.get("categoryName")));
			return entityManager.createQuery(query).getResultList();
		}catch (Exception ex) {
			throw new ApplicationException("Exception");
		}
	}
	
	public List<DocumentType> documentTypeDetails() throws ApplicationException{
		try {
			CriteriaBuilder cb = entityManager.getCriteriaBuilder();
			CriteriaQuery<DocumentType> query = cb.createQuery(DocumentType.class);
			Root<DocumentType> root = query.from(DocumentType.class);
			query.select(root);			
			query.orderBy(cb.asc(root.get("documentTypeName")));
			return entityManager.createQuery(query).getResultList();
		}catch (Exception ex) {
			throw new ApplicationException("Exception");
		}
	}
	
	public List<CoverType> coverTypeDetails() throws ApplicationException{
		try {
			CriteriaBuilder cb = entityManager.getCriteriaBuilder();
			CriteriaQuery<CoverType> query = cb.createQuery(CoverType.class);
			Root<CoverType> root = query.from(CoverType.class);
			query.select(root);			
			query.orderBy(cb.asc(root.get("coverName")));
			return entityManager.createQuery(query).getResultList();
		}catch (Exception ex) {
			throw new ApplicationException("Exception");
		}
	}
}
