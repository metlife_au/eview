/**
 * 
 */
package com.metlife.au.uw.dto;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author 672381
 *
 */
public class ClaimCoversInfoDto {

	private long id;
	private BigDecimal suminsClaim;
	private UWApplicantInfoDto elodgeApplicantInfo;
	private String insCovcur;
	private String insCovcurComents;
	private String coverCode;
	private Date dateOfEvent;
	private String claimCondition;
	private String claimCause;
	private Date datePrimiumStarted;
	private String undewrittenyorn;

	/**
	 * 
	 */
	public ClaimCoversInfoDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param id
	 * @param suminsClaim
	 * @param elodgeApplicantInfo
	 * @param insCovcur
	 * @param insCovcurComents
	 * @param coverCode
	 * @param dateOfEvent
	 * @param claimCondition
	 * @param claimCause
	 * @param datePrimiumStarted
	 * @param undewrittenyorn
	 */
	public ClaimCoversInfoDto(long id, BigDecimal suminsClaim, UWApplicantInfoDto elodgeApplicantInfo, String insCovcur,
			String insCovcurComents, String coverCode, Date dateOfEvent, String claimCondition, String claimCause,
			Date datePrimiumStarted, String undewrittenyorn) {
		super();
		this.id = id;
		this.suminsClaim = suminsClaim;
		this.elodgeApplicantInfo = elodgeApplicantInfo;
		this.insCovcur = insCovcur;
		this.insCovcurComents = insCovcurComents;
		this.coverCode = coverCode;
		this.dateOfEvent = dateOfEvent;
		this.claimCondition = claimCondition;
		this.claimCause = claimCause;
		this.datePrimiumStarted = datePrimiumStarted;
		this.undewrittenyorn = undewrittenyorn;
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the suminsClaim
	 */
	public BigDecimal getSuminsClaim() {
		return suminsClaim;
	}

	/**
	 * @param suminsClaim the suminsClaim to set
	 */
	public void setSuminsClaim(BigDecimal suminsClaim) {
		this.suminsClaim = suminsClaim;
	}

	/**
	 * @return the elodgeApplicantInfo
	 */
	public UWApplicantInfoDto getElodgeApplicantInfo() {
		return elodgeApplicantInfo;
	}

	/**
	 * @param elodgeApplicantInfo the elodgeApplicantInfo to set
	 */
	public void setElodgeApplicantInfo(UWApplicantInfoDto elodgeApplicantInfo) {
		this.elodgeApplicantInfo = elodgeApplicantInfo;
	}

	/**
	 * @return the insCovcur
	 */
	public String getInsCovcur() {
		return insCovcur;
	}

	/**
	 * @param insCovcur the insCovcur to set
	 */
	public void setInsCovcur(String insCovcur) {
		this.insCovcur = insCovcur;
	}

	/**
	 * @return the insCovcurComents
	 */
	public String getInsCovcurComents() {
		return insCovcurComents;
	}

	/**
	 * @param insCovcurComents the insCovcurComents to set
	 */
	public void setInsCovcurComents(String insCovcurComents) {
		this.insCovcurComents = insCovcurComents;
	}

	/**
	 * @return the coverCode
	 */
	public String getCoverCode() {
		return coverCode;
	}

	/**
	 * @param coverCode the coverCode to set
	 */
	public void setCoverCode(String coverCode) {
		this.coverCode = coverCode;
	}

	/**
	 * @return the dateOfEvent
	 */
	public Date getDateOfEvent() {
		return dateOfEvent;
	}

	/**
	 * @param dateOfEvent the dateOfEvent to set
	 */
	public void setDateOfEvent(Date dateOfEvent) {
		this.dateOfEvent = dateOfEvent;
	}

	/**
	 * @return the claimCondition
	 */
	public String getClaimCondition() {
		return claimCondition;
	}

	/**
	 * @param claimCondition the claimCondition to set
	 */
	public void setClaimCondition(String claimCondition) {
		this.claimCondition = claimCondition;
	}

	/**
	 * @return the claimCause
	 */
	public String getClaimCause() {
		return claimCause;
	}

	/**
	 * @param claimCause the claimCause to set
	 */
	public void setClaimCause(String claimCause) {
		this.claimCause = claimCause;
	}

	/**
	 * @return the datePrimiumStarted
	 */
	public Date getDatePrimiumStarted() {
		return datePrimiumStarted;
	}

	/**
	 * @param datePrimiumStarted the datePrimiumStarted to set
	 */
	public void setDatePrimiumStarted(Date datePrimiumStarted) {
		this.datePrimiumStarted = datePrimiumStarted;
	}

	/**
	 * @return the undewrittenyorn
	 */
	public String getUndewrittenyorn() {
		return undewrittenyorn;
	}

	/**
	 * @param undewrittenyorn the undewrittenyorn to set
	 */
	public void setUndewrittenyorn(String undewrittenyorn) {
		this.undewrittenyorn = undewrittenyorn;
	}

}
