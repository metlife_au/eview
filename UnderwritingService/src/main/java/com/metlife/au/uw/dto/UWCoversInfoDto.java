/**
 * 
 */
package com.metlife.au.uw.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author 672381
 *
 */
public class UWCoversInfoDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5811768856014607187L;
	private long coverId;
	private Short existingCoverUnits;
	private BigDecimal existingCover;
	private Short additionalCoverUnits;
	private BigDecimal additionalCover;
	private BigDecimal totalRequestedCover;
	private String coverCode;
	private Date createDate=new Date();
	private Date lastUpdateDate=new Date();
	private String existingCoverOption;
	private String additionalCoverOption;
	private String waitingPeriod;
	private String benefitPeriod;
	private String existingWaitingPeriod;
	private String existingBenefitPeriod;

	/**
	 * 
	 */
	public UWCoversInfoDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public long getCoverId() {
		return coverId;
	}

	public void setCoverId(long coverId) {
		this.coverId = coverId;
	}

	public Short getExistingCoverUnits() {
		return existingCoverUnits;
	}

	public void setExistingCoverUnits(Short existingCoverUnits) {
		this.existingCoverUnits = existingCoverUnits;
	}

	public BigDecimal getExistingCover() {
		return existingCover;
	}

	public void setExistingCover(BigDecimal existingCover) {
		this.existingCover = existingCover;
	}

	public Short getAdditionalCoverUnits() {
		return additionalCoverUnits;
	}

	public void setAdditionalCoverUnits(Short additionalCoverUnits) {
		this.additionalCoverUnits = additionalCoverUnits;
	}

	public BigDecimal getAdditionalCover() {
		return additionalCover;
	}

	public void setAdditionalCover(BigDecimal additionalCover) {
		this.additionalCover = additionalCover;
	}

	public BigDecimal getTotalRequestedCover() {
		return totalRequestedCover;
	}

	public void setTotalRequestedCover(BigDecimal totalRequestedCover) {
		this.totalRequestedCover = totalRequestedCover;
	}

	public String getCoverCode() {
		return coverCode;
	}

	public void setCoverCode(String coverCode) {
		this.coverCode = coverCode;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getLastUpdateDate() {
		return lastUpdateDate;
	}

	public void setLastUpdateDate(Date lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}

	public String getExistingCoverOption() {
		return existingCoverOption;
	}

	public void setExistingCoverOption(String existingCoverOption) {
		this.existingCoverOption = existingCoverOption;
	}

	public String getAdditionalCoverOption() {
		return additionalCoverOption;
	}

	public void setAdditionalCoverOption(String additionalCoverOption) {
		this.additionalCoverOption = additionalCoverOption;
	}

	public String getWaitingPeriod() {
		return waitingPeriod;
	}

	public void setWaitingPeriod(String waitingPeriod) {
		this.waitingPeriod = waitingPeriod;
	}

	public String getBenefitPeriod() {
		return benefitPeriod;
	}

	public void setBenefitPeriod(String benefitPeriod) {
		this.benefitPeriod = benefitPeriod;
	}

	public String getExistingWaitingPeriod() {
		return existingWaitingPeriod;
	}

	public void setExistingWaitingPeriod(String existingWaitingPeriod) {
		this.existingWaitingPeriod = existingWaitingPeriod;
	}

	public String getExistingBenefitPeriod() {
		return existingBenefitPeriod;
	}

	public void setExistingBenefitPeriod(String existingBenefitPeriod) {
		this.existingBenefitPeriod = existingBenefitPeriod;
	}

}
