/**
 * 
 */
package com.metlife.au.uw.dto;

import java.io.Serializable;

/**
 * @author 672381
 *
 */
public class CategoryDto implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1787166951689924757L;

	private int categoryId;

	private String categoryName;

	public CategoryDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public int getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

}
