/**
 * 
 */
package com.metlife.au.uw.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.metlife.au.uw.dto.UWApplicantInfoDto;
import com.metlife.au.uw.entity.UWApplicantInfo;
import com.metlife.au.uw.exception.ApplicationException;
import com.metlife.au.uw.service.UnderwritingService;
import com.metlife.au.uw.utils.MapperUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @author 672381
 *
 */

@Api(value = "/api/v1/application")
@RestController
@RequestMapping("/api/v1/application")
public class UnderwritingController {

	private final Logger logger = LoggerFactory.getLogger(UnderwritingController.class.getName());

	private UnderwritingService underwritingService;
	
	/**
	 * @param eLodgementService
	 */
	@Autowired
	public UnderwritingController(UnderwritingService underwritingService) {
		super();
		this.underwritingService = underwritingService;
	}

	@ApiOperation(value = "/lodgement", notes = "Save New UnderWritting")
	@PostMapping("/lodgement")
	public ResponseEntity<UWApplicantInfoDto> saveUnderWritting(@RequestBody UWApplicantInfoDto uWApplicantInfoDto) {
		logger.info("ELodgementController :  saveUnderWritting start");
		ResponseEntity<UWApplicantInfoDto> resEntity = new ResponseEntity<>(HttpStatus.NO_CONTENT);
		try {
			
			UWApplicantInfoDto applicationInfo = underwritingService.saveUnderwirrting(uWApplicantInfoDto);		
			
			resEntity = new ResponseEntity<>(applicationInfo, HttpStatus.OK);
		} catch (ApplicationException ex) {
			resEntity = new ResponseEntity<>(new UWApplicantInfoDto(), HttpStatus.EXPECTATION_FAILED);
			logger.info("ELodgementController :  saveUnderWritting catch block : " + ex.getMessage());
		} catch (Exception ex) {
			resEntity = new ResponseEntity<>(new UWApplicantInfoDto(), HttpStatus.EXPECTATION_FAILED);
			logger.info("ELodgementController :  saveUnderWritting catch block : " + ex.getMessage());
		}
		logger.info("ELodgementController :  saveUnderWritting end");
		return resEntity;
	}
}
