/**
 * 
 */
package com.metlife.au.uw.dto;

import java.io.Serializable;

import javax.persistence.Column;

/**
 * @author 672381
 *
 */
public class CoverTypeDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 667021883335487164L;
	private int coverId;
	private String covercode;
	private String coverName;

	public CoverTypeDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getCovercode() {
		return covercode;
	}

	public void setCovercode(String covercode) {
		this.covercode = covercode;
	}

	public String getCoverName() {
		return coverName;
	}

	public void setCoverName(String coverName) {
		this.coverName = coverName;
	}

	public int getCoverId() {
		return coverId;
	}

	public void setCoverId(int coverId) {
		this.coverId = coverId;
	}

}
