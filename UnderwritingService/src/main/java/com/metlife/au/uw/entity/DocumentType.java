/**
 * 
 */
package com.metlife.au.uw.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author 672381
 *
 */

@Entity
@Table(name = "DOCUMENT_TYPE", schema = "MTLWPS")
public class DocumentType implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2476706005320106549L;
	
	@Id	
	@Column(name = "DOCUMENT_TYPE_ID", nullable=false)
	private int documentTypeID;

	@Column(name = "DOCUMENT_TYPE_NAME")
	private String documentTypeName;

	public DocumentType() {
		super();
		// TODO Auto-generated constructor stub
	}

	public int getDocumentTypeID() {
		return documentTypeID;
	}

	public void setDocumentTypeID(int documentTypeID) {
		this.documentTypeID = documentTypeID;
	}

	public String getDocumentTypeName() {
		return documentTypeName;
	}

	public void setDocumentTypeName(String documentTypeName) {
		this.documentTypeName = documentTypeName;
	}
	
	

}
