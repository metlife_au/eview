package com.metlife.au.uwreport.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.metlife.au.uwreport.dto.ReportFilter;
import com.metlife.au.uwreport.dto.UWSearchFilter;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @author 672381
 *
 */

@Api(value = "/api/v1/uwreports")
@RestController
@RequestMapping("/api/v1/uwreports")
public class ReportsController {

	private final Logger logger = LoggerFactory.getLogger(ReportsController.class.getName());

	@Autowired
	private LoadBalancerClient loadBalancer;

	@Autowired
	RestTemplate restTemplate;

	@Value("${uwStatusReportURL}")
	private String uwStatusURL;

	@Value("${uwSLAReportURL}")
	private String uwSLAReportURL;

	@Value("${uwRequirementReportURL}")
	private String uwRequirementReportURL;

	@Value("${uwSearchURL}")
	private String uwSearchURL;

	@ApiOperation(value = "/uwStatusReport", notes = "To retrive the UW Status Report")
	@PostMapping("/uwStatusReport")
	public ResponseEntity<String> getUWStatusReport(@RequestBody ReportFilter reportFilter) {
		ResponseEntity<String> resEntity = new ResponseEntity<>(HttpStatus.NO_CONTENT);
		logger.info("ReportsController :  getUWStatusReport start");
		try {
			HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.set("Content-Type", "application/json");
			ObjectMapper objectMap = new ObjectMapper();
			String json = objectMap.writeValueAsString(reportFilter);
			HttpEntity<String> httpEntity = new HttpEntity<String>(json, httpHeaders);
			ResponseEntity<String> responseEntity = restTemplate.postForEntity(uwSLAReportURL, httpEntity,
					String.class);
			logger.info("ReportsController : " + responseEntity.getBody());
			resEntity = new ResponseEntity<>(responseEntity.getBody(), HttpStatus.OK);
		} catch (Exception e) {
			resEntity = new ResponseEntity<>("Failed", HttpStatus.EXPECTATION_FAILED);
		}
		logger.info("ReportsController :  getUWStatusReport end");
		return resEntity;
	}

	@ApiOperation(value = "/uwSLAReport", notes = "To retrive the UW SLA Report")
	@PostMapping("/uwSLAReport")
	public ResponseEntity<String> getUWSLAReport(@RequestBody ReportFilter reportFilter) {
		ResponseEntity<String> resEntity = new ResponseEntity<>(HttpStatus.NO_CONTENT);
		logger.info("ReportsController :  getUWSLAReport start");
		try {
			HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.set("Content-Type", "application/json");
			ObjectMapper objectMap = new ObjectMapper();
			String json = objectMap.writeValueAsString(reportFilter);
			HttpEntity<String> httpEntity = new HttpEntity<String>(json, httpHeaders);
			ResponseEntity<String> responseEntity = restTemplate.postForEntity(uwStatusURL, httpEntity, String.class);
			logger.info("ReportsController : " + responseEntity.getBody());
			resEntity = new ResponseEntity<>(responseEntity.getBody(), HttpStatus.OK);
		} catch (Exception e) {
			resEntity = new ResponseEntity<>("Failed", HttpStatus.EXPECTATION_FAILED);
		}
		logger.info("ReportsController :  getUWSLAReport end");
		return resEntity;
	}

	@ApiOperation(value = "/uwRequirementReport", notes = "To retrive the UW Requirement Report")
	@PostMapping("/uwRequirementReport")
	public ResponseEntity<String> getUWRequirementReport(@RequestBody ReportFilter reportFilter) {
		ResponseEntity<String> resEntity = new ResponseEntity<>(HttpStatus.NO_CONTENT);
		logger.info("ReportsController :  getUWRequirementReport start");
		try {
			HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.set("Content-Type", "application/json");
			ObjectMapper objectMap = new ObjectMapper();
			String json = objectMap.writeValueAsString(reportFilter);
			HttpEntity<String> httpEntity = new HttpEntity<String>(json, httpHeaders);
			ResponseEntity<String> responseEntity = restTemplate.postForEntity(uwRequirementReportURL, httpEntity,
					String.class);
			logger.info("ReportsController : " + responseEntity.getBody());
			resEntity = new ResponseEntity<>(responseEntity.getBody(), HttpStatus.OK);
		} catch (Exception e) {
			resEntity = new ResponseEntity<>("Failed", HttpStatus.EXPECTATION_FAILED);
		}
		logger.info("ReportsController :  getUWRequirementReport end");
		return resEntity;
	}

	@ApiOperation(value = "/uwSearch", notes = "Elodgement Search")
	@PostMapping("/uwSearch")
	public ResponseEntity<String> getUWSearch(@RequestBody UWSearchFilter uwSearchFilter) {
		ResponseEntity<String> resEntity = new ResponseEntity<>(HttpStatus.NO_CONTENT);
		logger.info("ReportsController :  getUWSearch start");
		try {
			HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.set("Content-Type", "application/json");
			ObjectMapper objectMap = new ObjectMapper();
			String json = objectMap.writeValueAsString(uwSearchFilter);
			HttpEntity<String> httpEntity = new HttpEntity<String>(json, httpHeaders);
			ResponseEntity<String> responseEntity = restTemplate.postForEntity(uwSearchURL, httpEntity, String.class);
			logger.info("ReportsController : " + responseEntity.getBody());
			resEntity = new ResponseEntity<>(responseEntity.getBody(), HttpStatus.OK);
		} catch (Exception e) {
			resEntity = new ResponseEntity<>("Failed", HttpStatus.EXPECTATION_FAILED);
		}
		logger.info("ReportsController :  getUWSearch end");
		return resEntity;
	}

}
