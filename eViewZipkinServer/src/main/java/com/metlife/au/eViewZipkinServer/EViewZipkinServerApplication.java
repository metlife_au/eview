package com.metlife.au.eViewZipkinServer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import zipkin2.server.internal.EnableZipkinServer;

@SpringBootApplication
@EnableZipkinServer
public class EViewZipkinServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(EViewZipkinServerApplication.class, args);
	}

}

