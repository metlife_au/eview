package com.metlife.au.eViewEurekaServer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;


@SpringBootApplication
@EnableEurekaServer
public class EViewEurekaServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(EViewEurekaServerApplication.class, args);
	}

}

