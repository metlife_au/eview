/**
 * 
 */
package com.metlife.au.claims.reports.aspects;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * @author 672381
 *
 */

@Aspect
@Component
public class LoggingAspect {

	private final Logger logger = LoggerFactory.getLogger(LoggingAspect.class);

	@Before("within(com.metlife.au.claims.reports..*)")
	public void beforeLog(JoinPoint joinPoint) {
		logger.info("Before service method in : " + joinPoint.getSignature().getName());
	}

	@After("within(com.metlife.au.claims.reports..*)")
	public void afterLog(JoinPoint joinPoint) {
		logger.info("After service method in : " + joinPoint.getSignature().getName());
	}

	@AfterReturning(pointcut = "within(com.metlife.au.claims.reports..*)", returning = "result")
	public void afterReturningLog(JoinPoint joinPoint, Object result) {
		logger.info("After Return service method in : " + joinPoint.getSignature().getName() + " : " + result);
	}

	@AfterThrowing(pointcut = "within(com.metlife.au.claims.reports..*)", throwing = "error")
	public void afterThrowLog(JoinPoint joinPoint, Throwable error) {
		logger.info("After Throw service method in : " + joinPoint.getSignature().getName() + " : " + error);
	}

}
