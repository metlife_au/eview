package com.metlife.au.claims.reports.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @author 672381
 *
 */

@Api(value = "/api/v1/claimreports")
@RestController
@RequestMapping("/api/v1/claimreports")
public class ReportsController {

	private final Logger logger = LoggerFactory.getLogger(ReportsController.class.getName());

	@Autowired
	RestTemplate restTemplate;

	@Value("${claimsSearchURL}")
	private String claimsSearchURL;

	@ApiOperation(value = "/claimsSearch", notes = "Elodgement Claims Search")
	@PostMapping("/claimsSearch")
	public ResponseEntity<String> getClaimsSearch(
			@RequestBody com.metlife.au.claims.reports.dto.UWSearchFilter uwSearchFilter) {
		ResponseEntity<String> resEntity = new ResponseEntity<>(HttpStatus.NO_CONTENT);
		logger.info("ReportsController :  getClaimsSearch start");
		try {
			HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.set("Content-Type", "application/json");
			ObjectMapper objectMap = new ObjectMapper();
			String json = objectMap.writeValueAsString(uwSearchFilter);
			HttpEntity<String> httpEntity = new HttpEntity<String>(json, httpHeaders);
			ResponseEntity<String> responseEntity = restTemplate.postForEntity(claimsSearchURL, httpEntity,
					String.class);
			logger.info("ReportsController : " + responseEntity.getBody());
			resEntity = new ResponseEntity<>(responseEntity.getBody(), HttpStatus.OK);
		} catch (Exception e) {
			resEntity = new ResponseEntity<>("Failed", HttpStatus.EXPECTATION_FAILED);
		}
		logger.info("ReportsController :  getClaimsSearch end");
		return resEntity;
	}

}
