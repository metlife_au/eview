/**
 * 
 */
package com.metlife.au.user.dto;

import java.util.Map;

/**
 * @author 672381
 *
 */
public interface User<T> {

    T getInternalId();

    T getUsername();

    String getFirstname();

    String getLastname();

    String getEmail();

    String getSource();

    Map<String, Object> getProperties();
    
     String getCn();
}