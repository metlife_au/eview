/**
 * 
 */
package com.metlife.au.user.dto;

/**
 * @author Admin.inbasekaranp
 *
 */
public class Status {
	
	private String response;
	
	private String message;

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	

}
