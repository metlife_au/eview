/**
 * 
 */
package com.metlife.au.user.entity;

import java.util.Map;

import com.metlife.au.user.dto.User;

/**
 * @author 672381
 *
 */
public class LdapUser implements User<String> {

	 private final String username;
	    private String firstname, lastname, email, dn,cn;
	    private Map<String, Object> properties;

	    public LdapUser(String username) {
	        this.username = username;
	    }

	    @Override
	    public String getInternalId() {
	        return dn;
	    }

	    @Override
	    public String getEmail() {
	        return email;
	    }

	    public void setEmail(String email) {
	        this.email = email;
	    }

	    @Override
	    public String getFirstname() {
	        return firstname;
	    }

	    public void setFirstname(String firstname) {
	        this.firstname = firstname;
	    }

	    @Override
	    public String getUsername() {
	        return username;
	    }

	    @Override
	    public String getLastname() {
	        return lastname;
	    }

	    public void setLastname(String lastname) {
	        this.lastname = lastname;
	    }

	    public String getDn() {
	        return dn;
	    }

	    public void setDn(String dn) {
	        this.dn = dn;
	    }

	    @Override
	    public Map<String, Object> getProperties() {
	        return properties;
	    }

	    public void setProperties(Map<String, Object> properties) {
	        this.properties = properties;
	    }

		@Override
		public String getSource() {
			// TODO Auto-generated method stub
			return null;
		}

		public String getCn() {
			return cn;
		}

		public void setCn(String cn) {
			this.cn = cn;
		}

	    
	    
}
