/**
 * 
 */
package com.metlife.au.user.controller;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.List;

import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.DirContext;
import javax.naming.directory.ModificationItem;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ldap.core.AttributesMapper;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.filter.AndFilter;
import org.springframework.ldap.filter.EqualsFilter;
import org.springframework.ldap.filter.WhitespaceWildcardsFilter;
import org.springframework.ldap.query.LdapQuery;
import org.springframework.ldap.query.LdapQueryBuilder;
import org.springframework.ldap.query.SearchScope;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.metlife.au.user.dto.Status;
import com.metlife.au.user.dto.User;
import com.metlife.au.user.entity.LdapUser;
import com.metlife.au.user.util.PasswordGenerator;

/**
 * @author 672381
 *
 */
@RestController
@RequestMapping("/api/v1/application/user")
public class UserController {

	private final Logger logger = LoggerFactory.getLogger(UserController.class.getName());
	
	@Value("${auth.ldap.dn}")
	private String LDAP_DN;
	
	@Value("${auth.ldap.user.dn}")
	private String LDAP_USER_DN;
	
	@Autowired
	private LdapTemplate ldapTemplate;
	
	@Autowired
	private PasswordGenerator passwordGenerator;
	
	@RequestMapping(value="/{userid}/{emailid}/forgot", method = RequestMethod.GET)
	public ResponseEntity<Status> forgottPassword(@PathVariable("userid") String userid,@PathVariable("emailid") String emailid){
		ResponseEntity<Status> resEntity = new ResponseEntity<>(HttpStatus.NO_CONTENT);
		String password="";
		Status status=new Status();
		try {
				
			 	AndFilter filter = new AndFilter();
		        filter.and(new EqualsFilter("objectclass", "person"));
		        filter.and(new WhitespaceWildcardsFilter("uid", userid));

		        LdapQuery ldapQuery = LdapQueryBuilder
		                .query()
		                .base(LDAP_USER_DN)
		                .countLimit(20)
		                .timeLimit(5000)
		                .searchScope(SearchScope.SUBTREE)
		                .attributes("uid","givenname", "sn", "mail","cn")
		                .filter(filter);
		        List<User> userlist=ldapTemplate.search(ldapQuery, new UserAttributesMapper());		        
		        if(userlist!=null && userlist.size()>0) {
		        	 password=passwordGenerator.passwordGeneration(8);
		        	 ModificationItem repitemFlag = new ModificationItem(DirContext.REPLACE_ATTRIBUTE, new BasicAttribute("pwdForceChange", "TRUE"));
		        	 ModificationItem repitem = new ModificationItem(DirContext.REPLACE_ATTRIBUTE, new BasicAttribute("userPassword", password));
		        	 String dn="uid="+userlist.get(0).getUsername()+","+LDAP_USER_DN;
					 ldapTemplate.modifyAttributes(dn, new ModificationItem[] { repitemFlag,repitem });
					 status.setResponse("Success");
					 status.setMessage("Password reset is success. Please check your email for your new password " +password);
		        }    
		        status.setResponse("Failure");
			    status.setMessage("The request user id is not in System");    
		    
			resEntity = new ResponseEntity<>(status, HttpStatus.OK);
		} catch (Exception e) {
			status.setResponse("Failure");
		    status.setMessage("Password reset is failed. Please contact your application support team.");
			resEntity = new ResponseEntity<>(status, HttpStatus.EXPECTATION_FAILED);
		}
		
		return resEntity;
	}
	
	@RequestMapping(value="/{userid}/reset/{password}", method = RequestMethod.GET)
	public ResponseEntity<String> resetPassword(@PathVariable("userid") String userid,@PathVariable("password") String newPassword){
		ResponseEntity<String> resEntity = new ResponseEntity<>(HttpStatus.NO_CONTENT);
		String password="";
		Status status=new Status();
		try {
			
			 	AndFilter filter = new AndFilter();
		        filter.and(new EqualsFilter("objectclass", "person"));
		        filter.and(new WhitespaceWildcardsFilter("uid", userid));

		        LdapQuery ldapQuery = LdapQueryBuilder
		                .query()
		                .base(LDAP_USER_DN)
		                .countLimit(20)
		                .timeLimit(5000)
		                .searchScope(SearchScope.SUBTREE)
		                .attributes("uid","givenname", "sn", "mail","cn")
		                .filter(filter);
		        List<User> userlist=ldapTemplate.search(ldapQuery, new UserAttributesMapper());		        
		        if(userlist!=null && userlist.size()>0) {		        	
		        	 ModificationItem repitemFlag = new ModificationItem(DirContext.REPLACE_ATTRIBUTE, new BasicAttribute("pwdForceChange", "FALSE"));
		        	 ModificationItem repitem = new ModificationItem(DirContext.REPLACE_ATTRIBUTE, new BasicAttribute("userPassword", newPassword));
		        	 String dn="uid="+userlist.get(0).getUsername()+","+LDAP_USER_DN;
					 ldapTemplate.modifyAttributes(dn, new ModificationItem[] { repitemFlag,repitem });
					 status.setResponse("Success");
					 status.setMessage("Password reset is success. Please check your email for your new password ");
		        }       
		        status.setResponse("Failure");
			    status.setMessage("The request user id is not in System");    
			resEntity = new ResponseEntity<>(newPassword, HttpStatus.OK);
		} catch (Exception e) {
			status.setResponse("Failure");
		    status.setMessage("Password reset is failed. Please contact your application support team.");
			resEntity = new ResponseEntity<>("Failed", HttpStatus.EXPECTATION_FAILED);
		}
		
		return resEntity;
	}
	
	private class UserAttributesMapper implements AttributesMapper<User> {
        public User mapFromAttributes(Attributes attrs) throws NamingException {
            LdapUser user = new LdapUser(attributeValue(attrs, "uid"));
            user.setFirstname(attributeValue(attrs, "givenname"));
            user.setLastname(attributeValue(attrs, "sn"));
            user.setEmail(attributeValue(attrs, "mail"));
            user.setCn(attributeValue(attrs, "cn"));
            return user;
        }
    }
	
	 private String attributeValue(Attributes attrs, String attributeId) throws NamingException {
	        Attribute attr = attrs.get(attributeId);
	        if (attr != null) {
	            return (String) attr.get();
	        }

	        return null;
	    }
	
	
}

