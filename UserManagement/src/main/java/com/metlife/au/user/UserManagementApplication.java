package com.metlife.au.user;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.security.ldap.DefaultSpringSecurityContextSource;

@SpringBootApplication
@EnableEurekaClient
public class UserManagementApplication {

	@Value("${auth.ldap.uri}")
	private String LDAP_URI;
	@Value("${auth.ldap.dn}")
	private String LDAP_DN;
	@Value("${auth.ldap.manager.id}")
	private String LDAP_MGR_ID;
	@Value("${auth.ldap.manager.password}")
	private String LDAP_MGR_PAS;
	
	@Bean
	public DefaultSpringSecurityContextSource contextSource() {
		DefaultSpringSecurityContextSource context=new DefaultSpringSecurityContextSource(this.LDAP_URI);	
		context.setBase(this.LDAP_DN);
		context.setUserDn(this.LDAP_MGR_ID);
		context.setPassword(this.LDAP_MGR_PAS);		
		context.setAnonymousReadOnly(true);
		return context;
	}

	@Bean
	public LdapTemplate ldapTemplate() {
		return new LdapTemplate(contextSource());
	}
	
	public static void main(String[] args) {
		SpringApplication.run(UserManagementApplication.class, args);
	}

}
