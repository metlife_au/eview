/**
 * 
 */
package com.metlife.au.user.util;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.metlife.au.user.controller.UserController;

/**
 * @author 672381
 *
 */
@Component
public class PasswordGenerator {

	private final Logger logger = LoggerFactory.getLogger(UserController.class.getName());
	
	 public String passwordGeneration(int length) {
			
	        byte random[] = new byte[length];
	        char characters[] = new char[length];
	        if(length <= 0){
	            length = 8;
	        }
	        try{
	            SecureRandom.getInstance("SHA1PRNG").nextBytes(random);
	        }
	        catch(NoSuchAlgorithmException e){
	            random = (new SecureRandom()).generateSeed(length);
	        }
	        for(int i = 0; i < length; i++)
	            characters[i] = getPrintableChar(random[i]);
	        logger.info("password Generation :" +new String(characters));
	        return new String(characters);
	}

	 public char getPrintableChar(byte num){
     char c;
     label0:{
     	label1:{
             c = (char)(num + 128);
             if(c > '\200'){
                 c -= '\200';
             }
             if(c < '0'){
                 c += '0';
             }
             if(c > '9'){
                 if(c < 'A')
                     break label1;
             }
             if(c > 'Z'){
                 if(c < 'a')
                     break label1;
             }
             if(c <= 'z')
                 break label0;
         }
         c -= '\n';
     }
     return c;
 }
}
