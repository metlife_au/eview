/**
 * 
 */
package com.metlife.au.metbi.dto;

/**
 * @author 672381
 *
 */
public class UWStatusReportDto extends UnderwritingDetailDto {

	private long applicationNo;
	private String fundName;
	private String fundCode;
	private String clientReferenceNo = null;
	private String firstName;
	private String surName;
	private String memberDob;
	private String memberGender;
	private String memberType;
	private String applicationDate;
	private String status;
	private String source;
	private String lastActionDate;
	private String totalDeathCover;
	private String totalTraumaCover;
	private String totalTPDCover;
	private String totalSCICover;
	private String requestWaitPeriod;
	private String requestBenefitPeriod;

	public UWStatusReportDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	
	public long getApplicationNo() {
		return applicationNo;
	}


	public void setApplicationNo(long applicationNo) {
		this.applicationNo = applicationNo;
	}


	public String getFundName() {
		return fundName;
	}


	public void setFundName(String fundName) {
		this.fundName = fundName;
	}


	public String getFundCode() {
		return fundCode;
	}


	public void setFundCode(String fundCode) {
		this.fundCode = fundCode;
	}


	public String getClientReferenceNo() {
		return clientReferenceNo;
	}


	public void setClientReferenceNo(String clientReferenceNo) {
		this.clientReferenceNo = clientReferenceNo;
	}


	public String getFirstName() {
		return firstName;
	}


	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


	public String getSurName() {
		return surName;
	}


	public void setSurName(String surName) {
		this.surName = surName;
	}


	public String getMemberDob() {
		return memberDob;
	}


	public void setMemberDob(String memberDob) {
		this.memberDob = memberDob;
	}


	public String getMemberGender() {
		return memberGender;
	}


	public void setMemberGender(String memberGender) {
		this.memberGender = memberGender;
	}


	public String getMemberType() {
		return memberType;
	}


	public void setMemberType(String memberType) {
		this.memberType = memberType;
	}


	public String getApplicationDate() {
		return applicationDate;
	}


	public void setApplicationDate(String applicationDate) {
		this.applicationDate = applicationDate;
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}


	public String getSource() {
		return source;
	}


	public void setSource(String source) {
		this.source = source;
	}


	public String getLastActionDate() {
		return lastActionDate;
	}


	public void setLastActionDate(String lastActionDate) {
		this.lastActionDate = lastActionDate;
	}


	public String getTotalDeathCover() {
		return totalDeathCover;
	}

	public void setTotalDeathCover(String totalDeathCover) {
		this.totalDeathCover = totalDeathCover;
	}

	public String getTotalTraumaCover() {
		return totalTraumaCover;
	}

	public void setTotalTraumaCover(String totalTraumaCover) {
		this.totalTraumaCover = totalTraumaCover;
	}

	public String getTotalTPDCover() {
		return totalTPDCover;
	}

	public void setTotalTPDCover(String totalTPDCover) {
		this.totalTPDCover = totalTPDCover;
	}

	public String getTotalSCICover() {
		return totalSCICover;
	}

	public void setTotalSCICover(String totalSCICover) {
		this.totalSCICover = totalSCICover;
	}

	public String getRequestWaitPeriod() {
		return requestWaitPeriod;
	}

	public void setRequestWaitPeriod(String requestWaitPeriod) {
		this.requestWaitPeriod = requestWaitPeriod;
	}

	public String getRequestBenefitPeriod() {
		return requestBenefitPeriod;
	}

	public void setRequestBenefitPeriod(String requestBenefitPeriod) {
		this.requestBenefitPeriod = requestBenefitPeriod;
	}

}
