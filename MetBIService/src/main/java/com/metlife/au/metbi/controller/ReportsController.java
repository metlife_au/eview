package com.metlife.au.metbi.controller;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.metlife.au.metbi.constants.ReportConstants;
import com.metlife.au.metbi.dto.ReportFilter;
import com.metlife.au.metbi.dto.UWDrillDownReport;
import com.metlife.au.metbi.dto.UWRequirementReportDto;
import com.metlife.au.metbi.dto.UWSLAReportDto;
import com.metlife.au.metbi.dto.UWSearchFilter;
import com.metlife.au.metbi.dto.UWSearchResult;
import com.metlife.au.metbi.dto.UWStatusReportDto;
import com.metlife.au.metbi.dto.UnderwritingDetailDto;
import com.metlife.au.metbi.entity.UnderwritingDetail;
import com.metlife.au.metbi.exception.ApplicationException;
import com.metlife.au.metbi.service.ReportsService;
import com.metlife.au.metbi.utils.ReportsMapper;

import cz.jirutka.rsql.parser.RSQLParser;
import cz.jirutka.rsql.parser.ast.Node;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @author 672381
 *
 */

@Api(value = "/api/v1/report/")
@RestController
@RequestMapping("/api/v1/report/")
public class ReportsController {

	private final Logger logger = LoggerFactory.getLogger(ReportsController.class.getName());

	private ReportsService reportsService;

	private ReportsMapper reportsMapper;

	@Autowired
	public ReportsController(ReportsService reportsService, ReportsMapper reportsMapper) {
		this.reportsService = reportsService;
		this.reportsMapper = reportsMapper;
	}

	@ApiOperation(value = "/status", notes = "To retrive the UW Status Report")
	@GetMapping("/status")
	public ResponseEntity<List<UWStatusReportDto>> getUWStatusReport(
			@RequestParam(value = "searchkey") String queryParam,
			@RequestParam(value = "offset", required = false) int offset,
			@RequestParam(value = "limit", required = false) int limit,
			@RequestParam(value = "orderby", required = false) String orderby,
			@RequestParam(value = "order", required = false) String order) {
		logger.info("ReportsController :  getUWStatusReport start");
		List<UWStatusReportDto> reportDetails = new ArrayList<UWStatusReportDto>();
		ResponseEntity<List<UWStatusReportDto>> resEntity = new ResponseEntity<>(HttpStatus.NO_CONTENT);
		try {
			ReportFilter reportFilter = new ReportFilter();
			reportFilter.setOffset(offset);
			reportFilter.setLimit(limit);
			reportFilter.setOrderby(orderby);
			reportFilter.setOrder(order);
			List<UnderwritingDetail> reportEntityDetails = reportsService.getUWStatusDetails(queryParam,reportFilter);
			reportDetails = reportsMapper.uwStatusConversion(reportEntityDetails);
			resEntity = new ResponseEntity<>(reportDetails, HttpStatus.OK);
		} catch (ApplicationException ex) {
			resEntity = new ResponseEntity<>(reportDetails, HttpStatus.EXPECTATION_FAILED);
			logger.info("ReportsController :  getUWStatusReport catch block : " + ex.getMessage());
		} catch (Exception ex) {
			resEntity = new ResponseEntity<>(reportDetails, HttpStatus.EXPECTATION_FAILED);
			logger.info("ReportsController :  getUWStatusReport catch block : " + ex.getMessage());
		}
		logger.info("ReportsController :  getUWStatusReport end");
		return resEntity;
	}

	@ApiOperation(value = "/sla", notes = "To retrive the UW SLA Report")
	@GetMapping("/sla")
	public ResponseEntity<List<UWSLAReportDto>> getUWSLAReport(@RequestParam(value = "searchkey") String queryParam,
			@RequestParam(value = "offset", required = false) int offset,
			@RequestParam(value = "limit", required = false) int limit,
			@RequestParam(value = "orderby", required = false) String orderby,
			@RequestParam(value = "order", required = false) String order) {
		logger.info("ReportsController :  getUWSLAReport start");
		List<UWSLAReportDto> reportDetails = new ArrayList<UWSLAReportDto>();
		ResponseEntity<List<UWSLAReportDto>> resEntity = new ResponseEntity<>(HttpStatus.NO_CONTENT);
		try {
			ReportFilter reportFilter = new ReportFilter();
			reportFilter.setOffset(offset);
			reportFilter.setLimit(limit);
			reportFilter.setOrderby(orderby);
			reportFilter.setOrder(order);
			List<UnderwritingDetail> reportEntityDetails = reportsService.getUWSLADetails(queryParam, reportFilter);
			reportDetails = reportsMapper.uwSLAConversion(reportEntityDetails);
			resEntity = new ResponseEntity<>(reportDetails, HttpStatus.OK);
		} catch (ApplicationException ex) {
			resEntity = new ResponseEntity<>(reportDetails, HttpStatus.EXPECTATION_FAILED);
			logger.info("ReportsController :  getUWSLAReport catch block : " + ex.getMessage());
		} catch (Exception ex) {
			resEntity = new ResponseEntity<>(reportDetails, HttpStatus.EXPECTATION_FAILED);
			logger.info("ReportsController :  getUWSLAReport catch block : " + ex.getMessage());
		}
		logger.info("ReportsController :  getUWSLAReport end");
		return resEntity;
	}

	@ApiOperation(value = "/requirement", notes = "To retrive the UW Requirement Report")
	@GetMapping("/requirement")
	public ResponseEntity<List<UWRequirementReportDto>> getUWRequirementReport(@RequestParam(value = "searchkey") String queryParam,
			@RequestParam(value = "offset", required = false) int offset,
			@RequestParam(value = "limit", required = false) int limit,
			@RequestParam(value = "orderby", required = false) String orderby,
			@RequestParam(value = "order", required = false) String order) {
		logger.info("ReportsController :  getUWRequirementReport start");
		List<UWRequirementReportDto> reportDetails = new ArrayList<UWRequirementReportDto>();
		ResponseEntity<List<UWRequirementReportDto>> resEntity = new ResponseEntity<>(HttpStatus.NO_CONTENT);
		try {
			ReportFilter reportFilter = new ReportFilter();
			reportFilter.setOffset(offset);
			reportFilter.setLimit(limit);
			reportFilter.setOrderby(orderby);
			reportFilter.setOrder(order);
			List<UnderwritingDetail> reportEntityDetails = reportsService.getUWRequirementReport(queryParam, reportFilter);
			reportDetails = reportsMapper.uwRequirementConversion(reportEntityDetails);
			resEntity = new ResponseEntity<>(reportDetails, HttpStatus.OK);
		} catch (ApplicationException ex) {
			resEntity = new ResponseEntity<>(reportDetails, HttpStatus.EXPECTATION_FAILED);
			logger.info("ReportsController :  getUWRequirementReport catch block : " + ex.getMessage());
		} catch (Exception ex) {
			resEntity = new ResponseEntity<>(reportDetails, HttpStatus.EXPECTATION_FAILED);
			logger.info("ReportsController :  getUWRequirementReport catch block : " + ex.getMessage());
		}
		logger.info("ReportsController :  getUWRequirementReport end");
		return resEntity;
	}
	
	
	
	@ApiOperation(value = "/drilldown/status", notes = "To retrive the UW Requirement Report using funcode-")
	@GetMapping("/drilldown/status")
	public ResponseEntity<UWDrillDownReport> getUWDrillDownStatusReport(@RequestParam(value = "fundcode", required = false) String fundcode,@RequestParam(value = "applicationNo", required = false) long applicationNo) {
		logger.info("ReportsController :  getUWDrillDownStatusReport start");
		UWDrillDownReport uwStatusReportDtoByID = new UWDrillDownReport();
		ResponseEntity<UWDrillDownReport> resEntity = new ResponseEntity<>(HttpStatus.NO_CONTENT);
		try {
			
		 uwStatusReportDtoByID = reportsService.getUWDrillDownReport(fundcode,applicationNo,ReportConstants.STATUS_EVENT_HISTORY);
			resEntity = new ResponseEntity<>(uwStatusReportDtoByID, HttpStatus.OK);
		} catch (Exception ex) {
			resEntity = new ResponseEntity<>(uwStatusReportDtoByID, HttpStatus.EXPECTATION_FAILED);
			logger.info("ReportsController :  getUWRequirementReport catch block : " + ex.getMessage());
		}
		logger.info("ReportsController :  getUWRequirementReport end");
		return resEntity;
	}
	
	@ApiOperation(value = "/drilldown/requirement", notes = "To retrive the UW Requirement Report using funcode-")
	@GetMapping("/drilldown/requirement")
	public ResponseEntity<UWDrillDownReport> getUWDrillDownRequirementReport(@RequestParam(value = "fundcode", required = false) String fundcode,@RequestParam(value = "applicationNo", required = false) long applicationNo) {
		logger.info("ReportsController :  getUWDrillDownStatusReport start");
		UWDrillDownReport uwStatusReportDtoByID = new UWDrillDownReport();
		ResponseEntity<UWDrillDownReport> resEntity = new ResponseEntity<>(HttpStatus.NO_CONTENT);
		try {
			
			 uwStatusReportDtoByID = reportsService.getUWDrillDownReport(fundcode,applicationNo,ReportConstants.REQUIREMENT_EVENT_HISTORY);
			resEntity = new ResponseEntity<>(uwStatusReportDtoByID, HttpStatus.OK);
		} catch (Exception ex) {
			resEntity = new ResponseEntity<>(uwStatusReportDtoByID, HttpStatus.EXPECTATION_FAILED);
			logger.info("ReportsController :  getUWRequirementReport catch block : " + ex.getMessage());
		}
		logger.info("ReportsController :  getUWRequirementReport end");
		return resEntity;
	}

}
