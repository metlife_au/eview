/**
 * 
 */
package com.metlife.au.metbi.entity;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * @author 672381
 *
 */
@Entity
@Table(name = "EVIEW_UW_EVENT", schema = "DBUSDWW")
public class UnderwritingEvent {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "EVENTID")
	private int eventId;

	@Column(name = "UWEVENTDESCRIPTION")
	private String eventDesc;

	@Column(name = "LASTUPDDATE")
	private String lastUpdatedDate;

	@OneToMany(mappedBy = "underwritingEvent", orphanRemoval = true)
	private Set<UnderwritingAction> underwritingAction;

	/**
	 * 
	 */
	public UnderwritingEvent() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param eventId
	 * @param eventDesc
	 * @param lastUpdatedDate
	 * @param underwritingAction
	 */
	public UnderwritingEvent(int eventId, String eventDesc, String lastUpdatedDate) {
		super();
		this.eventId = eventId;
		this.eventDesc = eventDesc;
		this.lastUpdatedDate = lastUpdatedDate;

	}

	/**
	 * @return the eventId
	 */
	public int getEventId() {
		return eventId;
	}

	/**
	 * @param eventId the eventId to set
	 */
	public void setEventId(int eventId) {
		this.eventId = eventId;
	}

	/**
	 * @return the eventDesc
	 */
	public String getEventDesc() {
		return eventDesc;
	}

	/**
	 * @param eventDesc the eventDesc to set
	 */
	public void setEventDesc(String eventDesc) {
		this.eventDesc = eventDesc;
	}

	/**
	 * @return the lastUpdatedDate
	 */
	public String getLastUpdatedDate() {
		return lastUpdatedDate;
	}

	/**
	 * @param lastUpdatedDate the lastUpdatedDate to set
	 */
	public void setLastUpdatedDate(String lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}

	/**
	 * @return the underwritingAction
	 */
	public Set<UnderwritingAction> getUnderwritingAction() {
		return underwritingAction;
	}

	/**
	 * @param underwritingAction the underwritingAction to set
	 */
	public void setUnderwritingAction(Set<UnderwritingAction> underwritingAction) {
		this.underwritingAction = underwritingAction;
	}

}
