/**
 * 
 */
package com.metlife.au.metbi.dto;

/**
 * @author 672381
 *
 */
public class UnderwritingEventDto {

	private int eventId;
	private String eventDesc;
	private String lastUpdatedDate;

	/**
	 * 
	 */
	public UnderwritingEventDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param eventId
	 * @param eventDesc
	 * @param lastUpdatedDate
	 */
	public UnderwritingEventDto(int eventId, String eventDesc, String lastUpdatedDate) {
		super();
		this.eventId = eventId;
		this.eventDesc = eventDesc;
		this.lastUpdatedDate = lastUpdatedDate;
	}

	/**
	 * @return the eventId
	 */
	public int getEventId() {
		return eventId;
	}

	/**
	 * @param eventId the eventId to set
	 */
	public void setEventId(int eventId) {
		this.eventId = eventId;
	}

	/**
	 * @return the eventDesc
	 */
	public String getEventDesc() {
		return eventDesc;
	}

	/**
	 * @param eventDesc the eventDesc to set
	 */
	public void setEventDesc(String eventDesc) {
		this.eventDesc = eventDesc;
	}

	/**
	 * @return the lastUpdatedDate
	 */
	public String getLastUpdatedDate() {
		return lastUpdatedDate;
	}

	/**
	 * @param lastUpdatedDate the lastUpdatedDate to set
	 */
	public void setLastUpdatedDate(String lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}

}
