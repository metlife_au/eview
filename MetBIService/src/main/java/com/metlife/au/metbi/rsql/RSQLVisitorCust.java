package com.metlife.au.metbi.rsql;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.jirutka.rsql.parser.ast.AndNode;
import cz.jirutka.rsql.parser.ast.ComparisonNode;
import cz.jirutka.rsql.parser.ast.NoArgRSQLVisitorAdapter;
import cz.jirutka.rsql.parser.ast.Node;
import cz.jirutka.rsql.parser.ast.OrNode;

public class RSQLVisitorCust extends NoArgRSQLVisitorAdapter<RSQLRootExpression> {
	private static final Logger log = LoggerFactory.getLogger(RSQLVisitorCust.class);

	private RSQLRootExpression expression;

	public RSQLVisitorCust() {
		expression = new RSQLRootExpression();
		expression.setParameters(new ArrayList<RSQLParameter>());
	}

	@Override
	public RSQLRootExpression visit(AndNode node) {
		expression.setRootOperator("AND");
		for (Iterator<Node> it = node.iterator(); it.hasNext();) {
			Node child = it.next();
			if(child instanceof AndNode){
				RSQLGroupVisitor visitor = new RSQLGroupVisitor();
				RSQLAndParameter rExpression = new RSQLAndParameter();
				rExpression.setParameters(new ArrayList<RSQLParameter>());
				expression.getParameters().add(rExpression);
				child.accept(visitor, rExpression);
			}
			else if(child instanceof OrNode){
				RSQLGroupVisitor visitor = new RSQLGroupVisitor();
				RSQLOrParameter rExpression = new RSQLOrParameter();
				rExpression.setParameters(new ArrayList<RSQLParameter>());
				expression.getParameters().add(rExpression);
				child.accept(visitor, rExpression);
			}
			else{
				child.accept(this);
			}
		}
		return expression;
	}

	@Override
	public RSQLRootExpression visit(OrNode node) {
		expression.setRootOperator("OR");
		for (Iterator<Node> it = node.iterator(); it.hasNext();) {
			Node child = it.next();
			if(child instanceof AndNode){
				RSQLGroupVisitor visitor = new RSQLGroupVisitor();
				RSQLAndParameter rExpression = new RSQLAndParameter();
				rExpression.setParameters(new ArrayList<RSQLParameter>());
				expression.getParameters().add(rExpression);
				child.accept(visitor, rExpression);
			}
			else if(child instanceof OrNode){
				RSQLGroupVisitor visitor = new RSQLGroupVisitor();
				RSQLOrParameter rExpression = new RSQLOrParameter();
				rExpression.setParameters(new ArrayList<RSQLParameter>());
				expression.getParameters().add(rExpression);
				child.accept(visitor, rExpression);
			}
			else{
				child.accept(this);
			}
		}
		return expression;
	}

	@Override
	public RSQLRootExpression visit(ComparisonNode node) {
		RSQLParameter parameter = new RSQLParameter();
		String attrName = node.getSelector();
		parameter.setParameterName(attrName);
		String symbol = node.getOperator().getSymbol();
		List<String> values = node.getArguments();
		switch (symbol) {
		case "=q=":
			parameter.setParameterSingleValue(values.get(0));
			if ("*".equals(attrName)) {
				parameter.setParameterExpression("SEARCH_ALL");
			} else {
				parameter.setParameterExpression(attrName + ".SEARCH");
			}
			break;
		case "==":
			parameter.setParameterSingleValue(values.get(0));
			parameter.setOperator("EQ");
			parameter.setParameterExpression(attrName + ".EQ");
			break;
		case "=in=":
			parameter.setOperator("IN");
			parameter.setParameterExpression(attrName + ".IN");
			parameter.setParameterMultiValues(values);
			break;
		case "=out=":
			parameter.setOperator("OUT");
			parameter.setParameterExpression(attrName + ".OUT");
			parameter.setParameterMultiValues(values);
			break;
		case "=lt=":
		case "<":
			parameter.setParameterSingleValue(values.get(0));
			parameter.setOperator("LT");
			parameter.setParameterExpression(attrName + ".LT");
			break;
		case "=le=":
		case "<=":
			parameter.setParameterSingleValue(values.get(0));
			parameter.setOperator("LTE");
			parameter.setParameterExpression(attrName + ".LTE");
			break;
		case "=gt=":
		case ">":
			parameter.setParameterSingleValue(values.get(0));
			parameter.setOperator("GT");
			parameter.setParameterExpression(attrName + ".GT");
			break;
		case "=ge=":
		case ">=":
			parameter.setParameterSingleValue(values.get(0));
			parameter.setOperator("GTE");
			parameter.setParameterExpression(attrName + ".GTE");
			break;
		case "=like=":
			parameter.setParameterSingleValue(values.get(0));
			parameter.setOperator("LIKE");
			parameter.setParameterExpression(attrName + ".LIKE");
			break;
		case "!=":
			parameter.setParameterSingleValue(values.get(0));
			parameter.setOperator("NE");
			parameter.setParameterExpression(attrName + ".NE");
			break;
		default:
			throw new RSQLParserException(0, "Unknown RSQL query operator [" + symbol + "]");
		}
		expression.getParameters().add(parameter);
		return expression;
	}

}
