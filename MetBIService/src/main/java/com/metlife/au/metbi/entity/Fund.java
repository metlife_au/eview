/**
 * 
 */
package com.metlife.au.metbi.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author 672381
 *
 */
@Entity
@Table(name = "MQT_CLM_FUND", schema = "DBUSDWW")
public class Fund {

	@Id	
	@Column(name = "FUND_CODE", nullable=false)
	private String fundCode;

	@Column(name = "FUND_NAME")
	private String fundName;

	public Fund() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getFundCode() {
		return fundCode;
	}

	public void setFundCode(String fundCode) {
		this.fundCode = fundCode;
	}

	public String getFundName() {
		return fundName;
	}

	public void setFundName(String fundName) {
		this.fundName = fundName;
	}
	
	
}
