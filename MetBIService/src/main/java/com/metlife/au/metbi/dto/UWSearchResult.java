/**
 * 
 */
package com.metlife.au.metbi.dto;

/**
 * @author 672381
 *
 */
public class UWSearchResult {

	private String fundCode;

	private String firstName;

	private String surName;

	private String gender;

	private String dateOfBirth;

	private String fundName;

	private String productType;

	private String status;

	private String applicationDate;

	private String extMemberNo;

	private String claimNo;

	private String reference;

	private String disabilityDate;

	private String dateSubmitted;

	private String statusCode;

	private String claimType;

	private String sumInsured;

	private String monthlyBenefit;

	private String fundOwnerId;

	/**
	 * 
	 */
	public UWSearchResult() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the fundCode
	 */
	public String getFundCode() {
		return fundCode;
	}

	/**
	 * @param fundCode the fundCode to set
	 */
	public void setFundCode(String fundCode) {
		this.fundCode = fundCode;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the surName
	 */
	public String getSurName() {
		return surName;
	}

	/**
	 * @param surName the surName to set
	 */
	public void setSurName(String surName) {
		this.surName = surName;
	}

	/**
	 * @return the gender
	 */
	public String getGender() {
		return gender;
	}

	/**
	 * @param gender the gender to set
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}

	/**
	 * @return the dateOfBirth
	 */
	public String getDateOfBirth() {
		return dateOfBirth;
	}

	/**
	 * @param dateOfBirth the dateOfBirth to set
	 */
	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	/**
	 * @return the fundName
	 */
	public String getFundName() {
		return fundName;
	}

	/**
	 * @param fundName the fundName to set
	 */
	public void setFundName(String fundName) {
		this.fundName = fundName;
	}

	/**
	 * @return the productType
	 */
	public String getProductType() {
		return productType;
	}

	/**
	 * @param productType the productType to set
	 */
	public void setProductType(String productType) {
		this.productType = productType;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the applicationDate
	 */
	public String getApplicationDate() {
		return applicationDate;
	}

	/**
	 * @param applicationDate the applicationDate to set
	 */
	public void setApplicationDate(String applicationDate) {
		this.applicationDate = applicationDate;
	}

	/**
	 * @return the extMemberNo
	 */
	public String getExtMemberNo() {
		return extMemberNo;
	}

	/**
	 * @param extMemberNo the extMemberNo to set
	 */
	public void setExtMemberNo(String extMemberNo) {
		this.extMemberNo = extMemberNo;
	}

	/**
	 * @return the claimNo
	 */
	public String getClaimNo() {
		return claimNo;
	}

	/**
	 * @param claimNo the claimNo to set
	 */
	public void setClaimNo(String claimNo) {
		this.claimNo = claimNo;
	}

	/**
	 * @return the reference
	 */
	public String getReference() {
		return reference;
	}

	/**
	 * @param reference the reference to set
	 */
	public void setReference(String reference) {
		this.reference = reference;
	}

	/**
	 * @return the disabilityDate
	 */
	public String getDisabilityDate() {
		return disabilityDate;
	}

	/**
	 * @param disabilityDate the disabilityDate to set
	 */
	public void setDisabilityDate(String disabilityDate) {
		this.disabilityDate = disabilityDate;
	}

	/**
	 * @return the dateSubmitted
	 */
	public String getDateSubmitted() {
		return dateSubmitted;
	}

	/**
	 * @param dateSubmitted the dateSubmitted to set
	 */
	public void setDateSubmitted(String dateSubmitted) {
		this.dateSubmitted = dateSubmitted;
	}

	/**
	 * @return the statusCode
	 */
	public String getStatusCode() {
		return statusCode;
	}

	/**
	 * @param statusCode the statusCode to set
	 */
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	/**
	 * @return the claimType
	 */
	public String getClaimType() {
		return claimType;
	}

	/**
	 * @param claimType the claimType to set
	 */
	public void setClaimType(String claimType) {
		this.claimType = claimType;
	}

	/**
	 * @return the sumInsured
	 */
	public String getSumInsured() {
		return sumInsured;
	}

	/**
	 * @param sumInsured the sumInsured to set
	 */
	public void setSumInsured(String sumInsured) {
		this.sumInsured = sumInsured;
	}

	/**
	 * @return the monthlyBenefit
	 */
	public String getMonthlyBenefit() {
		return monthlyBenefit;
	}

	/**
	 * @param monthlyBenefit the monthlyBenefit to set
	 */
	public void setMonthlyBenefit(String monthlyBenefit) {
		this.monthlyBenefit = monthlyBenefit;
	}

	/**
	 * @return the fundOwnerId
	 */
	public String getFundOwnerId() {
		return fundOwnerId;
	}

	/**
	 * @param fundOwnerId the fundOwnerId to set
	 */
	public void setFundOwnerId(String fundOwnerId) {
		this.fundOwnerId = fundOwnerId;
	}

}
