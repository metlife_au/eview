/**
 * 
 */
package com.metlife.au.metbi.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * @author 672381
 *
 */
@Embeddable
public class ClaimStatusEmbedded implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -262597198444720596L;

	@Column(name = "CLAIMSTATUS")
	private String claimSatus;

	@Column(name = "PRODUCTTYPE")
	private String productType;

	/**
	 * 
	 */
	public ClaimStatusEmbedded() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param claimSatus
	 * @param productType
	 */
	public ClaimStatusEmbedded(String claimSatus, String productType) {
		super();
		this.claimSatus = claimSatus;
		this.productType = productType;
	}

	/**
	 * @return the claimSatus
	 */
	public String getClaimSatus() {
		return claimSatus;
	}

	/**
	 * @param claimSatus the claimSatus to set
	 */
	public void setClaimSatus(String claimSatus) {
		this.claimSatus = claimSatus;
	}

	/**
	 * @return the productType
	 */
	public String getProductType() {
		return productType;
	}

	/**
	 * @param productType the productType to set
	 */
	public void setProductType(String productType) {
		this.productType = productType;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((claimSatus == null) ? 0 : claimSatus.hashCode());
		result = prime * result + ((productType == null) ? 0 : productType.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ClaimStatusEmbedded other = (ClaimStatusEmbedded) obj;
		if (claimSatus == null) {
			if (other.claimSatus != null)
				return false;
		} else if (!claimSatus.equals(other.claimSatus))
			return false;
		if (productType == null) {
			if (other.productType != null)
				return false;
		} else if (!productType.equals(other.productType))
			return false;
		return true;
	}

}
