/**
 * 
 */
package com.metlife.au.metbi.dto;

import javax.persistence.Column;

/**
 * @author 672381
 *
 */
public class FundDto {

	private String fundCode;
	
	private String fundName;

	public FundDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getFundCode() {
		return fundCode;
	}

	public void setFundCode(String fundCode) {
		this.fundCode = fundCode;
	}

	public String getFundName() {
		return fundName;
	}

	public void setFundName(String fundName) {
		this.fundName = fundName;
	}
	
	
}
