package com.metlife.au.metbi.dto;

public class UWDrillDownReport {
	
	UWDrillDownApplicationInfo applicationInfo = new UWDrillDownApplicationInfo();
	
	UWDrillDownEventInfo eventInfo = new UWDrillDownEventInfo();
	
	UWDrillDownMemberInfo memberInfo = new UWDrillDownMemberInfo();

	public UWDrillDownApplicationInfo getApplicationInfo() {
		return applicationInfo;
	}

	public void setApplicationInfo(UWDrillDownApplicationInfo applicationInfo) {
		this.applicationInfo = applicationInfo;
	}

	public UWDrillDownEventInfo getEventInfo() {
		return eventInfo;
	}

	public void setEventInfo(UWDrillDownEventInfo eventInfo) {
		this.eventInfo = eventInfo;
	}

	public UWDrillDownMemberInfo getMemberInfo() {
		return memberInfo;
	}

	public void setMemberInfo(UWDrillDownMemberInfo memberInfo) {
		this.memberInfo = memberInfo;
	}

	
	
	
	
	

}
