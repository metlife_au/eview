package com.metlife.au.metbi.dto;

public class UWDrillDownMemberInfo {
	private String firstName;
	private String surName;
	private String memberDob;
	private String memberGender;
	private String memberType;
	private String clientReferenceNo;
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getSurName() {
		return surName;
	}
	public void setSurName(String surName) {
		this.surName = surName;
	}
	public String getMemberDob() {
		return memberDob;
	}
	public void setMemberDob(String memberDob) {
		this.memberDob = memberDob;
	}
	public String getMemberGender() {
		return memberGender;
	}
	public void setMemberGender(String memberGender) {
		this.memberGender = memberGender;
	}
	public String getMemberType() {
		return memberType;
	}
	public void setMemberType(String memberType) {
		this.memberType = memberType;
	}
	public String getClientReferenceNo() {
		return clientReferenceNo;
	}
	public void setClientReferenceNo(String clientReferenceNo) {
		this.clientReferenceNo = clientReferenceNo;
	}


}
