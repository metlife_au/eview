package com.metlife.au.metbi.rsql;



public class RSQLParserException extends RuntimeException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6809117502938054056L;
	


	private final Throwable exception;

	private final int errorCode;
	
	private final String errorMsg;
	
	
	public RSQLParserException(){
		this.errorCode = 0;
		this.errorMsg = "Error occured";
		this.exception = new Exception();
	}
	
	public RSQLParserException(int errorCode, String errorMessage){
		this.errorCode = errorCode;
		this.errorMsg = errorMessage;
		this.exception = new Exception();
	}
	
	public RSQLParserException(int errorCode, String errorMessage, Throwable cause){
		this.errorCode = errorCode;
		this.errorMsg = errorMessage;
		this.exception = cause;
	}
	
	
	public Throwable getException() {
		return exception;
	}

	public int getErrorCode() {
		return errorCode;
	}

	public String getErrorMsg() {
		return errorMsg;
	}


}
