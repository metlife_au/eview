/**
 * 
 */
package com.metlife.au.metbi.constants;

/**
 * @author 672381
 *
 */
public class ReportConstants {

	public static final String ACTION_UWSTATUS_RUN_REPORT = "runUWStatusReport";
	public static final String ACTION_UWSTATUS_EXTRACT = "uwStatusExtract";

	public static final String SQL_DATE_FORMAT = "yyyy-MM-dd";
	public static final String SQL_TIMESTAMP_FORMAT = "yyyy-MM-dd HH:mm:ss";
	public static final String SQL_TIMESTAMP_FORMAT_BEGIN = "yyyy-MM-dd 00:00:00";
	public static final String SQL_TIMESTAMP_FORMAT_END = "yyyy-MM-dd 00:00:00";

	public static final String EQ_UW_STS_OPEN_STATUS = "Open";
	public static final String EQ_UW_STS_CLOSED_STATUS = "Closed";

	public static final String DELIMITER_FWD_SLASH = "/";
	public static final String DELIMITER_DOT = "\\.";
	public static final String DELIMITERDOT = ".";
	public static final String EMPTY_STRING = "";
	public static final String WHITE_SPACE = " ";
	public static final String SINGLE_QUOTES = "'";
	public static final String DOUBLE_QUOTES = "''";
	public static final String COMMA = ", ";
	public static final String CLOSING_BRACES = " )";
	public static final String SEMI_COLON = ";";
	public static final String SEMI_COLON_SPACE = "; ";
	public static final String HTML_BR = "<br/>";
	public static final String EQUALS_WITH_SPACE = " = ";
	public static final String EQUALS = "=";
	public static final String SINGLE_QUOTE = "'";

	public static final String UW_CATEGORY = "Underwriting";
	public static final String CLAIM_CATEGORY = "Claim";

	public static final String QUERY_UWSTATUS_RUN_REPORT = "SELECT distinct  fct FROM UnderwritingDetail fct left join UnderwritingAction act on fct.endorsementId=act.underwritingDetail.endorsementId  WHERE fct.applicationNo > 100000 AND fct.source IS NOT NULL";
	public static final String QUERY_UWSTATUS_EXTRACT = "SELECT distinct  fct FROM UnderwritingDetail fct WHERE fct.applicationNo > 100000 AND fct.source IS NOT NULL";
	public static final String QUERY_UWSLA = "SELECT distinct  fct FROM UnderwritingDetail fct join UnderwritingAction act on fct.endorsementId=act.underwritingDetail.endorsementId join UnderwritingEvent evt on act.underwritingEvent.eventId=evt.eventId WHERE fct.applicationNo > 100000 AND fct.source IS NOT NULL";
	public static final String QUERY_UWSEARCH_RUN_REPORT = "SELECT distinct  fct FROM UnderwritingDetail fct WHERE fct.applicationNo > 100000 AND fct.source IS NOT NULL";
	public static final String QUERY_UWREQUIREMENT = "SELECT distinct  fct FROM UnderwritingDetail fct join UnderwritingAction act on fct.endorsementId=act.underwritingDetail.endorsementId left outer join UnderwritingAction action on action.underwritingDetail.endorsementId=act.underwritingDetail.endorsementId and action.req=act.req and action.underwritingEvent.eventId=1009 and action.startDate=act.startDate where act.underwritingEvent.eventId=1004 and fct.statusCode not in (108,106,110) and action.startDate is null";
	public static final String QUERY_CLAIMS_SEARCH = "SELECT fct FROM ClaimDetails fct, ClaimStatus sts WHERE  fct.claimStatus.claimStatusEmbedded.claimSatus=sts.claimStatusEmbedded.claimSatus and fct.claimStatus.claimStatusEmbedded.productType=sts.claimStatusEmbedded.productType";

public static final String QUERY_UW_REPORT_DRILLDOWN = "select fct from UnderwritingDetail fct where fct.applicationNo = :appno and fct.fundName = :selfundname";
	
	public static final String QUERY_UW_REQUIREMENT_EVENTHISTORY = "select distinct act from UnderwritingAction act join "
			+ "UnderwritingDetail fct on fct.endorsementId=act.underwritingDetail.endorsementId"
			+ " left join UnderwritingReqDoc uwreq on lower(uwreq.requirement) = lower(act.underwritingReqDoc.requirement)  "
			+" left outer join  UnderwritingAction uwaction on act.underwritingDetail.endorsementId = uwaction.underwritingDetail.endorsementId and lower(act.underwritingReqDoc.requirement) = lower(uwaction.underwritingReqDoc.requirement) and uwaction.underwritingEvent.eventId = 1009 and uwaction.startDate >= act.startDate" 
			+ " where act.underwritingEvent.eventId = 1004 and fct.applicationNo = :appno and fct.fundName = :selfundname order by act.startDate desc";
	
	
	
	public static final String QUERY_UW_STATUS_EVENTHISTORY = "select distinct act from UnderwritingAction act join UnderwritingEvent evt on act.underwritingEvent.eventId=evt.eventId " +
			"join UnderwritingDetail fct on fct.endorsementId=act.underwritingDetail.endorsementId " + 
			"left join UnderwritingReqDoc uwreq on lower(uwreq.requirement) = lower(act.underwritingReqDoc.requirement) " + 
			"WHERE fct.applicationNo = :appno AND fct.fundName = :selfundname AND act.underwritingEvent.eventId not in (1002,1103,1005,2101,2102) " + 
			"order by act.sNo desc";
	
	
	
	public static final String STATUS_EVENT_HISTORY = "status";
	public static final String REQUIREMENT_EVENT_HISTORY = "requirement";
	
	
}
