package com.metlife.au.metbi.dto;

public class UWDrillDownApplicationInfo {

	private long applicationNo;
	private String fundName;
	private String fundCode;
	private String applicationDate;
	private String status;
	private String source;
	private String lastActionDate;
	private String category;
	private String totalDeathCover;

	private String totalTraumaCover;

	private String totalTPDCover;

	private String totalSCICover;

	private String requestWaitPeriod;

	private String requestBenefitPeriod;

	private String totDeathCoverDecison;

	private String totTPDCoverDecison;
	private String totTraumaCoverDecison;
	private String totSCICoverDecison;
	private String totDeathCoverLoding;
	private String totTPDCoverLoding;
	private String totTraCoverLoding;
	private String totSCICoverLoding;
	private String extDeathGroupCover;
	private String extTPDGroupCover;
	private String extTraumaGroupCover;
	private String extSCICover;
	private String decisionLetterPath;
	public long getApplicationNo() {
		return applicationNo;
	}
	public void setApplicationNo(long applicationNo) {
		this.applicationNo = applicationNo;
	}
	public String getFundName() {
		return fundName;
	}
	public void setFundName(String fundName) {
		this.fundName = fundName;
	}
	public String getFundCode() {
		return fundCode;
	}
	public void setFundCode(String fundCode) {
		this.fundCode = fundCode;
	}
	public String getApplicationDate() {
		return applicationDate;
	}
	public void setApplicationDate(String applicationDate) {
		this.applicationDate = applicationDate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public String getLastActionDate() {
		return lastActionDate;
	}
	public void setLastActionDate(String lastActionDate) {
		this.lastActionDate = lastActionDate;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	
	public String getTotalDeathCover() {
		return totalDeathCover;
	}
	public void setTotalDeathCover(String totalDeathCover) {
		this.totalDeathCover = totalDeathCover;
	}
	public String getTotalTraumaCover() {
		return totalTraumaCover;
	}
	public void setTotalTraumaCover(String totalTraumaCover) {
		this.totalTraumaCover = totalTraumaCover;
	}
	public String getTotalTPDCover() {
		return totalTPDCover;
	}
	public void setTotalTPDCover(String totalTPDCover) {
		this.totalTPDCover = totalTPDCover;
	}
	public String getTotalSCICover() {
		return totalSCICover;
	}
	public void setTotalSCICover(String totalSCICover) {
		this.totalSCICover = totalSCICover;
	}
	public String getRequestWaitPeriod() {
		return requestWaitPeriod;
	}
	public void setRequestWaitPeriod(String requestWaitPeriod) {
		this.requestWaitPeriod = requestWaitPeriod;
	}
	public String getRequestBenefitPeriod() {
		return requestBenefitPeriod;
	}
	public void setRequestBenefitPeriod(String requestBenefitPeriod) {
		this.requestBenefitPeriod = requestBenefitPeriod;
	}
	public String getTotDeathCoverDecison() {
		return totDeathCoverDecison;
	}
	public void setTotDeathCoverDecison(String totDeathCoverDecison) {
		this.totDeathCoverDecison = totDeathCoverDecison;
	}
	public String getTotTPDCoverDecison() {
		return totTPDCoverDecison;
	}
	public void setTotTPDCoverDecison(String totTPDCoverDecison) {
		this.totTPDCoverDecison = totTPDCoverDecison;
	}
	public String getTotTraumaCoverDecison() {
		return totTraumaCoverDecison;
	}
	public void setTotTraumaCoverDecison(String totTraumaCoverDecison) {
		this.totTraumaCoverDecison = totTraumaCoverDecison;
	}
	public String getTotSCICoverDecison() {
		return totSCICoverDecison;
	}
	public void setTotSCICoverDecison(String totSCICoverDecison) {
		this.totSCICoverDecison = totSCICoverDecison;
	}
	public String getTotDeathCoverLoding() {
		return totDeathCoverLoding;
	}
	public void setTotDeathCoverLoding(String totDeathCoverLoding) {
		this.totDeathCoverLoding = totDeathCoverLoding;
	}
	public String getTotTPDCoverLoding() {
		return totTPDCoverLoding;
	}
	public void setTotTPDCoverLoding(String totTPDCoverLoding) {
		this.totTPDCoverLoding = totTPDCoverLoding;
	}
	public String getTotTraCoverLoding() {
		return totTraCoverLoding;
	}
	public void setTotTraCoverLoding(String totTraCoverLoding) {
		this.totTraCoverLoding = totTraCoverLoding;
	}
	public String getTotSCICoverLoding() {
		return totSCICoverLoding;
	}
	public void setTotSCICoverLoding(String totSCICoverLoding) {
		this.totSCICoverLoding = totSCICoverLoding;
	}
	public String getExtDeathGroupCover() {
		return extDeathGroupCover;
	}
	public void setExtDeathGroupCover(String extDeathGroupCover) {
		this.extDeathGroupCover = extDeathGroupCover;
	}
	public String getExtTPDGroupCover() {
		return extTPDGroupCover;
	}
	public void setExtTPDGroupCover(String extTPDGroupCover) {
		this.extTPDGroupCover = extTPDGroupCover;
	}
	public String getExtTraumaGroupCover() {
		return extTraumaGroupCover;
	}
	public void setExtTraumaGroupCover(String extTraumaGroupCover) {
		this.extTraumaGroupCover = extTraumaGroupCover;
	}
	public String getExtSCICover() {
		return extSCICover;
	}
	public void setExtSCICover(String extSCICover) {
		this.extSCICover = extSCICover;
	}
	public String getDecisionLetterPath() {
		return decisionLetterPath;
	}
	public void setDecisionLetterPath(String decisionLetterPath) {
		this.decisionLetterPath = decisionLetterPath;
	}
	
	
	
	

}
