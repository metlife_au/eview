package com.metlife.au.metbi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@SpringBootApplication
@EnableCircuitBreaker
@EnableAspectJAutoProxy
@EnableEurekaClient
public class MetBiServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(MetBiServiceApplication.class, args);
	}

}
