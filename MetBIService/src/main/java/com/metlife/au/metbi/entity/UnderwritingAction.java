/**
 * 
 */
package com.metlife.au.metbi.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

/**
 * @author 672381
 *
 */

@Entity
@Table(name = "FCT_UW_ACT", schema = "DBUSDWW")
public class UnderwritingAction {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private Long id;

	/*
	 * @Column(name = "ENDORSEMENT_ID") private Long endorsementId;
	 */

	@Column(name = "START_DATE")
	private String startDate;

	@Column(name = "DESCRIPTION")
	private String description;

	@Column(name = "S_NO")
	private Long sNo;

	@Column(name = "OUTCOME_CODE")
	private Long outComeCode;

	@Column(name = "OUTCOME")
	private String outCome;

	@Column(name = "ACTIVITY_STATUS_CODE")
	private Long actStatusCode;

	@Column(name = "ST")
	private int st;

	@Column(name = "S_D")
	private String sd;

	/*
	 * @Column(name = "REQ") private String requirement;
	 */
	@Column(name = "COMMENTS")
	private String comments;

	@Column(name = "STANDARD_SLA")
	private int standardSLA;

	@Column(name = "ACTUAL_SLA")
	private int actualSLA;

	@Column(name = "SLA_MET")
	private String slaMet;

	@Column(name = "NAME")
	private String name;

	@Column(name = "SRC_TABLE_NAME")
	private String srcTableName;

	@Column(name = "INS_DATE_TIME")
	private String insDateTime;

	@Column(name = "UPD_DATE_TIME")
	private String updDateTime;

	@Column(name = "END_DATE")
	private String endDate;

	@Column(name = "SOURCE_KEY")
	private String sourceKey;

	@Column(name = "APPLICATIONNO")
	private Long applicationNo;

	@Column(name = "SOURCE_SYSTEM")
	private Long sourceSystem;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ENDORSEMENT_ID", referencedColumnName = "ENDORSEMENT_ID")
	private UnderwritingDetail underwritingDetail;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "UWEVENTID", referencedColumnName = "EVENTID")
	private UnderwritingEvent underwritingEvent;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "REQ", referencedColumnName = "REQUIREMENT")
	private UnderwritingReqDoc underwritingReqDoc;

	

	/**
	 * 
	 */
	public UnderwritingAction() {

	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the startDate
	 */
	public String getStartDate() {
		return startDate;
	}

	/**
	 * @param startDate the startDate to set
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the sNo
	 */
	public Long getsNo() {
		return sNo;
	}

	/**
	 * @param sNo the sNo to set
	 */
	public void setsNo(Long sNo) {
		this.sNo = sNo;
	}

	/**
	 * @return the outComeCode
	 */
	public Long getOutComeCode() {
		return outComeCode;
	}

	/**
	 * @param outComeCode the outComeCode to set
	 */
	public void setOutComeCode(Long outComeCode) {
		this.outComeCode = outComeCode;
	}

	/**
	 * @return the outCome
	 */
	public String getOutCome() {
		return outCome;
	}

	/**
	 * @param outCome the outCome to set
	 */
	public void setOutCome(String outCome) {
		this.outCome = outCome;
	}

	/**
	 * @return the actStatusCode
	 */
	public Long getActStatusCode() {
		return actStatusCode;
	}

	/**
	 * @param actStatusCode the actStatusCode to set
	 */
	public void setActStatusCode(Long actStatusCode) {
		this.actStatusCode = actStatusCode;
	}

	/**
	 * @return the st
	 */
	public int getSt() {
		return st;
	}

	/**
	 * @param st the st to set
	 */
	public void setSt(int st) {
		this.st = st;
	}

	/**
	 * @return the sd
	 */
	public String getSd() {
		return sd;
	}

	/**
	 * @param sd the sd to set
	 */
	public void setSd(String sd) {
		this.sd = sd;
	}

	

	/*
	 * public String getRequirement() { return requirement; }
	 * 
	 * public void setRequirement(String requirement) { this.requirement =
	 * requirement; }
	 */
	/**
	 * @return the comments
	 */
	public String getComments() {
		return comments;
	}

	/**
	 * @param comments the comments to set
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}

	/**
	 * @return the standardSLA
	 */
	public int getStandardSLA() {
		return standardSLA;
	}

	/**
	 * @param standardSLA the standardSLA to set
	 */
	public void setStandardSLA(int standardSLA) {
		this.standardSLA = standardSLA;
	}

	/**
	 * @return the actualSLA
	 */
	public int getActualSLA() {
		return actualSLA;
	}

	/**
	 * @param actualSLA the actualSLA to set
	 */
	public void setActualSLA(int actualSLA) {
		this.actualSLA = actualSLA;
	}

	/**
	 * @return the slaMet
	 */
	public String getSlaMet() {
		return slaMet;
	}

	/**
	 * @param slaMet the slaMet to set
	 */
	public void setSlaMet(String slaMet) {
		this.slaMet = slaMet;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the srcTableName
	 */
	public String getSrcTableName() {
		return srcTableName;
	}

	/**
	 * @param srcTableName the srcTableName to set
	 */
	public void setSrcTableName(String srcTableName) {
		this.srcTableName = srcTableName;
	}

	/**
	 * @return the insDateTime
	 */
	public String getInsDateTime() {
		return insDateTime;
	}

	/**
	 * @param insDateTime the insDateTime to set
	 */
	public void setInsDateTime(String insDateTime) {
		this.insDateTime = insDateTime;
	}

	/**
	 * @return the updDateTime
	 */
	public String getUpdDateTime() {
		return updDateTime;
	}

	/**
	 * @param updDateTime the updDateTime to set
	 */
	public void setUpdDateTime(String updDateTime) {
		this.updDateTime = updDateTime;
	}

	/**
	 * @return the endDate
	 */
	public String getEndDate() {
		return endDate;
	}

	/**
	 * @param endDate the endDate to set
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	/**
	 * @return the sourceKey
	 */
	public String getSourceKey() {
		return sourceKey;
	}

	/**
	 * @param sourceKey the sourceKey to set
	 */
	public void setSourceKey(String sourceKey) {
		this.sourceKey = sourceKey;
	}

	/**
	 * @return the applicationNo
	 */
	public Long getApplicationNo() {
		return applicationNo;
	}

	/**
	 * @param applicationNo the applicationNo to set
	 */
	public void setApplicationNo(Long applicationNo) {
		this.applicationNo = applicationNo;
	}

	/**
	 * @return the sourceSystem
	 */
	public Long getSourceSystem() {
		return sourceSystem;
	}

	/**
	 * @param sourceSystem the sourceSystem to set
	 */
	public void setSourceSystem(Long sourceSystem) {
		this.sourceSystem = sourceSystem;
	}

	/**
	 * @return the underwritingDetail
	 */
	public UnderwritingDetail getUnderwritingDetail() {
		return underwritingDetail;
	}

	/**
	 * @param underwritingDetail the underwritingDetail to set
	 */
	public void setUnderwritingDetail(UnderwritingDetail underwritingDetail) {
		this.underwritingDetail = underwritingDetail;
	}

	/**
	 * @return the underwritingEvent
	 */
	public UnderwritingEvent getUnderwritingEvent() {
		return underwritingEvent;
	}

	/**
	 * @param underwritingEvent the underwritingEvent to set
	 */
	public void setUnderwritingEvent(UnderwritingEvent underwritingEvent) {
		this.underwritingEvent = underwritingEvent;
	}
	
	public UnderwritingReqDoc getUnderwritingReqDoc() {
		return underwritingReqDoc;
	}

	public void setUnderwritingReqDoc(UnderwritingReqDoc underwritingReqDoc) {
		this.underwritingReqDoc = underwritingReqDoc;
	}

	/*	*//**
			 * @return the endorsementId
			 */
	/*
	 * public Long getEndorsementId() { return endorsementId; }
	 * 
	 *//**
		 * @param endorsementId the endorsementId to set
		 *//*
			 * public void setEndorsementId(Long endorsementId) { this.endorsementId =
			 * endorsementId; }
			 */

}
