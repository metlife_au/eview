package com.metlife.au.metbi.rsql;

import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.jirutka.rsql.parser.ast.AndNode;
import cz.jirutka.rsql.parser.ast.ComparisonNode;
import cz.jirutka.rsql.parser.ast.Node;
import cz.jirutka.rsql.parser.ast.OrNode;
import cz.jirutka.rsql.parser.ast.RSQLVisitor;

public class RSQLGroupVisitor implements RSQLVisitor<RSQLGroupParameter, RSQLGroupParameter> {
	
	private static final Logger log = LoggerFactory.getLogger(RSQLGroupVisitor.class);
	
	//private RSQLRootExpression expression;

	public RSQLGroupVisitor() {
		//expression = new RSQLRootExpression();
		//expression.setParameters(new ArrayList<RSQLParameter>());
	}

	@Override
	public RSQLGroupParameter visit(AndNode node, RSQLGroupParameter rExpression) {
		for (Iterator<Node> it = node.iterator(); it.hasNext();) {
			Node child = it.next();
			if(child instanceof AndNode){
				RSQLGroupVisitor visitor = new RSQLGroupVisitor();
				RSQLAndParameter rsqlExpression = new RSQLAndParameter();
				//rExpression.setParameters(new ArrayList<RSQLParameter>());
				rExpression.getParameters().add(rsqlExpression);
				child.accept(visitor, rsqlExpression);
			}
			else if(child instanceof OrNode){
				RSQLGroupVisitor visitor = new RSQLGroupVisitor();
				RSQLOrParameter rsqlExpression = new RSQLOrParameter();
				//rExpression.setParameters(new ArrayList<RSQLParameter>());
				rExpression.getParameters().add(rsqlExpression);
				child.accept(visitor, rsqlExpression);
			}
			else{
				child.accept(this, rExpression);
			}
		}
		return rExpression;
	}

	@Override
	public RSQLGroupParameter visit(OrNode node, RSQLGroupParameter rExpression) {
		for (Iterator<Node> it = node.iterator(); it.hasNext();) {
			Node child = it.next();
			if(child instanceof AndNode){
				RSQLGroupVisitor visitor = new RSQLGroupVisitor();
				RSQLAndParameter rsqlExpression = new RSQLAndParameter();
				//rExpression.setParameters(new ArrayList<RSQLParameter>());
				rExpression.getParameters().add(rsqlExpression);
				child.accept(visitor, rsqlExpression);
			}
			else if(child instanceof OrNode){
				RSQLGroupVisitor visitor = new RSQLGroupVisitor();
				RSQLOrParameter rsqlExpression = new RSQLOrParameter();
				//rExpression.setParameters(new ArrayList<RSQLParameter>());
				rExpression.getParameters().add(rsqlExpression);
				child.accept(visitor, rsqlExpression);
			}
			else{
				child.accept(this, rExpression);
			}
		}
		return rExpression;
	}

	@Override
	public RSQLGroupParameter visit(ComparisonNode node, RSQLGroupParameter rExpression) {
		RSQLParameter parameter = new RSQLParameter();
		String attrName = node.getSelector();
		parameter.setParameterName(attrName);
		String symbol = node.getOperator().getSymbol();
		List<String> values = node.getArguments();
		switch (symbol) {
		case "=q=":
			parameter.setParameterSingleValue(values.get(0));
			if ("*".equals(attrName)) {
				parameter.setParameterExpression("SEARCH_ALL");
			} else {
				parameter.setParameterExpression(attrName + ".SEARCH");
			}
			break;
		case "==":
			parameter.setParameterSingleValue(values.get(0));
			parameter.setOperator("EQ");
			parameter.setParameterExpression(attrName + ".EQ");
			break;
		case "=in=":
			parameter.setOperator("IN");
			parameter.setParameterExpression(attrName + ".IN");
			parameter.setParameterMultiValues(values);
			break;
		case "=out=":
			parameter.setOperator("OUT");
			parameter.setParameterExpression(attrName + ".OUT");
			parameter.setParameterMultiValues(values);
			break;
		case "=lt=":
		case "<":
			parameter.setParameterSingleValue(values.get(0));
			parameter.setOperator("LT");
			parameter.setParameterExpression(attrName + ".LT");
			break;
		case "=le=":
		case "<=":
			parameter.setParameterSingleValue(values.get(0));
			parameter.setOperator("LTE");
			parameter.setParameterExpression(attrName + ".LTE");
			break;
		case "=gt=":
		case ">":
			parameter.setParameterSingleValue(values.get(0));
			parameter.setOperator("GT");
			parameter.setParameterExpression(attrName + ".GT");
			break;
		case "=ge=":
		case ">=":
			parameter.setParameterSingleValue(values.get(0));
			parameter.setOperator("GTE");
			parameter.setParameterExpression(attrName + ".GTE");
			break;
		case "=like=":
			parameter.setParameterSingleValue(values.get(0));
			parameter.setOperator("LIKE");
			parameter.setParameterExpression(attrName + ".LIKE");
			break;
		case "!=":
			parameter.setParameterSingleValue(values.get(0));
			parameter.setOperator("NE");
			parameter.setParameterExpression(attrName + ".NE");
			break;
		default:
			throw new RSQLParserException(0, "Unknown RSQL query operator [" + symbol + "]");
		}
		rExpression.getParameters().add(parameter);
		return rExpression;
	}


}
