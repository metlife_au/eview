/**
 * 
 */
package com.metlife.au.metbi.dto;

import java.io.Serializable;

/**
 * @author 672381
 *
 */
public class ClaimPaymentEmbeddedDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7691590778459522686L;

	private Long claimId;

	private String claimNo;

	/**
	 * 
	 */
	public ClaimPaymentEmbeddedDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the claimId
	 */
	public Long getClaimId() {
		return claimId;
	}

	/**
	 * @param claimId the claimId to set
	 */
	public void setClaimId(Long claimId) {
		this.claimId = claimId;
	}

	/**
	 * @return the claimNo
	 */
	public String getClaimNo() {
		return claimNo;
	}

	/**
	 * @param claimNo the claimNo to set
	 */
	public void setClaimNo(String claimNo) {
		this.claimNo = claimNo;
	}

}
