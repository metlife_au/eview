/**
 * 
 */
package com.metlife.au.metbi.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

/**
 * @author 672381
 *
 */

@Entity
@Table(name = "FCT_CLM_DTL", schema = "DBUSDWW")
public class ClaimDetails implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 295338376630207426L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "CLAIM_ID")
	private Long claimId;

	@Column(name = "CLAIMNO")
	private String claimNo;

	/*
	 * @Column(name = "PRODUCTTYPE") private String productType;
	 */

	@Column(name = "POLICYID")
	private String policyId;

	@Column(name = "SCHEME")
	private String scheme;

	@Column(name = "POLICYNO")
	private String policyNo;

	@Column(name = "MEM_PARTY_ID")
	private String memPartyId;

	@Column(name = "MEM_ROLE_PLAYER_ID")
	private String memRolePlayerId;

	@Column(name = "FIRSTNAME")
	private String firstName;

	@Column(name = "SURNAME")
	private String surName;

	@Column(name = "DATEOFBIRTH")
	private String memberDob;

	@Column(name = "GENDER")
	private String memberGender;

	@Column(name = "REFERENCE")
	private String reference;

	@Column(name = "CLAIMTYPE")
	private String claimType;

	@Column(name = "CLAIMTYPE_DES")
	private String claimTypeDes;

	/*
	 * @Column(name = "STATUS_CODE") private String statusCode;
	 */

	@Column(name = "DATESUBMITTED")
	private String dateSubmitted;

	@Column(name = "DISABILITYDATE")
	private String disabilityDate;

	@Column(name = "DATELASTPAID")
	private String detailsPaid;

	@Column(name = "CLAIMCAUSE")
	private String claimCause;

	@Column(name = "CLAIMCAUSE_DES")
	private String claimCauseDes;

	@Column(name = "CLAIMASSESSOR")
	private String claimsSessor;

	@Column(name = "SUMINSURED")
	private String sumInsured;

	@Column(name = "TOTALBENEFITPAID_GROSS")
	private String totalBenefitPaidGross;

	@Column(name = "TOTALBENEFITPAID_DEDUCTIONS")
	private String totalBenefitPaidDeduct;

	@Column(name = "MONTHLYBENEFIT")
	private String monthlyBenefit;

	@Column(name = "WAITPERIOD")
	private String waitPeriod;

	@Column(name = "BENEFITPERIOD")
	private String benefitPeriod;

	@Column(name = "SALARYINSURED")
	private String salaryInsured;

	@Column(name = "SUPERCONTRIBUTION")
	private String superContribution;

	@Column(name = "FUNDOWNERID")
	private String fundOwnerId;

	@Column(name = "FUND_NAME")
	private String fundName;

	@Column(name = "FUND_PARTY_ID")
	private String fundPartyId;

	@Column(name = "FUND_ROLE_ID")
	private String fundRoleId;

	@Column(name = "LASTUPDDATE")
	private String lastUpdate;

	@Transient
	private String mStatus;

	@OneToMany(mappedBy = "claimDetails", fetch = FetchType.EAGER)
	private List<ClaimActivity> claimActivity;

	@ManyToOne
	@JoinColumns({ @JoinColumn(name = "PRODUCTTYPE", referencedColumnName = "PRODUCTTYPE"),
			@JoinColumn(name = "STATUS_CODE", referencedColumnName = "CLAIMSTATUS") })
	private ClaimStatus claimStatus;

	@ManyToOne
	@JoinColumns({
			@JoinColumn(name = "CLAIMNO", referencedColumnName = "CLAIM_NO", insertable = false, updatable = false),
			@JoinColumn(name = "CLAIM_ID", referencedColumnName = "CLAIM_ID", insertable = false, updatable = false) })
	@NotFound(action = NotFoundAction.IGNORE)
	private ClaimPayment claimPayment;

	/**
	 * @return the claimId
	 */
	public Long getClaimId() {
		return claimId;
	}

	/**
	 * @param claimId the claimId to set
	 */
	public void setClaimId(Long claimId) {
		this.claimId = claimId;
	}

	/**
	 * @return the claimNo
	 */
	public String getClaimNo() {
		return claimNo;
	}

	/**
	 * @param claimNo the claimNo to set
	 */
	public void setClaimNo(String claimNo) {
		this.claimNo = claimNo;
	}

	/**
	 * @return the policyId
	 */
	public String getPolicyId() {
		return policyId;
	}

	/**
	 * @param policyId the policyId to set
	 */
	public void setPolicyId(String policyId) {
		this.policyId = policyId;
	}

	/**
	 * @return the scheme
	 */
	public String getScheme() {
		return scheme;
	}

	/**
	 * @param scheme the scheme to set
	 */
	public void setScheme(String scheme) {
		this.scheme = scheme;
	}

	/**
	 * @return the policyNo
	 */
	public String getPolicyNo() {
		return policyNo;
	}

	/**
	 * @param policyNo the policyNo to set
	 */
	public void setPolicyNo(String policyNo) {
		this.policyNo = policyNo;
	}

	/**
	 * @return the memPartyId
	 */
	public String getMemPartyId() {
		return memPartyId;
	}

	/**
	 * @param memPartyId the memPartyId to set
	 */
	public void setMemPartyId(String memPartyId) {
		this.memPartyId = memPartyId;
	}

	/**
	 * @return the memRolePlayerId
	 */
	public String getMemRolePlayerId() {
		return memRolePlayerId;
	}

	/**
	 * @param memRolePlayerId the memRolePlayerId to set
	 */
	public void setMemRolePlayerId(String memRolePlayerId) {
		this.memRolePlayerId = memRolePlayerId;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the surName
	 */
	public String getSurName() {
		return surName;
	}

	/**
	 * @param surName the surName to set
	 */
	public void setSurName(String surName) {
		this.surName = surName;
	}

	/**
	 * @return the memberDob
	 */
	public String getMemberDob() {
		return memberDob;
	}

	/**
	 * @param memberDob the memberDob to set
	 */
	public void setMemberDob(String memberDob) {
		this.memberDob = memberDob;
	}

	/**
	 * @return the memberGender
	 */
	public String getMemberGender() {
		return memberGender;
	}

	/**
	 * @param memberGender the memberGender to set
	 */
	public void setMemberGender(String memberGender) {
		this.memberGender = memberGender;
	}

	/**
	 * @return the reference
	 */
	public String getReference() {
		return reference;
	}

	/**
	 * @param reference the reference to set
	 */
	public void setReference(String reference) {
		this.reference = reference;
	}

	/**
	 * @return the claimType
	 */
	public String getClaimType() {
		return claimType;
	}

	/**
	 * @param claimType the claimType to set
	 */
	public void setClaimType(String claimType) {
		this.claimType = claimType;
	}

	/**
	 * @return the claimTypeDes
	 */
	public String getClaimTypeDes() {
		return claimTypeDes;
	}

	/**
	 * @param claimTypeDes the claimTypeDes to set
	 */
	public void setClaimTypeDes(String claimTypeDes) {
		this.claimTypeDes = claimTypeDes;
	}

	/**
	 * @return the dateSubmitted
	 */
	public String getDateSubmitted() {
		return dateSubmitted;
	}

	/**
	 * @param dateSubmitted the dateSubmitted to set
	 */
	public void setDateSubmitted(String dateSubmitted) {
		this.dateSubmitted = dateSubmitted;
	}

	/**
	 * @return the disabilityDate
	 */
	public String getDisabilityDate() {
		return disabilityDate;
	}

	/**
	 * @param disabilityDate the disabilityDate to set
	 */
	public void setDisabilityDate(String disabilityDate) {
		this.disabilityDate = disabilityDate;
	}

	/**
	 * @return the detailsPaid
	 */
	public String getDetailsPaid() {
		return detailsPaid;
	}

	/**
	 * @param detailsPaid the detailsPaid to set
	 */
	public void setDetailsPaid(String detailsPaid) {
		this.detailsPaid = detailsPaid;
	}

	/**
	 * @return the claimCause
	 */
	public String getClaimCause() {
		return claimCause;
	}

	/**
	 * @param claimCause the claimCause to set
	 */
	public void setClaimCause(String claimCause) {
		this.claimCause = claimCause;
	}

	/**
	 * @return the claimCauseDes
	 */
	public String getClaimCauseDes() {
		return claimCauseDes;
	}

	/**
	 * @param claimCauseDes the claimCauseDes to set
	 */
	public void setClaimCauseDes(String claimCauseDes) {
		this.claimCauseDes = claimCauseDes;
	}

	/**
	 * @return the claimsSessor
	 */
	public String getClaimsSessor() {
		return claimsSessor;
	}

	/**
	 * @param claimsSessor the claimsSessor to set
	 */
	public void setClaimsSessor(String claimsSessor) {
		this.claimsSessor = claimsSessor;
	}

	/**
	 * @return the sumInsured
	 */
	public String getSumInsured() {
		return sumInsured;
	}

	/**
	 * @param sumInsured the sumInsured to set
	 */
	public void setSumInsured(String sumInsured) {
		this.sumInsured = sumInsured;
	}

	/**
	 * @return the totalBenefitPaidGross
	 */
	public String getTotalBenefitPaidGross() {
		return totalBenefitPaidGross;
	}

	/**
	 * @param totalBenefitPaidGross the totalBenefitPaidGross to set
	 */
	public void setTotalBenefitPaidGross(String totalBenefitPaidGross) {
		this.totalBenefitPaidGross = totalBenefitPaidGross;
	}

	/**
	 * @return the totalBenefitPaidDeduct
	 */
	public String getTotalBenefitPaidDeduct() {
		return totalBenefitPaidDeduct;
	}

	/**
	 * @param totalBenefitPaidDeduct the totalBenefitPaidDeduct to set
	 */
	public void setTotalBenefitPaidDeduct(String totalBenefitPaidDeduct) {
		this.totalBenefitPaidDeduct = totalBenefitPaidDeduct;
	}

	/**
	 * @return the monthlyBenefit
	 */
	public String getMonthlyBenefit() {
		return monthlyBenefit;
	}

	/**
	 * @param monthlyBenefit the monthlyBenefit to set
	 */
	public void setMonthlyBenefit(String monthlyBenefit) {
		this.monthlyBenefit = monthlyBenefit;
	}

	/**
	 * @return the waitPeriod
	 */
	public String getWaitPeriod() {
		return waitPeriod;
	}

	/**
	 * @param waitPeriod the waitPeriod to set
	 */
	public void setWaitPeriod(String waitPeriod) {
		this.waitPeriod = waitPeriod;
	}

	/**
	 * @return the benefitPeriod
	 */
	public String getBenefitPeriod() {
		return benefitPeriod;
	}

	/**
	 * @param benefitPeriod the benefitPeriod to set
	 */
	public void setBenefitPeriod(String benefitPeriod) {
		this.benefitPeriod = benefitPeriod;
	}

	/**
	 * @return the salaryInsured
	 */
	public String getSalaryInsured() {
		return salaryInsured;
	}

	/**
	 * @param salaryInsured the salaryInsured to set
	 */
	public void setSalaryInsured(String salaryInsured) {
		this.salaryInsured = salaryInsured;
	}

	/**
	 * @return the superContribution
	 */
	public String getSuperContribution() {
		return superContribution;
	}

	/**
	 * @param superContribution the superContribution to set
	 */
	public void setSuperContribution(String superContribution) {
		this.superContribution = superContribution;
	}

	/**
	 * @return the fundOwnerId
	 */
	public String getFundOwnerId() {
		return fundOwnerId;
	}

	/**
	 * @param fundOwnerId the fundOwnerId to set
	 */
	public void setFundOwnerId(String fundOwnerId) {
		this.fundOwnerId = fundOwnerId;
	}

	/**
	 * @return the fundName
	 */
	public String getFundName() {
		return fundName;
	}

	/**
	 * @param fundName the fundName to set
	 */
	public void setFundName(String fundName) {
		this.fundName = fundName;
	}

	/**
	 * @return the fundPartyId
	 */
	public String getFundPartyId() {
		return fundPartyId;
	}

	/**
	 * @param fundPartyId the fundPartyId to set
	 */
	public void setFundPartyId(String fundPartyId) {
		this.fundPartyId = fundPartyId;
	}

	/**
	 * @return the fundRoleId
	 */
	public String getFundRoleId() {
		return fundRoleId;
	}

	/**
	 * @param fundRoleId the fundRoleId to set
	 */
	public void setFundRoleId(String fundRoleId) {
		this.fundRoleId = fundRoleId;
	}

	/**
	 * @return the lastUpdate
	 */
	public String getLastUpdate() {
		return lastUpdate;
	}

	/**
	 * @param lastUpdate the lastUpdate to set
	 */
	public void setLastUpdate(String lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	/**
	 * @return the mStatus
	 */
	public String getmStatus() {
		return mStatus;
	}

	/**
	 * @param mStatus the mStatus to set
	 */
	public void setmStatus(String mStatus) {
		this.mStatus = mStatus;
	}

	/**
	 * @return the claimActivity
	 */
	public List<ClaimActivity> getClaimActivity() {
		return claimActivity;
	}

	/**
	 * @param claimActivity the claimActivity to set
	 */
	public void setClaimActivity(List<ClaimActivity> claimActivity) {
		this.claimActivity = claimActivity;
	}

	/**
	 * @return the claimStatus
	 */
	public ClaimStatus getClaimStatus() {
		return claimStatus;
	}

	/**
	 * @param claimStatus the claimStatus to set
	 */
	public void setClaimStatus(ClaimStatus claimStatus) {
		this.claimStatus = claimStatus;
	}

	/**
	 * @return the claimPayment
	 */
	public ClaimPayment getClaimPayment() {
		return claimPayment;
	}

	/**
	 * @param claimPayment the claimPayment to set
	 */
	public void setClaimPayment(ClaimPayment claimPayment) {
		this.claimPayment = claimPayment;
	}

}
