/**
 * 
 */
package com.metlife.au.metbi.utils;

import org.springframework.util.StringUtils;

import com.metlife.au.metbi.constants.ReportConstants;

/**
 * @author 672381
 *
 */
public class CommonUtils {

	public static String addEscapeApostrophe(String name) {
		String updatedName = name;
		if (!StringUtils.isEmpty(updatedName) && updatedName.length() > 0
				&& updatedName.contains(ReportConstants.SINGLE_QUOTE)) {
			updatedName = updatedName.replace(ReportConstants.SINGLE_QUOTE, ReportConstants.DOUBLE_QUOTES);
		}
		return updatedName;
	}

}
