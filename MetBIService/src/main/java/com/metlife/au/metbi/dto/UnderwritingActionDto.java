/**
 * 
 */
package com.metlife.au.metbi.dto;

/**
 * @author 672381
 *
 */
public class UnderwritingActionDto {

	private String requirement;
	private String comments;
	private int standardSLA;
	private int actualSLA;
	private String slaMet;	
	private UnderwritingEventDto underwritingEvent;


	/**
	 * 
	 */
	public UnderwritingActionDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getRequirement() {
		return requirement;
	}

	public void setRequirement(String requirement) {
		this.requirement = requirement;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public int getStandardSLA() {
		return standardSLA;
	}

	public void setStandardSLA(int standardSLA) {
		this.standardSLA = standardSLA;
	}

	public int getActualSLA() {
		return actualSLA;
	}

	public void setActualSLA(int actualSLA) {
		this.actualSLA = actualSLA;
	}

	public String getSlaMet() {
		return slaMet;
	}

	public void setSlaMet(String slaMet) {
		this.slaMet = slaMet;
	}
	
	public UnderwritingEventDto getUnderwritingEvent() {
		return underwritingEvent;
	}

	public void setUnderwritingEvent(UnderwritingEventDto underwritingEvent) {
		this.underwritingEvent = underwritingEvent;
	}	

}
