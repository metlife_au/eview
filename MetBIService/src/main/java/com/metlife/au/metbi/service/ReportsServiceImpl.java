package com.metlife.au.metbi.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.tennaito.rsql.jpa.JpaCriteriaQueryVisitor;
import com.metlife.au.metbi.constants.ReportConstants;
import com.metlife.au.metbi.dto.FundDto;
import com.metlife.au.metbi.dto.ReportFilter;
import com.metlife.au.metbi.dto.UWDrillDownApplicationInfo;
import com.metlife.au.metbi.dto.UWDrillDownEventInfo;
import com.metlife.au.metbi.dto.UWDrillDownMemberInfo;
import com.metlife.au.metbi.dto.UWDrillDownReport;
import com.metlife.au.metbi.dto.UWSearchFilter;
import com.metlife.au.metbi.dto.UWSearchResult;
import com.metlife.au.metbi.dto.UnderwritingActionDto;
import com.metlife.au.metbi.entity.Fund;
import com.metlife.au.metbi.entity.UnderwritingAction;
import com.metlife.au.metbi.entity.UnderwritingDetail;
import com.metlife.au.metbi.exception.ApplicationException;
import com.metlife.au.metbi.repository.ReportsRepository;
import com.metlife.au.metbi.rsql.RSQLVisitorCust;
import com.metlife.au.metbi.utils.ReportsMapper;
import com.metlife.au.metbi.rsql.RSQLRootExpression;

import cz.jirutka.rsql.parser.RSQLParser;
import cz.jirutka.rsql.parser.ast.Node;
import cz.jirutka.rsql.parser.ast.RSQLVisitor;

/**
 * @author 672381
 *
 */

@Service
public class ReportsServiceImpl implements ReportsService {

	private final Logger logger = LoggerFactory.getLogger(ReportsServiceImpl.class);

	private ReportsRepository reportsRepository;

	EntityManager entityManager;
	
	private ReportsMapper reportsMapper;

	@Autowired
	public ReportsServiceImpl(ReportsRepository reportsRepository, EntityManager entityManager, ReportsMapper reportsMapper) {
		this.reportsRepository = reportsRepository;
		this.entityManager = entityManager;
		this.reportsMapper = reportsMapper;
	}

	public List<UnderwritingDetail> getUWStatusDetails(String queryParam, ReportFilter reportFilter)
			throws ApplicationException {
		return this.reportsRepository.getUWStatusDetails(queryParam, reportFilter);
	}

	public List<UnderwritingDetail> getUWSLADetails(String queryParam, ReportFilter reportFilter)
			throws ApplicationException {
		return this.reportsRepository.getUWSLADetails(queryParam, reportFilter);
	}

	public List<UnderwritingDetail> getUWRequirementReport(String queryParam, ReportFilter reportFilter) throws ApplicationException {
		return this.reportsRepository.getUWRequirementReport(queryParam, reportFilter);
	}

	@Override
	public List<UWSearchResult> getUWSaerch(UWSearchFilter uwSearchFilter) throws ApplicationException {
		return reportsRepository.getUWSaerch(uwSearchFilter);
	}
	
	public List<FundDto> fundDetails(List<String> fundcodes) throws ApplicationException{
		List<FundDto> fundDtoList=new ArrayList<FundDto>();
		try {
			List<Fund> fundList=reportsRepository.fundDetails(fundcodes);
			if(fundList.size()>0) {
				fundDtoList=reportsMapper.fundConversion(fundList);
			}
		}catch(Exception e) {
			throw new ApplicationException("Exception in fundDetails");
		}
		return fundDtoList;
	}
	
	@Override
	public UWDrillDownReport getUWDrillDownReport(String fundcode, long applicationNo, String drilldownType)
			throws ApplicationException {
		// TODO Auto-generated method stub
		UWDrillDownReport uwDrillDownreport = new UWDrillDownReport();
		try {
			UnderwritingDetail reportDetails = reportsRepository.getUWDrillDownReport(fundcode, applicationNo);
			UWDrillDownApplicationInfo applicationInfo = reportsMapper.uwApplicationInfo(reportDetails);
			UWDrillDownMemberInfo memberInfo = new UWDrillDownMemberInfo();
			UWDrillDownEventInfo eventInfo = new UWDrillDownEventInfo();
			List<UnderwritingAction> underwritingAction = new ArrayList<UnderwritingAction>();
			if (drilldownType.equalsIgnoreCase(ReportConstants.STATUS_EVENT_HISTORY)) {
				underwritingAction = reportsRepository.getEventHistory(fundcode, applicationNo,drilldownType);
			} else if (drilldownType.equalsIgnoreCase(ReportConstants.REQUIREMENT_EVENT_HISTORY)) {
				underwritingAction = reportsRepository.getEventHistory(fundcode, applicationNo,drilldownType);
			}
			UnderwritingActionDto underwritingact = new UnderwritingActionDto();
			underwritingact = null;
			if (underwritingAction.size() > 0) {
				underwritingact = reportsMapper.uwActionmapper(underwritingAction.get(0));
			}
			memberInfo.setClientReferenceNo(reportDetails.getClientReferenceNo());
			memberInfo.setFirstName(reportDetails.getFirstName());
			memberInfo.setMemberDob(reportDetails.getMemberDob());
			memberInfo.setSurName(reportDetails.getSurName());
			if (underwritingact != null) {
				eventInfo.setEventId(underwritingact.getUnderwritingEvent().getEventId());
				eventInfo.setEventDate(underwritingact.getUnderwritingEvent().getLastUpdatedDate());
			}
			uwDrillDownreport.setApplicationInfo(applicationInfo);
			uwDrillDownreport.setMemberInfo(memberInfo);
			uwDrillDownreport.setEventInfo(eventInfo);

		} catch (Exception e) {
			throw new ApplicationException("ex");
			// TODO: handle exception
		}

		return uwDrillDownreport;
	}
	
}
