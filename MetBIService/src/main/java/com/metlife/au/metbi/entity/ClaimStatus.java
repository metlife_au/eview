/**
 * 
 */
package com.metlife.au.metbi.entity;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * @author 672381
 *
 */

@Entity
@Table(name = "TBLCLAIMSTATUS", schema = "DBUSDWW")
public class ClaimStatus implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4382077560921075082L;

	@EmbeddedId
	private ClaimStatusEmbedded claimStatusEmbedded;

	@Column(name = "STATUSDESCRIPTION")
	private String statusDescription;

	@OneToMany(mappedBy = "claimStatus")
	private Set<ClaimDetails> claimDetails;

	/**
	 * @return the statusDescription
	 */
	public String getStatusDescription() {
		return statusDescription;
	}

	/**
	 * @param statusDescription the statusDescription to set
	 */
	public void setStatusDescription(String statusDescription) {
		this.statusDescription = statusDescription;
	}

	/**
	 * @return the claimDetails
	 */
	public Set<ClaimDetails> getClaimDetails() {
		return claimDetails;
	}

	/**
	 * @param claimDetails the claimDetails to set
	 */
	public void setClaimDetails(Set<ClaimDetails> claimDetails) {
		this.claimDetails = claimDetails;
	}

	/**
	 * @return the claimStatusEmbedded
	 */
	public ClaimStatusEmbedded getClaimStatusEmbedded() {
		return claimStatusEmbedded;
	}

	/**
	 * @param claimStatusEmbedded the claimStatusEmbedded to set
	 */
	public void setClaimStatusEmbedded(ClaimStatusEmbedded claimStatusEmbedded) {
		this.claimStatusEmbedded = claimStatusEmbedded;
	}

}
