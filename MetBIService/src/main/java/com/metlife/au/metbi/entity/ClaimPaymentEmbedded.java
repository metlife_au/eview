/**
 * 
 */
package com.metlife.au.metbi.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * @author 672381
 *
 */
@Embeddable
public class ClaimPaymentEmbedded implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2768325546447218556L;

	@Column(name = "CLAIM_ID")
	private Long claimId;

	@Column(name = "CLAIM_NO")
	private String claimNo;

	/**
	 * 
	 */
	public ClaimPaymentEmbedded() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param claimId
	 * @param claimNo
	 */
	public ClaimPaymentEmbedded(Long claimId, String claimNo) {
		super();
		this.claimId = claimId;
		this.claimNo = claimNo;
	}

	/**
	 * @return the claimId
	 */
	public Long getClaimId() {
		return claimId;
	}

	/**
	 * @param claimId the claimId to set
	 */
	public void setClaimId(Long claimId) {
		this.claimId = claimId;
	}

	/**
	 * @return the claimNo
	 */
	public String getClaimNo() {
		return claimNo;
	}

	/**
	 * @param claimNo the claimNo to set
	 */
	public void setClaimNo(String claimNo) {
		this.claimNo = claimNo;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((claimId == null) ? 0 : claimId.hashCode());
		result = prime * result + ((claimNo == null) ? 0 : claimNo.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ClaimPaymentEmbedded other = (ClaimPaymentEmbedded) obj;
		if (claimId == null) {
			if (other.claimId != null)
				return false;
		} else if (!claimId.equals(other.claimId))
			return false;
		if (claimNo == null) {
			if (other.claimNo != null)
				return false;
		} else if (!claimNo.equals(other.claimNo))
			return false;
		return true;
	}

}
