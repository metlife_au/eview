package com.metlife.au.metbi.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class ClaimPaymentDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1498840061144327240L;

	private ClaimPaymentEmbeddedDto claimPaymentEmbedded;

	private String chequeNo;

	private Date dispatchDate;

	private Long sourceSystem;

	private Date fromDate;

	private Date toDate;

	private BigDecimal amount;

	public ClaimPaymentDto() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the chequeNo
	 */
	public String getChequeNo() {
		return chequeNo;
	}

	/**
	 * @param chequeNo the chequeNo to set
	 */
	public void setChequeNo(String chequeNo) {
		this.chequeNo = chequeNo;
	}

	/**
	 * @return the dispatchDate
	 */
	public Date getDispatchDate() {
		return dispatchDate;
	}

	/**
	 * @param dispatchDate the dispatchDate to set
	 */
	public void setDispatchDate(Date dispatchDate) {
		this.dispatchDate = dispatchDate;
	}

	/**
	 * @return the sourceSystem
	 */
	public Long getSourceSystem() {
		return sourceSystem;
	}

	/**
	 * @param sourceSystem the sourceSystem to set
	 */
	public void setSourceSystem(Long sourceSystem) {
		this.sourceSystem = sourceSystem;
	}

	/**
	 * @return the fromDate
	 */
	public Date getFromDate() {
		return fromDate;
	}

	/**
	 * @param fromDate the fromDate to set
	 */
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	/**
	 * @return the toDate
	 */
	public Date getToDate() {
		return toDate;
	}

	/**
	 * @param toDate the toDate to set
	 */
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	/**
	 * @return the amount
	 */
	public BigDecimal getAmount() {
		return amount;
	}

	/**
	 * @param amount the amount to set
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	/**
	 * @return the claimPaymentEmbedded
	 */
	public ClaimPaymentEmbeddedDto getClaimPaymentEmbedded() {
		return claimPaymentEmbedded;
	}

	/**
	 * @param claimPaymentEmbedded the claimPaymentEmbedded to set
	 */
	public void setClaimPaymentEmbedded(ClaimPaymentEmbeddedDto claimPaymentEmbedded) {
		this.claimPaymentEmbedded = claimPaymentEmbedded;
	}

}
