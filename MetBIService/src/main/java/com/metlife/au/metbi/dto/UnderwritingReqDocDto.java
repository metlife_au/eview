package com.metlife.au.metbi.dto;

public class UnderwritingReqDocDto {
	private String requirement;
	private String document;
	public String getRequirement() {
		return requirement;
	}
	public void setRequirement(String requirement) {
		this.requirement = requirement;
	}
	public String getDocument() {
		return document;
	}
	public void setDocument(String document) {
		this.document = document;
	}


}
