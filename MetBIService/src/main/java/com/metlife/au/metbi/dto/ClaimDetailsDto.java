/**
 * 
 */
package com.metlife.au.metbi.dto;

import java.io.Serializable;
import java.util.Set;

/**
 * 
 * @author 390092
 *
 */
public class ClaimDetailsDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 899267697695972931L;

	private Long claimId;

	private String claimNo;

	private String productType;

	private String policyId;

	private String scheme;

	private String policyNo;

	private String memPartyId;

	private String memRolePlayerId;

	private String firstName;

	private String surName;

	private String memberDob;

	private String memberGender;

	private String reference;

	private String claimType;

	private String claimTypeDes;

	private String statusCode;

	private String dateSubmitted;

	private String disabilityDate;

	private String detailsPaid;

	private String claimCause;

	private String claimCauseDes;

	private String claimAssessor;

	private String sumInsured;

	private String totalBenefitPaidGross;

	private String totalBenefitPaidDeduct;

	private String monthlyBenefit;

	private String waitPeriod;

	private String benefitPeriod;

	private String salaryInsured;

	private String superContribution;

	private String fundOwnerId;

	private String fundName;

	private String fundPartyId;

	private String fundRoleId;

	private String lastUpdate;

	private String mStatus;

	private Set<ClaimActivityDto> claimActivity;

	private ClaimStatusDto claimStatus;

	private ClaimPaymentDto claimPayment;

	/**
	 * @return the claimId
	 */
	public Long getClaimId() {
		return claimId;
	}

	/**
	 * @param claimId the claimId to set
	 */
	public void setClaimId(Long claimId) {
		this.claimId = claimId;
	}

	/**
	 * @return the claimNo
	 */
	public String getClaimNo() {
		return claimNo;
	}

	/**
	 * @param claimNo the claimNo to set
	 */
	public void setClaimNo(String claimNo) {
		this.claimNo = claimNo;
	}

	/**
	 * @return the productType
	 */
	public String getProductType() {
		return productType;
	}

	/**
	 * @param productType the productType to set
	 */
	public void setProductType(String productType) {
		this.productType = productType;
	}

	/**
	 * @return the policyId
	 */
	public String getPolicyId() {
		return policyId;
	}

	/**
	 * @param policyId the policyId to set
	 */
	public void setPolicyId(String policyId) {
		this.policyId = policyId;
	}

	/**
	 * @return the scheme
	 */
	public String getScheme() {
		return scheme;
	}

	/**
	 * @param scheme the scheme to set
	 */
	public void setScheme(String scheme) {
		this.scheme = scheme;
	}

	/**
	 * @return the policyNo
	 */
	public String getPolicyNo() {
		return policyNo;
	}

	/**
	 * @param policyNo the policyNo to set
	 */
	public void setPolicyNo(String policyNo) {
		this.policyNo = policyNo;
	}

	/**
	 * @return the memPartyId
	 */
	public String getMemPartyId() {
		return memPartyId;
	}

	/**
	 * @param memPartyId the memPartyId to set
	 */
	public void setMemPartyId(String memPartyId) {
		this.memPartyId = memPartyId;
	}

	/**
	 * @return the memRolePlayerId
	 */
	public String getMemRolePlayerId() {
		return memRolePlayerId;
	}

	/**
	 * @param memRolePlayerId the memRolePlayerId to set
	 */
	public void setMemRolePlayerId(String memRolePlayerId) {
		this.memRolePlayerId = memRolePlayerId;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the surName
	 */
	public String getSurName() {
		return surName;
	}

	/**
	 * @param surName the surName to set
	 */
	public void setSurName(String surName) {
		this.surName = surName;
	}

	/**
	 * @return the memberDob
	 */
	public String getMemberDob() {
		return memberDob;
	}

	/**
	 * @param memberDob the memberDob to set
	 */
	public void setMemberDob(String memberDob) {
		this.memberDob = memberDob;
	}

	/**
	 * @return the memberGender
	 */
	public String getMemberGender() {
		return memberGender;
	}

	/**
	 * @param memberGender the memberGender to set
	 */
	public void setMemberGender(String memberGender) {
		this.memberGender = memberGender;
	}

	/**
	 * @return the reference
	 */
	public String getReference() {
		return reference;
	}

	/**
	 * @param reference the reference to set
	 */
	public void setReference(String reference) {
		this.reference = reference;
	}

	/**
	 * @return the claimType
	 */
	public String getClaimType() {
		return claimType;
	}

	/**
	 * @param claimType the claimType to set
	 */
	public void setClaimType(String claimType) {
		this.claimType = claimType;
	}

	/**
	 * @return the claimTypeDes
	 */
	public String getClaimTypeDes() {
		return claimTypeDes;
	}

	/**
	 * @param claimTypeDes the claimTypeDes to set
	 */
	public void setClaimTypeDes(String claimTypeDes) {
		this.claimTypeDes = claimTypeDes;
	}

	/**
	 * @return the statusCode
	 */
	public String getStatusCode() {
		return statusCode;
	}

	/**
	 * @param statusCode the statusCode to set
	 */
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	/**
	 * @return the dateSubmitted
	 */
	public String getDateSubmitted() {
		return dateSubmitted;
	}

	/**
	 * @param dateSubmitted the dateSubmitted to set
	 */
	public void setDateSubmitted(String dateSubmitted) {
		this.dateSubmitted = dateSubmitted;
	}

	/**
	 * @return the disabilityDate
	 */
	public String getDisabilityDate() {
		return disabilityDate;
	}

	/**
	 * @param disabilityDate the disabilityDate to set
	 */
	public void setDisabilityDate(String disabilityDate) {
		this.disabilityDate = disabilityDate;
	}

	/**
	 * @return the detailsPaid
	 */
	public String getDetailsPaid() {
		return detailsPaid;
	}

	/**
	 * @param detailsPaid the detailsPaid to set
	 */
	public void setDetailsPaid(String detailsPaid) {
		this.detailsPaid = detailsPaid;
	}

	/**
	 * @return the claimCause
	 */
	public String getClaimCause() {
		return claimCause;
	}

	/**
	 * @param claimCause the claimCause to set
	 */
	public void setClaimCause(String claimCause) {
		this.claimCause = claimCause;
	}

	/**
	 * @return the claimCauseDes
	 */
	public String getClaimCauseDes() {
		return claimCauseDes;
	}

	/**
	 * @param claimCauseDes the claimCauseDes to set
	 */
	public void setClaimCauseDes(String claimCauseDes) {
		this.claimCauseDes = claimCauseDes;
	}

	/**
	 * @return the claimAssessor
	 */
	public String getClaimAssessor() {
		return claimAssessor;
	}

	/**
	 * @param claimAssessor the claimAssessor to set
	 */
	public void setClaimAssessor(String claimAssessor) {
		this.claimAssessor = claimAssessor;
	}

//	/**
//	 * @return the claimStatus
//	 */
//	public Set<ClaimStatusDto> getClaimStatus() {
//		return claimStatus;
//	}
//
//	/**
//	 * @param claimStatus the claimStatus to set
//	 */
//	public void setClaimStatus(Set<ClaimStatusDto> claimStatus) {
//		this.claimStatus = claimStatus;
//	}

	/**
	 * @return the sumInsured
	 */
	public String getSumInsured() {
		return sumInsured;
	}

	/**
	 * @param sumInsured the sumInsured to set
	 */
	public void setSumInsured(String sumInsured) {
		this.sumInsured = sumInsured;
	}

	/**
	 * @return the totalBenefitPaidGross
	 */
	public String getTotalBenefitPaidGross() {
		return totalBenefitPaidGross;
	}

	/**
	 * @param totalBenefitPaidGross the totalBenefitPaidGross to set
	 */
	public void setTotalBenefitPaidGross(String totalBenefitPaidGross) {
		this.totalBenefitPaidGross = totalBenefitPaidGross;
	}

	/**
	 * @return the totalBenefitPaidDeduct
	 */
	public String getTotalBenefitPaidDeduct() {
		return totalBenefitPaidDeduct;
	}

	/**
	 * @param totalBenefitPaidDeduct the totalBenefitPaidDeduct to set
	 */
	public void setTotalBenefitPaidDeduct(String totalBenefitPaidDeduct) {
		this.totalBenefitPaidDeduct = totalBenefitPaidDeduct;
	}

	/**
	 * @return the monthlyBenefit
	 */
	public String getMonthlyBenefit() {
		return monthlyBenefit;
	}

	/**
	 * @param monthlyBenefit the monthlyBenefit to set
	 */
	public void setMonthlyBenefit(String monthlyBenefit) {
		this.monthlyBenefit = monthlyBenefit;
	}

	/**
	 * @return the waitPeriod
	 */
	public String getWaitPeriod() {
		return waitPeriod;
	}

	/**
	 * @param waitPeriod the waitPeriod to set
	 */
	public void setWaitPeriod(String waitPeriod) {
		this.waitPeriod = waitPeriod;
	}

	/**
	 * @return the benefitPeriod
	 */
	public String getBenefitPeriod() {
		return benefitPeriod;
	}

	/**
	 * @param benefitPeriod the benefitPeriod to set
	 */
	public void setBenefitPeriod(String benefitPeriod) {
		this.benefitPeriod = benefitPeriod;
	}

	/**
	 * @return the salaryInsured
	 */
	public String getSalaryInsured() {
		return salaryInsured;
	}

	/**
	 * @param salaryInsured the salaryInsured to set
	 */
	public void setSalaryInsured(String salaryInsured) {
		this.salaryInsured = salaryInsured;
	}

	/**
	 * @return the superContribution
	 */
	public String getSuperContribution() {
		return superContribution;
	}

	/**
	 * @param superContribution the superContribution to set
	 */
	public void setSuperContribution(String superContribution) {
		this.superContribution = superContribution;
	}

	/**
	 * @return the fundOwnerId
	 */
	public String getFundOwnerId() {
		return fundOwnerId;
	}

	/**
	 * @param fundOwnerId the fundOwnerId to set
	 */
	public void setFundOwnerId(String fundOwnerId) {
		this.fundOwnerId = fundOwnerId;
	}

	/**
	 * @return the fundName
	 */
	public String getFundName() {
		return fundName;
	}

	/**
	 * @param fundName the fundName to set
	 */
	public void setFundName(String fundName) {
		this.fundName = fundName;
	}

	/**
	 * @return the fundPartyId
	 */
	public String getFundPartyId() {
		return fundPartyId;
	}

	/**
	 * @param fundPartyId the fundPartyId to set
	 */
	public void setFundPartyId(String fundPartyId) {
		this.fundPartyId = fundPartyId;
	}

	/**
	 * @return the fundRoleId
	 */
	public String getFundRoleId() {
		return fundRoleId;
	}

	/**
	 * @param fundRoleId the fundRoleId to set
	 */
	public void setFundRoleId(String fundRoleId) {
		this.fundRoleId = fundRoleId;
	}

	/**
	 * @return the lastUpdate
	 */
	public String getLastUpdate() {
		return lastUpdate;
	}

	/**
	 * @param lastUpdate the lastUpdate to set
	 */
	public void setLastUpdate(String lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	/**
	 * @return the mStatus
	 */
	public String getmStatus() {
		return mStatus;
	}

	/**
	 * @param mStatus the mStatus to set
	 */
	public void setmStatus(String mStatus) {
		this.mStatus = mStatus;
	}

	/**
	 * @return the claimActivity
	 */
	public Set<ClaimActivityDto> getClaimActivity() {
		return claimActivity;
	}

	/**
	 * @param claimActivity the claimActivity to set
	 */
	public void setClaimActivity(Set<ClaimActivityDto> claimActivity) {
		this.claimActivity = claimActivity;
	}

	/**
	 * @return the claimStatus
	 */
	public ClaimStatusDto getClaimStatus() {
		return claimStatus;
	}

	/**
	 * @param claimStatus the claimStatus to set
	 */
	public void setClaimStatus(ClaimStatusDto claimStatus) {
		this.claimStatus = claimStatus;
	}

	/**
	 * @return the claimPayment
	 */
	public ClaimPaymentDto getClaimPayment() {
		return claimPayment;
	}

	/**
	 * @param claimPayment the claimPayment to set
	 */
	public void setClaimPayment(ClaimPaymentDto claimPayment) {
		this.claimPayment = claimPayment;
	}

}
