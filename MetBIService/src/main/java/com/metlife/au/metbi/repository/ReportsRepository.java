package com.metlife.au.metbi.repository;

import java.util.List;

import com.metlife.au.metbi.dto.FundDto;
import com.metlife.au.metbi.dto.ReportFilter;
import com.metlife.au.metbi.dto.UWSearchFilter;
import com.metlife.au.metbi.dto.UWSearchResult;
import com.metlife.au.metbi.entity.Fund;
import com.metlife.au.metbi.entity.UnderwritingAction;
import com.metlife.au.metbi.entity.UnderwritingDetail;
import com.metlife.au.metbi.exception.ApplicationException;

/**
 * @author 672381
 *
 */

public interface ReportsRepository {

	public List<UnderwritingDetail> getUWStatusDetails(String queryParam, ReportFilter reportFilter)
			throws ApplicationException;

	public List<UnderwritingDetail> getUWSLADetails(String queryParam, ReportFilter reportFilter)
			throws ApplicationException;

	public List<UnderwritingDetail> getUWRequirementReport(String queryParam, ReportFilter reportFilter) throws ApplicationException;

	public List<UWSearchResult> getUWSaerch(UWSearchFilter uwSearchFilter) throws ApplicationException;
	
	public List<Fund> fundDetails(List<String> fundcodes) throws ApplicationException;

	public List<UnderwritingAction> getEventHistory(String fundcode, long applicationNo, String drilldownType) throws ApplicationException;

	public UnderwritingDetail getUWDrillDownReport(String fundcode, long applicationNo) throws ApplicationException;

}
