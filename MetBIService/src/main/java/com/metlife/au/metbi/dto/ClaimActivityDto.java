package com.metlife.au.metbi.dto;

import java.util.Date;

/**
 * 
 * @author 390092
 *
 */
public class ClaimActivityDto implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5208209284991091197L;

	private long activityId;

	private Long contactPointId;

	private long activityTypeCode;

	private long activityStatusCode;

	private Long outcomeCode;

	private Long scriptId;

	private String externalReference;

	private Long sourceSystem;

	private Date startDate;

	private Date endDate;

	private Integer standardSla;

	private Integer actualSla;

	private Long activityPerformer;

	private String slaMet;

	private Date plannedDate;

	private Integer dnis;

	private Integer numberUsed;

	private String comments;

	private String additionalInformation;

	private String sourceKey;

	private String srcTableName;

	private Date insDateTime;

	private Date updDateTime;

	private Long batchId;

	private long datafileId;

	private Integer versionNumber;

	private Date vldFmDt;

	private Date vldToDt;

	private Date effFmDt;

	private Date effToDt;

	private String mailComment;

	private EviewActivityDto eViewActivity;

	public ClaimActivityDto() {

	}

	/**
	 * @return the activityId
	 */
	public long getActivityId() {
		return activityId;
	}

	/**
	 * @param activityId the activityId to set
	 */
	public void setActivityId(long activityId) {
		this.activityId = activityId;
	}

	/**
	 * @return the contactPointId
	 */
	public Long getContactPointId() {
		return contactPointId;
	}

	/**
	 * @param contactPointId the contactPointId to set
	 */
	public void setContactPointId(Long contactPointId) {
		this.contactPointId = contactPointId;
	}

	/**
	 * @return the activityTypeCode
	 */
	public long getActivityTypeCode() {
		return activityTypeCode;
	}

	/**
	 * @param activityTypeCode the activityTypeCode to set
	 */
	public void setActivityTypeCode(long activityTypeCode) {
		this.activityTypeCode = activityTypeCode;
	}

	/**
	 * @return the activityStatusCode
	 */
	public long getActivityStatusCode() {
		return activityStatusCode;
	}

	/**
	 * @param activityStatusCode the activityStatusCode to set
	 */
	public void setActivityStatusCode(long activityStatusCode) {
		this.activityStatusCode = activityStatusCode;
	}

	/**
	 * @return the outcomeCode
	 */
	public Long getOutcomeCode() {
		return outcomeCode;
	}

	/**
	 * @param outcomeCode the outcomeCode to set
	 */
	public void setOutcomeCode(Long outcomeCode) {
		this.outcomeCode = outcomeCode;
	}

	/**
	 * @return the scriptId
	 */
	public Long getScriptId() {
		return scriptId;
	}

	/**
	 * @param scriptId the scriptId to set
	 */
	public void setScriptId(Long scriptId) {
		this.scriptId = scriptId;
	}

	/**
	 * @return the externalReference
	 */
	public String getExternalReference() {
		return externalReference;
	}

	/**
	 * @param externalReference the externalReference to set
	 */
	public void setExternalReference(String externalReference) {
		this.externalReference = externalReference;
	}

	/**
	 * @return the sourceSystem
	 */
	public Long getSourceSystem() {
		return sourceSystem;
	}

	/**
	 * @param sourceSystem the sourceSystem to set
	 */
	public void setSourceSystem(Long sourceSystem) {
		this.sourceSystem = sourceSystem;
	}

	/**
	 * @return the startDate
	 */
	public Date getStartDate() {
		return startDate;
	}

	/**
	 * @param startDate the startDate to set
	 */
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	/**
	 * @return the endDate
	 */
	public Date getEndDate() {
		return endDate;
	}

	/**
	 * @param endDate the endDate to set
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	/**
	 * @return the standardSla
	 */
	public Integer getStandardSla() {
		return standardSla;
	}

	/**
	 * @param standardSla the standardSla to set
	 */
	public void setStandardSla(Integer standardSla) {
		this.standardSla = standardSla;
	}

	/**
	 * @return the actualSla
	 */
	public Integer getActualSla() {
		return actualSla;
	}

	/**
	 * @param actualSla the actualSla to set
	 */
	public void setActualSla(Integer actualSla) {
		this.actualSla = actualSla;
	}

	/**
	 * @return the activityPerformer
	 */
	public Long getActivityPerformer() {
		return activityPerformer;
	}

	/**
	 * @param activityPerformer the activityPerformer to set
	 */
	public void setActivityPerformer(Long activityPerformer) {
		this.activityPerformer = activityPerformer;
	}

	/**
	 * @return the slaMet
	 */
	public String getSlaMet() {
		return slaMet;
	}

	/**
	 * @param slaMet the slaMet to set
	 */
	public void setSlaMet(String slaMet) {
		this.slaMet = slaMet;
	}

	/**
	 * @return the plannedDate
	 */
	public Date getPlannedDate() {
		return plannedDate;
	}

	/**
	 * @param plannedDate the plannedDate to set
	 */
	public void setPlannedDate(Date plannedDate) {
		this.plannedDate = plannedDate;
	}

	/**
	 * @return the dnis
	 */
	public Integer getDnis() {
		return dnis;
	}

	/**
	 * @param dnis the dnis to set
	 */
	public void setDnis(Integer dnis) {
		this.dnis = dnis;
	}

	/**
	 * @return the numberUsed
	 */
	public Integer getNumberUsed() {
		return numberUsed;
	}

	/**
	 * @param numberUsed the numberUsed to set
	 */
	public void setNumberUsed(Integer numberUsed) {
		this.numberUsed = numberUsed;
	}

	/**
	 * @return the comments
	 */
	public String getComments() {
		return comments;
	}

	/**
	 * @param comments the comments to set
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}

	/**
	 * @return the additionalInformation
	 */
	public String getAdditionalInformation() {
		return additionalInformation;
	}

	/**
	 * @param additionalInformation the additionalInformation to set
	 */
	public void setAdditionalInformation(String additionalInformation) {
		this.additionalInformation = additionalInformation;
	}

	/**
	 * @return the sourceKey
	 */
	public String getSourceKey() {
		return sourceKey;
	}

	/**
	 * @param sourceKey the sourceKey to set
	 */
	public void setSourceKey(String sourceKey) {
		this.sourceKey = sourceKey;
	}

	/**
	 * @return the srcTableName
	 */
	public String getSrcTableName() {
		return srcTableName;
	}

	/**
	 * @param srcTableName the srcTableName to set
	 */
	public void setSrcTableName(String srcTableName) {
		this.srcTableName = srcTableName;
	}

	/**
	 * @return the insDateTime
	 */
	public Date getInsDateTime() {
		return insDateTime;
	}

	/**
	 * @param insDateTime the insDateTime to set
	 */
	public void setInsDateTime(Date insDateTime) {
		this.insDateTime = insDateTime;
	}

	/**
	 * @return the updDateTime
	 */
	public Date getUpdDateTime() {
		return updDateTime;
	}

	/**
	 * @param updDateTime the updDateTime to set
	 */
	public void setUpdDateTime(Date updDateTime) {
		this.updDateTime = updDateTime;
	}

	/**
	 * @return the batchId
	 */
	public Long getBatchId() {
		return batchId;
	}

	/**
	 * @param batchId the batchId to set
	 */
	public void setBatchId(Long batchId) {
		this.batchId = batchId;
	}

	/**
	 * @return the datafileId
	 */
	public long getDatafileId() {
		return datafileId;
	}

	/**
	 * @param datafileId the datafileId to set
	 */
	public void setDatafileId(long datafileId) {
		this.datafileId = datafileId;
	}

	/**
	 * @return the versionNumber
	 */
	public Integer getVersionNumber() {
		return versionNumber;
	}

	/**
	 * @param versionNumber the versionNumber to set
	 */
	public void setVersionNumber(Integer versionNumber) {
		this.versionNumber = versionNumber;
	}

	/**
	 * @return the vldFmDt
	 */
	public Date getVldFmDt() {
		return vldFmDt;
	}

	/**
	 * @param vldFmDt the vldFmDt to set
	 */
	public void setVldFmDt(Date vldFmDt) {
		this.vldFmDt = vldFmDt;
	}

	/**
	 * @return the vldToDt
	 */
	public Date getVldToDt() {
		return vldToDt;
	}

	/**
	 * @param vldToDt the vldToDt to set
	 */
	public void setVldToDt(Date vldToDt) {
		this.vldToDt = vldToDt;
	}

	/**
	 * @return the effFmDt
	 */
	public Date getEffFmDt() {
		return effFmDt;
	}

	/**
	 * @param effFmDt the effFmDt to set
	 */
	public void setEffFmDt(Date effFmDt) {
		this.effFmDt = effFmDt;
	}

	/**
	 * @return the effToDt
	 */
	public Date getEffToDt() {
		return effToDt;
	}

	/**
	 * @param effToDt the effToDt to set
	 */
	public void setEffToDt(Date effToDt) {
		this.effToDt = effToDt;
	}

	/**
	 * @return the mailComment
	 */
	public String getMailComment() {
		return mailComment;
	}

	/**
	 * @param mailComment the mailComment to set
	 */
	public void setMailComment(String mailComment) {
		this.mailComment = mailComment;
	}

	/**
	 * @return the eViewActivity
	 */
	public EviewActivityDto geteViewActivity() {
		return eViewActivity;
	}

	/**
	 * @param eViewActivity the eViewActivity to set
	 */
	public void seteViewActivity(EviewActivityDto eViewActivity) {
		this.eViewActivity = eViewActivity;
	}

}
