package com.metlife.au.metbi.service;

import java.util.List;

import com.metlife.au.metbi.dto.FundDto;
import com.metlife.au.metbi.dto.ReportFilter;
import com.metlife.au.metbi.dto.UWDrillDownReport;
import com.metlife.au.metbi.dto.UWSearchFilter;
import com.metlife.au.metbi.dto.UWSearchResult;
import com.metlife.au.metbi.entity.UnderwritingDetail;
import com.metlife.au.metbi.exception.ApplicationException;

/**
 * @author 672381
 *
 */

public interface ReportsService {

	public List<UnderwritingDetail> getUWStatusDetails(String queryParam, ReportFilter reportFilter)
			throws ApplicationException;

	public List<UnderwritingDetail> getUWSLADetails(String queryParam, ReportFilter reportFilter)
			throws ApplicationException;

	public List<UnderwritingDetail> getUWRequirementReport(String queryParam, ReportFilter reportFilter) throws ApplicationException;

	public List<UWSearchResult> getUWSaerch(UWSearchFilter uwSearchFilter) throws ApplicationException;

	public List<FundDto> fundDetails(List<String> fundcodes) throws ApplicationException;

	public UWDrillDownReport getUWDrillDownReport(String fundcode, long applicationNo, String string) throws ApplicationException;
}
