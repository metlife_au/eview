package com.metlife.au.metbi.rsql;

import java.util.ArrayList;
import java.util.List;

public class RSQLGroupParameter extends RSQLParameter {
	
	private List<RSQLParameter> parameters = new ArrayList<>();

	public List<RSQLParameter> getParameters() {
		return parameters;
	}

	public void setParameters(List<RSQLParameter> parameters) {
		this.parameters = parameters;
	}

}
