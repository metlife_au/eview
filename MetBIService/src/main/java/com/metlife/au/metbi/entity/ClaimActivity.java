package com.metlife.au.metbi.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "ACTIVITY_CLM_FCT", schema = "DBUSDWW")
public class ClaimActivity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3932744451172762243L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ACTIVITY_ID", nullable = false)
	private long activityId;

	@Column(name = "CONTACT_POINT_ID")
	private Long contactPointId;

	@Column(name = "ACTIVITY_TYPE_CODE", nullable = false)
	private long activityTypeCode;

	@Column(name = "ACTIVITY_STATUS_CODE", nullable = false)
	private long activityStatusCode;

	@Column(name = "OUTCOME_CODE")
	private Long outcomeCode;

	@Column(name = "SCRIPT_ID")
	private Long scriptId;

	@Column(name = "EXTERNAL_REFERENCE", length = 100)
	private String externalReference;

	@Column(name = "SOURCE_SYSTEM")
	private Long sourceSystem;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "START_DATE", length = 26)
	private Date startDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "END_DATE", length = 26)
	private Date endDate;

	@Column(name = "STANDARD_SLA")
	private Integer standardSla;

	@Column(name = "ACTUAL_SLA")
	private Integer actualSla;

	@Column(name = "ACTIVITY_PERFORMER")
	private Long activityPerformer;

	@Column(name = "SLA_MET", length = 1)
	private String slaMet;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "PLANNED_DATE", length = 26)
	private Date plannedDate;

	@Column(name = "DNIS")
	private Integer dnis;

	@Column(name = "NUMBER_USED")
	private Integer numberUsed;

	@Column(name = "COMMENTS", length = 2000)
	private String comments;

	@Column(name = "ADDITIONAL_INFORMATION", length = 2000)
	private String additionalInformation;

	@Column(name = "SOURCE_KEY", length = 100)
	private String sourceKey;

	@Column(name = "SRC_TABLE_NAME", length = 50)
	private String srcTableName;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "INS_DATE_TIME", length = 26)
	private Date insDateTime;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "UPD_DATE_TIME", length = 26)
	private Date updDateTime;

	@Column(name = "BATCH_ID")
	private Long batchId;

	@Column(name = "DATAFILE_ID", nullable = false)
	private long datafileId;

	@Column(name = "VERSION_NUMBER")
	private Integer versionNumber;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "VLD_FM_DT", length = 26)
	private Date vldFmDt;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "VLD_TO_DT", length = 26)
	private Date vldToDt;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "EFF_FM_DT", length = 26)
	private Date effFmDt;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "EFF_TO_DT", length = 26)
	private Date effToDt;

	@Column(name = "MAIL_COMMENT", length = 50)
	private String mailComment;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "CLAIM", referencedColumnName = "CLAIM_ID")
	private ClaimDetails claimDetails;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "DESCRIPTION", referencedColumnName = "ACTIVITY_DESCRIPTION")
	private EviewActivity eViewActivity;

	public ClaimActivity() {

	}

	/**
	 * @return the activityId
	 */
	public long getActivityId() {
		return activityId;
	}

	/**
	 * @param activityId the activityId to set
	 */
	public void setActivityId(long activityId) {
		this.activityId = activityId;
	}

	/**
	 * @return the contactPointId
	 */
	public Long getContactPointId() {
		return contactPointId;
	}

	/**
	 * @param contactPointId the contactPointId to set
	 */
	public void setContactPointId(Long contactPointId) {
		this.contactPointId = contactPointId;
	}

	/**
	 * @return the activityTypeCode
	 */
	public long getActivityTypeCode() {
		return activityTypeCode;
	}

	/**
	 * @param activityTypeCode the activityTypeCode to set
	 */
	public void setActivityTypeCode(long activityTypeCode) {
		this.activityTypeCode = activityTypeCode;
	}

	/**
	 * @return the activityStatusCode
	 */
	public long getActivityStatusCode() {
		return activityStatusCode;
	}

	/**
	 * @param activityStatusCode the activityStatusCode to set
	 */
	public void setActivityStatusCode(long activityStatusCode) {
		this.activityStatusCode = activityStatusCode;
	}

	/**
	 * @return the outcomeCode
	 */
	public Long getOutcomeCode() {
		return outcomeCode;
	}

	/**
	 * @param outcomeCode the outcomeCode to set
	 */
	public void setOutcomeCode(Long outcomeCode) {
		this.outcomeCode = outcomeCode;
	}

	/**
	 * @return the scriptId
	 */
	public Long getScriptId() {
		return scriptId;
	}

	/**
	 * @param scriptId the scriptId to set
	 */
	public void setScriptId(Long scriptId) {
		this.scriptId = scriptId;
	}

	/**
	 * @return the externalReference
	 */
	public String getExternalReference() {
		return externalReference;
	}

	/**
	 * @param externalReference the externalReference to set
	 */
	public void setExternalReference(String externalReference) {
		this.externalReference = externalReference;
	}

	/**
	 * @return the sourceSystem
	 */
	public Long getSourceSystem() {
		return sourceSystem;
	}

	/**
	 * @param sourceSystem the sourceSystem to set
	 */
	public void setSourceSystem(Long sourceSystem) {
		this.sourceSystem = sourceSystem;
	}

	/**
	 * @return the startDate
	 */
	public Date getStartDate() {
		return startDate;
	}

	/**
	 * @param startDate the startDate to set
	 */
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	/**
	 * @return the endDate
	 */
	public Date getEndDate() {
		return endDate;
	}

	/**
	 * @param endDate the endDate to set
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	/**
	 * @return the standardSla
	 */
	public Integer getStandardSla() {
		return standardSla;
	}

	/**
	 * @param standardSla the standardSla to set
	 */
	public void setStandardSla(Integer standardSla) {
		this.standardSla = standardSla;
	}

	/**
	 * @return the actualSla
	 */
	public Integer getActualSla() {
		return actualSla;
	}

	/**
	 * @param actualSla the actualSla to set
	 */
	public void setActualSla(Integer actualSla) {
		this.actualSla = actualSla;
	}

	/**
	 * @return the activityPerformer
	 */
	public Long getActivityPerformer() {
		return activityPerformer;
	}

	/**
	 * @param activityPerformer the activityPerformer to set
	 */
	public void setActivityPerformer(Long activityPerformer) {
		this.activityPerformer = activityPerformer;
	}

	/**
	 * @return the slaMet
	 */
	public String getSlaMet() {
		return slaMet;
	}

	/**
	 * @param slaMet the slaMet to set
	 */
	public void setSlaMet(String slaMet) {
		this.slaMet = slaMet;
	}

	/**
	 * @return the plannedDate
	 */
	public Date getPlannedDate() {
		return plannedDate;
	}

	/**
	 * @param plannedDate the plannedDate to set
	 */
	public void setPlannedDate(Date plannedDate) {
		this.plannedDate = plannedDate;
	}

	/**
	 * @return the dnis
	 */
	public Integer getDnis() {
		return dnis;
	}

	/**
	 * @param dnis the dnis to set
	 */
	public void setDnis(Integer dnis) {
		this.dnis = dnis;
	}

	/**
	 * @return the numberUsed
	 */
	public Integer getNumberUsed() {
		return numberUsed;
	}

	/**
	 * @param numberUsed the numberUsed to set
	 */
	public void setNumberUsed(Integer numberUsed) {
		this.numberUsed = numberUsed;
	}

	/**
	 * @return the comments
	 */
	public String getComments() {
		return comments;
	}

	/**
	 * @param comments the comments to set
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}

	/**
	 * @return the additionalInformation
	 */
	public String getAdditionalInformation() {
		return additionalInformation;
	}

	/**
	 * @param additionalInformation the additionalInformation to set
	 */
	public void setAdditionalInformation(String additionalInformation) {
		this.additionalInformation = additionalInformation;
	}

	/**
	 * @return the sourceKey
	 */
	public String getSourceKey() {
		return sourceKey;
	}

	/**
	 * @param sourceKey the sourceKey to set
	 */
	public void setSourceKey(String sourceKey) {
		this.sourceKey = sourceKey;
	}

	/**
	 * @return the srcTableName
	 */
	public String getSrcTableName() {
		return srcTableName;
	}

	/**
	 * @param srcTableName the srcTableName to set
	 */
	public void setSrcTableName(String srcTableName) {
		this.srcTableName = srcTableName;
	}

	/**
	 * @return the insDateTime
	 */
	public Date getInsDateTime() {
		return insDateTime;
	}

	/**
	 * @param insDateTime the insDateTime to set
	 */
	public void setInsDateTime(Date insDateTime) {
		this.insDateTime = insDateTime;
	}

	/**
	 * @return the updDateTime
	 */
	public Date getUpdDateTime() {
		return updDateTime;
	}

	/**
	 * @param updDateTime the updDateTime to set
	 */
	public void setUpdDateTime(Date updDateTime) {
		this.updDateTime = updDateTime;
	}

	/**
	 * @return the batchId
	 */
	public Long getBatchId() {
		return batchId;
	}

	/**
	 * @param batchId the batchId to set
	 */
	public void setBatchId(Long batchId) {
		this.batchId = batchId;
	}

	/**
	 * @return the datafileId
	 */
	public long getDatafileId() {
		return datafileId;
	}

	/**
	 * @param datafileId the datafileId to set
	 */
	public void setDatafileId(long datafileId) {
		this.datafileId = datafileId;
	}

	/**
	 * @return the versionNumber
	 */
	public Integer getVersionNumber() {
		return versionNumber;
	}

	/**
	 * @param versionNumber the versionNumber to set
	 */
	public void setVersionNumber(Integer versionNumber) {
		this.versionNumber = versionNumber;
	}

	/**
	 * @return the vldFmDt
	 */
	public Date getVldFmDt() {
		return vldFmDt;
	}

	/**
	 * @param vldFmDt the vldFmDt to set
	 */
	public void setVldFmDt(Date vldFmDt) {
		this.vldFmDt = vldFmDt;
	}

	/**
	 * @return the vldToDt
	 */
	public Date getVldToDt() {
		return vldToDt;
	}

	/**
	 * @param vldToDt the vldToDt to set
	 */
	public void setVldToDt(Date vldToDt) {
		this.vldToDt = vldToDt;
	}

	/**
	 * @return the effFmDt
	 */
	public Date getEffFmDt() {
		return effFmDt;
	}

	/**
	 * @param effFmDt the effFmDt to set
	 */
	public void setEffFmDt(Date effFmDt) {
		this.effFmDt = effFmDt;
	}

	/**
	 * @return the effToDt
	 */
	public Date getEffToDt() {
		return effToDt;
	}

	/**
	 * @param effToDt the effToDt to set
	 */
	public void setEffToDt(Date effToDt) {
		this.effToDt = effToDt;
	}

	/**
	 * @return the mailComment
	 */
	public String getMailComment() {
		return mailComment;
	}

	/**
	 * @param mailComment the mailComment to set
	 */
	public void setMailComment(String mailComment) {
		this.mailComment = mailComment;
	}

	/**
	 * @return the claimDetails
	 */
	public ClaimDetails getClaimDetails() {
		return claimDetails;
	}

	/**
	 * @param claimDetails the claimDetails to set
	 */
	public void setClaimDetails(ClaimDetails claimDetails) {
		this.claimDetails = claimDetails;
	}

	/**
	 * @return the eViewActivity
	 */
	public EviewActivity geteViewActivity() {
		return eViewActivity;
	}

	/**
	 * @param eViewActivity the eViewActivity to set
	 */
	public void seteViewActivity(EviewActivity eViewActivity) {
		this.eViewActivity = eViewActivity;
	}

}
