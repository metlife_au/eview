package com.metlife.au.metbi.dto;

/**
 * 
 * @author 390092
 *
 */
public class EviewActivityDto implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9100422270787512716L;

	private Integer activityTypeId;

	private String description;

	public EviewActivityDto() {

	}

	/**
	 * @return the activityTypeId
	 */
	public Integer getActivityTypeId() {
		return activityTypeId;
	}

	/**
	 * @param activityTypeId the activityTypeId to set
	 */
	public void setActivityTypeId(Integer activityTypeId) {
		this.activityTypeId = activityTypeId;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

}
