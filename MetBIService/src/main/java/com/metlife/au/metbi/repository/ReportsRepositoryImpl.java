package com.metlife.au.metbi.repository;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import com.github.tennaito.rsql.jpa.JpaCriteriaQueryVisitor;
import com.metlife.au.metbi.constants.ReportConstants;
import com.metlife.au.metbi.dto.ReportFilter;
import com.metlife.au.metbi.dto.UWSearchFilter;
import com.metlife.au.metbi.dto.UWSearchResult;
import com.metlife.au.metbi.entity.ClaimDetails;
import com.metlife.au.metbi.entity.Fund;
import com.metlife.au.metbi.entity.UnderwritingAction;
import com.metlife.au.metbi.entity.UnderwritingDetail;
import com.metlife.au.metbi.entity.UnderwritingEvent;
import com.metlife.au.metbi.exception.ApplicationException;
import com.metlife.au.metbi.rsql.RSQLParameter;
import com.metlife.au.metbi.rsql.RSQLRootExpression;
import com.metlife.au.metbi.rsql.RSQLVisitorCust;
import com.metlife.au.metbi.utils.CommonUtils;
import com.metlife.au.metbi.utils.ReportsMapper;

import cz.jirutka.rsql.parser.RSQLParser;
import cz.jirutka.rsql.parser.ast.Node;
import cz.jirutka.rsql.parser.ast.RSQLVisitor;

/**
 * @author 672381
 *
 */

@Transactional
@Repository
public class ReportsRepositoryImpl implements ReportsRepository {

	private final Logger logger = LoggerFactory.getLogger(ReportsRepositoryImpl.class);

	@PersistenceContext
	EntityManager entityManager;

	public static final SimpleDateFormat tsFormatBeginSQL = new SimpleDateFormat(
			ReportConstants.SQL_TIMESTAMP_FORMAT_BEGIN);
	public static final SimpleDateFormat tsFormatEndSQL = new SimpleDateFormat(
			ReportConstants.SQL_TIMESTAMP_FORMAT_END);
	public static final SimpleDateFormat tsFormatSQL = new SimpleDateFormat(ReportConstants.SQL_TIMESTAMP_FORMAT);

	private ReportsMapper reportsMapper;

	@Autowired
	public ReportsRepositoryImpl(ReportsMapper reportsMapper) {
		this.reportsMapper = reportsMapper;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<UnderwritingDetail> getUWStatusDetails(String queryParam, ReportFilter reportFilter)
			throws ApplicationException {
		List<UnderwritingDetail> reportDetailsList = new ArrayList<UnderwritingDetail>();
		try {			
			StringBuffer sql_uw_status_report = new StringBuffer();
			Node rootNode = new RSQLParser().parse(queryParam);		
			RSQLVisitorCust visitor = new RSQLVisitorCust();		
			RSQLRootExpression exp = rootNode.accept(visitor);
			List<RSQLParameter> params=exp.getParameters();
			sql_uw_status_report.append(ReportConstants.QUERY_UWSTATUS_RUN_REPORT);
			sql_uw_status_report.append(getUWStatusCriteria(params));
			Query query = entityManager.createQuery(sql_uw_status_report.toString(), UnderwritingDetail.class);
			reportDetailsList = query.getResultList();
		} catch (Exception ex) {
			throw new ApplicationException("Exception");
		}
		return reportDetailsList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<UnderwritingDetail> getUWSLADetails(String queryParam, ReportFilter reportFilter)
			throws ApplicationException {
		List<UnderwritingDetail> reportDetailsList = new ArrayList<UnderwritingDetail>();
		try {
			StringBuffer sql_uw_sla_report = new StringBuffer();
			Node rootNode = new RSQLParser().parse(queryParam);		
			RSQLVisitorCust visitor = new RSQLVisitorCust();		
			RSQLRootExpression exp = rootNode.accept(visitor);
			List<RSQLParameter> params=exp.getParameters();
			sql_uw_sla_report.append(ReportConstants.QUERY_UWSLA);
			sql_uw_sla_report.append(getUWStatusCriteria(params));
			Query query = entityManager.createQuery(sql_uw_sla_report.toString(), UnderwritingDetail.class);
			reportDetailsList = query.getResultList();

		} catch (Exception ex) {
			throw new ApplicationException("Exception");
		}
		return reportDetailsList;
	}

	@SuppressWarnings("unchecked")
	public List<UnderwritingDetail> getUWRequirementReport(String queryParam, ReportFilter reportFilter) throws ApplicationException {
		List<UnderwritingDetail> reportDetailsList = new ArrayList<UnderwritingDetail>();
		try {
			StringBuffer sql_uw_sla_report = new StringBuffer();
			Node rootNode = new RSQLParser().parse(queryParam);		
			RSQLVisitorCust visitor = new RSQLVisitorCust();		
			RSQLRootExpression exp = rootNode.accept(visitor);
			List<RSQLParameter> params=exp.getParameters();
			sql_uw_sla_report.append(ReportConstants.QUERY_UWREQUIREMENT);
			sql_uw_sla_report.append(getUWStatusCriteria(params));
			Query query = entityManager.createQuery(sql_uw_sla_report.toString(), UnderwritingDetail.class);
			reportDetailsList = query.getResultList();

		} catch (Exception ex) {
			throw new ApplicationException("Exception");
		}
		return reportDetailsList;
	}

	private <T> CriteriaQuery<T> getCriteriaQuery(String queryString,
			RSQLVisitor<CriteriaQuery<T>, EntityManager> visitor) {
		Node rootNode;
		CriteriaQuery<T> query;
		try {
			rootNode = new RSQLParser().parse(queryString);
			query = rootNode.accept(visitor, entityManager);
		} catch (Exception e) {
			logger.error("An error happened while executing RSQL query", e);
			throw new IllegalArgumentException(e.getMessage());
		}
		return query;
	}

	public String getUWStatusCriteria(List<RSQLParameter> params) throws ApplicationException {
		StringBuffer criteriaQuery = new StringBuffer();
		try {
			String statusValue="";
			String applicationStartDate="";
			String applicationEndDate="";
			boolean dateCheckFlag=false;
			for(RSQLParameter param :params) {
				if (param.getParameterName().equalsIgnoreCase("firstfame")) {
					criteriaQuery.append(" and lcase(fct.firstName) like lcase('")
							.append(param.getParameterSingleValue().trim()).append("%')");
				}
				if (param.getParameterName().equalsIgnoreCase("surName")) {
					criteriaQuery.append(" and lcase(fct.surName) like lcase('").append(param.getParameterSingleValue().trim())
							.append("%')");
				}
				if (param.getParameterName().equalsIgnoreCase("clientrefno")) {
					criteriaQuery.append(" and lcase(fct.clientReferenceNo) like lcase('")
							.append(param.getParameterSingleValue().trim()).append("%')");
				}
				if (param.getParameterName().equalsIgnoreCase("fundName")) {
					criteriaQuery.append(" and rtrim(fct.fundName) like '").append(param.getParameterSingleValue()).append("'");
				}			
			
				if (param.getParameterName().equalsIgnoreCase("applicationNo")) {
					criteriaQuery.append("	and fct.applicationNo=").append(param.getParameterSingleValue());
				}
	
				if (param.getParameterName().equalsIgnoreCase("memberDob")) {
					criteriaQuery.append(" and fct.memberDob = '").append(tsFormatSQL.format(param.getParameterSingleValue()))
							.append("' ");
				}
				if (param.getParameterName().equalsIgnoreCase("source")) {
					criteriaQuery.append(" and lcase(fct.commMedium) like lcase('").append(param.getParameterSingleValue())
							.append("%')");
				}
				if (param.getParameterName().equalsIgnoreCase("status")) {
					statusValue=param.getParameterSingleValue();
				}
				if (!dateCheckFlag && param.getParameterName().equalsIgnoreCase("applicationDate")) {
					applicationStartDate=param.getParameterSingleValue();
					dateCheckFlag = true;
				}else if(dateCheckFlag && param.getParameterName().equalsIgnoreCase("applicationDate")) {
					applicationEndDate=param.getParameterSingleValue();
					System.out.println(tsFormatBeginSQL.parse(applicationStartDate));
					System.out.println(tsFormatBeginSQL.format(tsFormatBeginSQL.parse(applicationStartDate)));
				}
			}
			if (StringUtils.isEmpty(statusValue)) {
				criteriaQuery.append("	and ").append("fct.applicationDate").append(" between '");
				criteriaQuery.append(tsFormatBeginSQL.format(tsFormatBeginSQL.parse(applicationStartDate))).append("'");
				criteriaQuery.append(" AND '").append(tsFormatEndSQL.format(tsFormatEndSQL.parse(applicationEndDate)))
						.append("'");
			} else if (ReportConstants.EQ_UW_STS_OPEN_STATUS.equals(statusValue)) {
				criteriaQuery.append(" and (fct.statusCode = 101 OR fct.statusCode = 102) ");
				criteriaQuery.append("	and ").append("fct.applicationDate").append(" between '");
				criteriaQuery.append(tsFormatBeginSQL.format(tsFormatBeginSQL.parse(applicationStartDate))).append("'");
				criteriaQuery.append(" AND '").append(tsFormatEndSQL.format(tsFormatEndSQL.parse(applicationEndDate)))
						.append("'");
			} else if (ReportConstants.EQ_UW_STS_CLOSED_STATUS.equals(statusValue)) {
				criteriaQuery.append(" and (fct.statusCode = 106 OR fct.statusCode = 108 OR fct.statusCode = 110) and ")
						.append("fct.applicationDate").append(" between '");
				criteriaQuery.append(tsFormatBeginSQL.format(tsFormatBeginSQL.parse(applicationStartDate))).append("'");
				criteriaQuery.append(" AND '").append(tsFormatEndSQL.format(tsFormatEndSQL.parse(applicationEndDate)))
						.append("' ");
			}
		} catch (Exception ex) {
			throw new ApplicationException("Exception");
		}

		return criteriaQuery.toString();
	}
	
	@Override
	public List<UWSearchResult> getUWSaerch(UWSearchFilter uwSearchFilter) throws ApplicationException {
		// TODO Auto-generated method stub
		return null;
	}

	public List<Fund> fundDetails(List<String> fundcodes) throws ApplicationException{
		try {
			CriteriaBuilder cb = entityManager.getCriteriaBuilder();
			CriteriaQuery<Fund> query = cb.createQuery(Fund.class);
			Root<Fund> root = query.from(Fund.class);
			query.select(root);
			if(fundcodes.size()>0) {
				List<String> parentList = fundcodes;
				Expression<String> parentExpression = root.get("fundCode");
				Predicate parentPredicate = parentExpression.in(parentList);
				query.where(parentPredicate);
			}
			query.orderBy(cb.asc(root.get("fundName")));
			return entityManager.createQuery(query).getResultList();
		}catch (Exception ex) {
			throw new ApplicationException("Exception");
		}
	}
	
	@Override
	public UnderwritingDetail getUWDrillDownReport(String fundcode, long applicationNo)
			throws ApplicationException {
		// TODO Auto-generated method stub
		List<UnderwritingDetail> reportDetailsList = new ArrayList<UnderwritingDetail>();
		try {

			Query query = entityManager.createQuery(ReportConstants.QUERY_UW_REPORT_DRILLDOWN,
					UnderwritingDetail.class);
			query.setParameter("appno", applicationNo);
			query.setParameter("selfundname", fundcode);
			reportDetailsList = query.getResultList();
			if (reportDetailsList.get(0) != null)
				return reportDetailsList.get(0);

		} catch (Exception e) {
			throw new ApplicationException("Exception");
			// TODO: handle exception
		}
		return null;
	}


	@Override
	public List<UnderwritingAction> getEventHistory(String fundcode, long applicationNo,String drilldownType) throws ApplicationException {
		// TODO Auto-generated method stub
		List<UnderwritingAction> reportDetailsList = new ArrayList<UnderwritingAction>();
		Query query = null;
		try {
			if (drilldownType.equalsIgnoreCase(ReportConstants.STATUS_EVENT_HISTORY)) {
				 query = entityManager.createQuery(ReportConstants.QUERY_UW_STATUS_EVENTHISTORY, UnderwritingAction.class);
			} else if (drilldownType.equalsIgnoreCase(ReportConstants.REQUIREMENT_EVENT_HISTORY)) {
				 query = entityManager.createQuery(ReportConstants.QUERY_UW_REQUIREMENT_EVENTHISTORY, UnderwritingAction.class);
			}
			query.setParameter("appno", applicationNo);
			query.setParameter("selfundname", fundcode);
			reportDetailsList = query.getResultList();
		} catch (Exception e) {
			// TODO: handle exception
			throw new ApplicationException("ee");
		}
		return reportDetailsList;
	}
	
	
}
