/**
 * 
 */
package com.metlife.au.metbi.dto;

import java.io.Serializable;

/**
 * @author 672381
 *
 */
public class ClaimStatusEmbeddedDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1286443781291780241L;

	private String claimSatus;

	private String productType;

	/**
	 * 
	 */
	public ClaimStatusEmbeddedDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the claimSatus
	 */
	public String getClaimSatus() {
		return claimSatus;
	}

	/**
	 * @param claimSatus the claimSatus to set
	 */
	public void setClaimSatus(String claimSatus) {
		this.claimSatus = claimSatus;
	}

	/**
	 * @return the productType
	 */
	public String getProductType() {
		return productType;
	}

	/**
	 * @param productType the productType to set
	 */
	public void setProductType(String productType) {
		this.productType = productType;
	}

}
