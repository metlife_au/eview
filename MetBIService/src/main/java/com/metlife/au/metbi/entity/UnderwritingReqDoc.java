package com.metlife.au.metbi.entity;


import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "REQ_URL", schema = "DBUSDWW")
public class UnderwritingReqDoc {
	@Id
	@Column(name = "REQUIREMENT", nullable = false)
	private String requirement;
	
	@Column(name = "DOCUMENT")
	private String document;
	

	@OneToMany(mappedBy = "underwritingReqDoc", orphanRemoval = true)
	private Set<UnderwritingAction> underwritingAction;
	

	public String getRequirement() {
		return requirement;
	}

	public void setRequirement(String requirement) {
		this.requirement = requirement;
	}

	public String getDocument() {
		return document;
	}

	public void setDocument(String document) {
		this.document = document;
	}

	public Set<UnderwritingAction> getUnderwritingAction() {
		return underwritingAction;
	}

	public void setUnderwritingAction(Set<UnderwritingAction> underwritingAction) {
		this.underwritingAction = underwritingAction;
	}

	

}
