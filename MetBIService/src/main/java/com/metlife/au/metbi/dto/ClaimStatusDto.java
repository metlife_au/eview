package com.metlife.au.metbi.dto;

import java.io.Serializable;

/**
 * 
 * @author 390092
 *
 */
public class ClaimStatusDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7389269260938309071L;

	private ClaimStatusEmbeddedDto claimStatusEmbedded;

	private String statusDescription;

	/**
	 * @return the statusDescription
	 */
	public String getStatusDescription() {
		return statusDescription;
	}

	/**
	 * @param statusDescription the statusDescription to set
	 */
	public void setStatusDescription(String statusDescription) {
		this.statusDescription = statusDescription;
	}

	/**
	 * @return the claimStatusEmbedded
	 */
	public ClaimStatusEmbeddedDto getClaimStatusEmbedded() {
		return claimStatusEmbedded;
	}

	/**
	 * @param claimStatusEmbedded the claimStatusEmbedded to set
	 */
	public void setClaimStatusEmbedded(ClaimStatusEmbeddedDto claimStatusEmbedded) {
		this.claimStatusEmbedded = claimStatusEmbedded;
	}

}
