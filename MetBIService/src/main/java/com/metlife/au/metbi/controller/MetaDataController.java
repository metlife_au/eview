/**
 * 
 */
package com.metlife.au.metbi.controller;

import java.util.ArrayList;
import java.util.List;

import org.assertj.core.util.Arrays;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.metlife.au.metbi.dto.FundDto;
import com.metlife.au.metbi.exception.ApplicationException;
import com.metlife.au.metbi.service.ReportsService;
import com.metlife.au.metbi.utils.ReportsMapper;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @author 672381
 *
 */
@Api(value = "/api/v1/application/metadata")
@RestController
@RequestMapping("/api/v1/application/metadata")
public class MetaDataController {
	
	private final Logger logger = LoggerFactory.getLogger(MetaDataController.class.getName());

	private ReportsService reportsService;	

	@Autowired
	public MetaDataController(ReportsService reportsService, ReportsMapper reportsMapper) {
		this.reportsService = reportsService;
		
	}
	
	@ApiOperation(value = "/fund", notes = "To retrive the fund details")
	@GetMapping("/fund")
	public ResponseEntity<List<FundDto>> fundDetails() {
		logger.info("MetaDataController :  fundDetails start");
		List<FundDto> fundList = new ArrayList<FundDto>();
		List<String> fundcodes=new ArrayList<String>();
		ResponseEntity<List<FundDto>> resEntity = new ResponseEntity<>(HttpStatus.NO_CONTENT);
		try {			
			fundcodes=new ArrayList<String>();			
			fundList=reportsService.fundDetails(fundcodes);
			resEntity = new ResponseEntity<>(fundList, HttpStatus.OK);
		} catch (ApplicationException ex) {
			resEntity = new ResponseEntity<>(fundList, HttpStatus.EXPECTATION_FAILED);
			logger.info("MetaDataController :  fundDetails catch block : " + ex.getMessage());
		} catch (Exception ex) {
			resEntity = new ResponseEntity<>(fundList, HttpStatus.EXPECTATION_FAILED);
			logger.info("MetaDataController :  fundDetails catch block : " + ex.getMessage());
		}
		logger.info("MetaDataController :  fundDetails end");
		return resEntity;
	}

	@SuppressWarnings("unchecked")
	@ApiOperation(value = "/fund/{fundcode}", notes = "To retrive the fund details based on fund codes")
	@GetMapping("/fund/{fundcode}")
	public ResponseEntity<List<FundDto>> fundDetailsByFuncode(
			@PathVariable(value = "fundcode") String[] fundcode ) {
		logger.info("MetaDataController :  fundDetailsByFuncode start");
		List<FundDto> fundList = new ArrayList<FundDto>();
		List<String> fundcodes=new ArrayList<String>();
		ResponseEntity<List<FundDto>> resEntity = new ResponseEntity<>(HttpStatus.NO_CONTENT);
		try {
			if(fundcode!=null && fundcode.length>0) {
				fundcodes=new ArrayList(Arrays.asList(fundcode));
			}
			fundList=reportsService.fundDetails(fundcodes);
			resEntity = new ResponseEntity<>(fundList, HttpStatus.OK);
		} catch (ApplicationException ex) {
			resEntity = new ResponseEntity<>(fundList, HttpStatus.EXPECTATION_FAILED);
			logger.info("MetaDataController :  fundDetailsByFuncode catch block : " + ex.getMessage());
		} catch (Exception ex) {
			resEntity = new ResponseEntity<>(fundList, HttpStatus.EXPECTATION_FAILED);
			logger.info("MetaDataController :  fundDetailsByFuncode catch block : " + ex.getMessage());
		}
		logger.info("MetaDataController :  fundDetailsByFuncode end");
		return resEntity;
	}

}
