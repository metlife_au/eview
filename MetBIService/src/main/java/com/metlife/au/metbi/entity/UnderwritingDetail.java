package com.metlife.au.metbi.entity;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * @author 672381
 *
 */

@Entity
@Table(name = "FCT_UW_DTL", schema = "DBUSDWW")
public class UnderwritingDetail {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ENDORSEMENT_ID")
	private Long endorsementId;

	@Column(name = "APPLICATIONNO")
	private Long applicationNo;

	@Column(name = "FUND_NAME")
	private String fundName;

	@Column(name = "FUND_CODE")
	private String fundCode;

	@Column(name = "EXT_MEMBER_NO")
	private String clientReferenceNo = null;

	@Column(name = "FIRSTNAME")
	private String firstName;

	@Column(name = "SURNAME")
	private String surName;

	@Column(name = "DATEOFBIRTH")
	private String memberDob;

	@Column(name = "GENDER")
	private String memberGender;

	@Column(name = "MEMBER_TYPE")
	private String memberType;

	@Column(name = "APPLICATION_DATE")
	private String applicationDate;

	@Column(name = "STATUS_DESCRIPTION")
	private String status;

	@Column(name = "STATUS_CODE")
	private String statusCode;

	@Column(name = "SOURCE_SYSTEM")
	private String source;

	@Column(name = "TOTAL_DEATH_COVER")
	private String totalDeathCover;

	@Column(name = "TOTAL_TRAUMA_COVER")
	private String totalTraumaCover;

	@Column(name = "TOTAL_TPD_COVER")
	private String totalTPDCover;

	@Column(name = "TOTAL_SCI_COVER")
	private String totalSCICover;

	@Column(name = "REQUESTEDWAITPERIOD")
	private String requestWaitPeriod;

	@Column(name = "REQUESTEDBENEFITPERIOD")
	private String requestBenefitPeriod;

	@Column(name = "EXISTING_DEATH_GROUP_COVER")
	private String extDeathGroupCover;

	@Column(name = "EXISTING_TPD_GROUP_COVER")
	private String extTPDGroupCover;

	@Column(name = "EXISTING_TRAUMA_GROUP_COVER")
	private String extTraumaGroupCover;

	@Column(name = "EXISTING_SCI_COVER")
	private String extSCICover;

	@Column(name = "LAST_ACTION_DATE")
	private String lastActionDate;

	@Column(name = "SCI_BENEFIT_PERIOD")
	private String sciBenefitPeriod;

	@Column(name = "SCI_WAIT_PERIOD")
	private String sciWaitPeriod;

	@Column(name = "ACCEPTED_TOTAL_DEATH_COVER")
	private String accTotalDeathCover;

	@Column(name = "ACCEPTED_TOTAL_TPD_COVER")
	private String accTotalTPDCover;

	@Column(name = "ACCEPTED_TOTAL_TRAUMA_COVER")
	private String accTotalTraumaCover;

	@Column(name = "ACCEPTED_TOTAL_SCI_COVER")
	private String accTotalSCICover;

	@Column(name = "FINALBENEFITPERIOD")
	private String finalBenefitPeriod;

	@Column(name = "FINALWAITPERIOD")
	private String finalWaitPeriod;

	@Column(name = "GL_AAL_DEATH_COVER")
	private String glAALDeathCover;

	@Column(name = "GL_AAL_TPD_COVER")
	private String glAALTPDCover;

	@Column(name = "GL_AAL_TRAUMA_COVER")
	private String glAALTraumaCover;

	@Column(name = "SCI_AAL")
	private String sciAAL;

	@Column(name = "TOT_DEATH_COVER_DECISION")
	private String totDeathCoverDecison;

	@Column(name = "TOT_TPD_COVER_DECISION")
	private String totTPDCoverDecison;

	@Column(name = "TOT_TRA_COVER_DECISION")
	private String totTraCoverDecison;

	@Column(name = "TOT_SCI_COVER_DECISION")
	private String totSCICoverDecison;

	@Column(name = "TOT_DEATH_COVER_LOADING")
	private String totDeathCoverLoding;

	@Column(name = "TOT_TPD_COVER_LOADING")
	private String totTPDCoverLoding;

	@Column(name = "TOT_TRA_COVER_LOADING")
	private String totTraCoverLoding;

	@Column(name = "TOT_SCI_COVER_LOADING")
	private String totSCICoverLoding;

	@Column(name = "TOT_DEATH_COVER_EXC")
	private String totDeathCoverExc;

	@Column(name = "GL_DEATH_FORWARD_COVER")
	private String glDeathFwdCover;

	@Column(name = "GL_TPD_FORWARD_COVER")
	private String glTPDFwdCover;

	@Column(name = "GL_TRAUMA_FORWARD_COVER")
	private String glTraFwdCover;

	@Column(name = "SCI_FORWARD_COVER")
	private String sciFwdCover;

	@Column(name = "GL_POLICY_NO")
	private String glPolicyNo;

	@Column(name = "SCI_POLICY_NO")
	private String sciPolicyNo;

	@Column(name = "COMMUNICATION_MEDIUM")
	private String commMedium;
	
	@Column(name = "UW_NAME") 
	private String uwName;
	
	@Column(name = "REQUEST_KIND") 
	private String requestKind;
	 
	@Transient
	private String requirements;
	
	@Transient
	private String decisionLetterPath;
	

	@OneToMany(mappedBy = "underwritingDetail", fetch = FetchType.EAGER)
	private Set<UnderwritingAction> underwritingAction;

	public UnderwritingDetail() {

	}

	/**
	 * @param applicationNo
	 * @param fundName
	 * @param fundCode
	 * @param extMemberNo
	 * @param firstName
	 * @param surName
	 * @param memberDob
	 * @param memberGender
	 * @param memberType
	 * @param applicationDate
	 * @param status
	 * @param source
	 * @param totalDeathCover
	 * @param totalTraumaCover
	 * @param totalTPDCover
	 * @param totalSCICover
	 * @param requestWaitPeriod
	 * @param requestBenefitPeriod
	 * @param extDeathGroupCover
	 * @param extTPDGroupCover
	 * @param extSCICover
	 * @param lastActionDate
	 * @param sciBenefitPeriod
	 * @param sciWaitPeriod
	 * @param accTotalDeathCover
	 * @param accTotalTPDCover
	 * @param accTotalTraumaCover
	 * @param accTotalSCICover
	 * @param finalBenefitPeriod
	 * @param finalWaitPeriod
	 * @param glAALDeathCover
	 * @param glAALTPDCover
	 * @param glAALTraumaCover
	 * @param sciAAL
	 * @param totDeathCoverDecison
	 * @param totTPDCoverDecison
	 * @param totTraCoverDecison
	 * @param totSCICoverDecison
	 * @param totDeathCoverLoding
	 * @param totTPDCoverLoding
	 * @param totTraCoverLoding
	 * @param totSCICoverLoding
	 * @param totDeathCoverExc
	 * @param glDeathFwdCover
	 * @param glTPDFwdCover
	 * @param glTraFwdCover
	 * @param sciFwdCover
	 * @param glPolicyNo
	 * @param sciPolicyNo
	 * @param requirements
	 */
	public UnderwritingDetail(Long applicationNo, String fundName, String fundCode, String clientReferenceNo,
			String firstName, String surName, String memberDob, String memberGender, String memberType,
			String applicationDate, String status, String source, String totalDeathCover, String totalTraumaCover,
			String totalTPDCover, String totalSCICover, String requestWaitPeriod, String requestBenefitPeriod,
			String extDeathGroupCover, String extTPDGroupCover, String extSCICover, String lastActionDate,
			String sciBenefitPeriod, String sciWaitPeriod, String accTotalDeathCover, String accTotalTPDCover,
			String accTotalTraumaCover, String accTotalSCICover, String finalBenefitPeriod, String finalWaitPeriod,
			String glAALDeathCover, String glAALTPDCover, String glAALTraumaCover, String sciAAL,
			String totDeathCoverDecison, String totTPDCoverDecison, String totTraCoverDecison,
			String totSCICoverDecison, String totDeathCoverLoding, String totTPDCoverLoding, String totTraCoverLoding,
			String totSCICoverLoding, String totDeathCoverExc, String glDeathFwdCover, String glTPDFwdCover,
			String glTraFwdCover, String sciFwdCover, String glPolicyNo, String sciPolicyNo, String requirements,String uwName,String requestKind) {
		super();
		this.applicationNo = applicationNo;
		this.fundName = fundName;
		this.fundCode = fundCode;
		this.clientReferenceNo = clientReferenceNo;
		this.firstName = firstName;
		this.surName = surName;
		this.memberDob = memberDob;
		this.memberGender = memberGender;
		this.memberType = memberType;
		this.applicationDate = applicationDate;
		this.status = status;
		this.source = source;
		this.totalDeathCover = totalDeathCover;
		this.totalTraumaCover = totalTraumaCover;
		this.totalTPDCover = totalTPDCover;
		this.totalSCICover = totalSCICover;
		this.requestWaitPeriod = requestWaitPeriod;
		this.requestBenefitPeriod = requestBenefitPeriod;
		this.extDeathGroupCover = extDeathGroupCover;
		this.extTPDGroupCover = extTPDGroupCover;
		this.extSCICover = extSCICover;
		this.lastActionDate = lastActionDate;
		this.sciBenefitPeriod = sciBenefitPeriod;
		this.sciWaitPeriod = sciWaitPeriod;
		this.accTotalDeathCover = accTotalDeathCover;
		this.accTotalTPDCover = accTotalTPDCover;
		this.accTotalTraumaCover = accTotalTraumaCover;
		this.accTotalSCICover = accTotalSCICover;
		this.finalBenefitPeriod = finalBenefitPeriod;
		this.finalWaitPeriod = finalWaitPeriod;
		this.glAALDeathCover = glAALDeathCover;
		this.glAALTPDCover = glAALTPDCover;
		this.glAALTraumaCover = glAALTraumaCover;
		this.sciAAL = sciAAL;
		this.totDeathCoverDecison = totDeathCoverDecison;
		this.totTPDCoverDecison = totTPDCoverDecison;
		this.totTraCoverDecison = totTraCoverDecison;
		this.totSCICoverDecison = totSCICoverDecison;
		this.totDeathCoverLoding = totDeathCoverLoding;
		this.totTPDCoverLoding = totTPDCoverLoding;
		this.totTraCoverLoding = totTraCoverLoding;
		this.totSCICoverLoding = totSCICoverLoding;
		this.totDeathCoverExc = totDeathCoverExc;
		this.glDeathFwdCover = glDeathFwdCover;
		this.glTPDFwdCover = glTPDFwdCover;
		this.glTraFwdCover = glTraFwdCover;
		this.sciFwdCover = sciFwdCover;
		this.glPolicyNo = glPolicyNo;
		this.sciPolicyNo = sciPolicyNo;
		this.requirements = requirements;
		this.requestKind = requestKind;
		this.uwName = uwName;
	}

	/**
	 * @return the applicationNo
	 */
	public Long getApplicationNo() {
		return applicationNo;
	}

	/**
	 * @param applicationNo the applicationNo to set
	 */
	public void setApplicationNo(Long applicationNo) {
		this.applicationNo = applicationNo;
	}

	/**
	 * @return the fundName
	 */
	public String getFundName() {
		return fundName;
	}

	/**
	 * @param fundName the fundName to set
	 */
	public void setFundName(String fundName) {
		this.fundName = fundName;
	}

	/**
	 * @return the fundCode
	 */
	public String getFundCode() {
		return fundCode;
	}

	/**
	 * @param fundCode the fundCode to set
	 */
	public void setFundCode(String fundCode) {
		this.fundCode = fundCode;
	}

	public String getClientReferenceNo() {
		return clientReferenceNo;
	}

	public void setClientReferenceNo(String clientReferenceNo) {
		this.clientReferenceNo = clientReferenceNo;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the surName
	 */
	public String getSurName() {
		return surName;
	}

	/**
	 * @param surName the surName to set
	 */
	public void setSurName(String surName) {
		this.surName = surName;
	}

	/**
	 * @return the memberDob
	 */
	public String getMemberDob() {
		return memberDob;
	}

	/**
	 * @param memberDob the memberDob to set
	 */
	public void setMemberDob(String memberDob) {
		this.memberDob = memberDob;
	}

	/**
	 * @return the memberGender
	 */
	public String getMemberGender() {
		return memberGender;
	}

	/**
	 * @param memberGender the memberGender to set
	 */
	public void setMemberGender(String memberGender) {
		this.memberGender = memberGender;
	}

	/**
	 * @return the memberType
	 */
	public String getMemberType() {
		return memberType;
	}

	/**
	 * @param memberType the memberType to set
	 */
	public void setMemberType(String memberType) {
		this.memberType = memberType;
	}

	/**
	 * @return the applicationDate
	 */
	public String getApplicationDate() {
		return applicationDate;
	}

	/**
	 * @param applicationDate the applicationDate to set
	 */
	public void setApplicationDate(String applicationDate) {
		this.applicationDate = applicationDate;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the source
	 */
	public String getSource() {
		return source;
	}

	/**
	 * @param source the source to set
	 */
	public void setSource(String source) {
		this.source = source;
	}

	/**
	 * @return the totalDeathCover
	 */
	public String getTotalDeathCover() {
		return totalDeathCover;
	}

	/**
	 * @param totalDeathCover the totalDeathCover to set
	 */
	public void setTotalDeathCover(String totalDeathCover) {
		this.totalDeathCover = totalDeathCover;
	}

	/**
	 * @return the totalTraumaCover
	 */
	public String getTotalTraumaCover() {
		return totalTraumaCover;
	}

	/**
	 * @param totalTraumaCover the totalTraumaCover to set
	 */
	public void setTotalTraumaCover(String totalTraumaCover) {
		this.totalTraumaCover = totalTraumaCover;
	}

	/**
	 * @return the totalTPDCover
	 */
	public String getTotalTPDCover() {
		return totalTPDCover;
	}

	/**
	 * @param totalTPDCover the totalTPDCover to set
	 */
	public void setTotalTPDCover(String totalTPDCover) {
		this.totalTPDCover = totalTPDCover;
	}

	/**
	 * @return the totalSCICover
	 */
	public String getTotalSCICover() {
		return totalSCICover;
	}

	/**
	 * @param totalSCICover the totalSCICover to set
	 */
	public void setTotalSCICover(String totalSCICover) {
		this.totalSCICover = totalSCICover;
	}

	/**
	 * @return the requestWaitPeriod
	 */
	public String getRequestWaitPeriod() {
		return requestWaitPeriod;
	}

	/**
	 * @param requestWaitPeriod the requestWaitPeriod to set
	 */
	public void setRequestWaitPeriod(String requestWaitPeriod) {
		this.requestWaitPeriod = requestWaitPeriod;
	}

	/**
	 * @return the requestBenefitPeriod
	 */
	public String getRequestBenefitPeriod() {
		return requestBenefitPeriod;
	}

	/**
	 * @param requestBenefitPeriod the requestBenefitPeriod to set
	 */
	public void setRequestBenefitPeriod(String requestBenefitPeriod) {
		this.requestBenefitPeriod = requestBenefitPeriod;
	}

	/**
	 * @return the extDeathGroupCover
	 */
	public String getExtDeathGroupCover() {
		return extDeathGroupCover;
	}

	/**
	 * @param extDeathGroupCover the extDeathGroupCover to set
	 */
	public void setExtDeathGroupCover(String extDeathGroupCover) {
		this.extDeathGroupCover = extDeathGroupCover;
	}

	/**
	 * @return the extTPDGroupCover
	 */
	public String getExtTPDGroupCover() {
		return extTPDGroupCover;
	}

	/**
	 * @param extTPDGroupCover the extTPDGroupCover to set
	 */
	public void setExtTPDGroupCover(String extTPDGroupCover) {
		this.extTPDGroupCover = extTPDGroupCover;
	}

	/**
	 * @return the extSCICover
	 */
	public String getExtSCICover() {
		return extSCICover;
	}

	/**
	 * @param extSCICover the extSCICover to set
	 */
	public void setExtSCICover(String extSCICover) {
		this.extSCICover = extSCICover;
	}

	/**
	 * @return the lastActionDate
	 */
	public String getLastActionDate() {
		return lastActionDate;
	}

	/**
	 * @param lastActionDate the lastActionDate to set
	 */
	public void setLastActionDate(String lastActionDate) {
		this.lastActionDate = lastActionDate;
	}

	/**
	 * @return the sciBenefitPeriod
	 */
	public String getSciBenefitPeriod() {
		return sciBenefitPeriod;
	}

	/**
	 * @param sciBenefitPeriod the sciBenefitPeriod to set
	 */
	public void setSciBenefitPeriod(String sciBenefitPeriod) {
		this.sciBenefitPeriod = sciBenefitPeriod;
	}

	/**
	 * @return the sciWaitPeriod
	 */
	public String getSciWaitPeriod() {
		return sciWaitPeriod;
	}

	/**
	 * @param sciWaitPeriod the sciWaitPeriod to set
	 */
	public void setSciWaitPeriod(String sciWaitPeriod) {
		this.sciWaitPeriod = sciWaitPeriod;
	}

	/**
	 * @return the accTotalDeathCover
	 */
	public String getAccTotalDeathCover() {
		return accTotalDeathCover;
	}

	/**
	 * @param accTotalDeathCover the accTotalDeathCover to set
	 */
	public void setAccTotalDeathCover(String accTotalDeathCover) {
		this.accTotalDeathCover = accTotalDeathCover;
	}

	/**
	 * @return the accTotalTPDCover
	 */
	public String getAccTotalTPDCover() {
		return accTotalTPDCover;
	}

	/**
	 * @param accTotalTPDCover the accTotalTPDCover to set
	 */
	public void setAccTotalTPDCover(String accTotalTPDCover) {
		this.accTotalTPDCover = accTotalTPDCover;
	}

	/**
	 * @return the accTotalTraumaCover
	 */
	public String getAccTotalTraumaCover() {
		return accTotalTraumaCover;
	}

	/**
	 * @param accTotalTraumaCover the accTotalTraumaCover to set
	 */
	public void setAccTotalTraumaCover(String accTotalTraumaCover) {
		this.accTotalTraumaCover = accTotalTraumaCover;
	}

	/**
	 * @return the accTotalSCICover
	 */
	public String getAccTotalSCICover() {
		return accTotalSCICover;
	}

	/**
	 * @param accTotalSCICover the accTotalSCICover to set
	 */
	public void setAccTotalSCICover(String accTotalSCICover) {
		this.accTotalSCICover = accTotalSCICover;
	}

	/**
	 * @return the finalBenefitPeriod
	 */
	public String getFinalBenefitPeriod() {
		return finalBenefitPeriod;
	}

	/**
	 * @param finalBenefitPeriod the finalBenefitPeriod to set
	 */
	public void setFinalBenefitPeriod(String finalBenefitPeriod) {
		this.finalBenefitPeriod = finalBenefitPeriod;
	}

	/**
	 * @return the finalWaitPeriod
	 */
	public String getFinalWaitPeriod() {
		return finalWaitPeriod;
	}

	/**
	 * @param finalWaitPeriod the finalWaitPeriod to set
	 */
	public void setFinalWaitPeriod(String finalWaitPeriod) {
		this.finalWaitPeriod = finalWaitPeriod;
	}

	/**
	 * @return the glAALDeathCover
	 */
	public String getGlAALDeathCover() {
		return glAALDeathCover;
	}

	/**
	 * @param glAALDeathCover the glAALDeathCover to set
	 */
	public void setGlAALDeathCover(String glAALDeathCover) {
		this.glAALDeathCover = glAALDeathCover;
	}

	/**
	 * @return the glAALTPDCover
	 */
	public String getGlAALTPDCover() {
		return glAALTPDCover;
	}

	/**
	 * @param glAALTPDCover the glAALTPDCover to set
	 */
	public void setGlAALTPDCover(String glAALTPDCover) {
		this.glAALTPDCover = glAALTPDCover;
	}

	/**
	 * @return the glAALTraumaCover
	 */
	public String getGlAALTraumaCover() {
		return glAALTraumaCover;
	}

	/**
	 * @param glAALTraumaCover the glAALTraumaCover to set
	 */
	public void setGlAALTraumaCover(String glAALTraumaCover) {
		this.glAALTraumaCover = glAALTraumaCover;
	}

	/**
	 * @return the sciAAL
	 */
	public String getSciAAL() {
		return sciAAL;
	}

	/**
	 * @param sciAAL the sciAAL to set
	 */
	public void setSciAAL(String sciAAL) {
		this.sciAAL = sciAAL;
	}

	/**
	 * @return the totDeathCoverDecison
	 */
	public String getTotDeathCoverDecison() {
		return totDeathCoverDecison;
	}

	/**
	 * @param totDeathCoverDecison the totDeathCoverDecison to set
	 */
	public void setTotDeathCoverDecison(String totDeathCoverDecison) {
		this.totDeathCoverDecison = totDeathCoverDecison;
	}

	/**
	 * @return the totTPDCoverDecison
	 */
	public String getTotTPDCoverDecison() {
		return totTPDCoverDecison;
	}

	/**
	 * @param totTPDCoverDecison the totTPDCoverDecison to set
	 */
	public void setTotTPDCoverDecison(String totTPDCoverDecison) {
		this.totTPDCoverDecison = totTPDCoverDecison;
	}

	/**
	 * @return the totTraCoverDecison
	 */
	public String getTotTraCoverDecison() {
		return totTraCoverDecison;
	}

	/**
	 * @param totTraCoverDecison the totTraCoverDecison to set
	 */
	public void setTotTraCoverDecison(String totTraCoverDecison) {
		this.totTraCoverDecison = totTraCoverDecison;
	}

	/**
	 * @return the totSCICoverDecison
	 */
	public String getTotSCICoverDecison() {
		return totSCICoverDecison;
	}

	/**
	 * @param totSCICoverDecison the totSCICoverDecison to set
	 */
	public void setTotSCICoverDecison(String totSCICoverDecison) {
		this.totSCICoverDecison = totSCICoverDecison;
	}

	/**
	 * @return the totDeathCoverLoding
	 */
	public String getTotDeathCoverLoding() {
		return totDeathCoverLoding;
	}

	/**
	 * @param totDeathCoverLoding the totDeathCoverLoding to set
	 */
	public void setTotDeathCoverLoding(String totDeathCoverLoding) {
		this.totDeathCoverLoding = totDeathCoverLoding;
	}

	/**
	 * @return the totTPDCoverLoding
	 */
	public String getTotTPDCoverLoding() {
		return totTPDCoverLoding;
	}

	/**
	 * @param totTPDCoverLoding the totTPDCoverLoding to set
	 */
	public void setTotTPDCoverLoding(String totTPDCoverLoding) {
		this.totTPDCoverLoding = totTPDCoverLoding;
	}

	/**
	 * @return the totTraCoverLoding
	 */
	public String getTotTraCoverLoding() {
		return totTraCoverLoding;
	}

	/**
	 * @param totTraCoverLoding the totTraCoverLoding to set
	 */
	public void setTotTraCoverLoding(String totTraCoverLoding) {
		this.totTraCoverLoding = totTraCoverLoding;
	}

	/**
	 * @return the totSCICoverLoding
	 */
	public String getTotSCICoverLoding() {
		return totSCICoverLoding;
	}

	/**
	 * @param totSCICoverLoding the totSCICoverLoding to set
	 */
	public void setTotSCICoverLoding(String totSCICoverLoding) {
		this.totSCICoverLoding = totSCICoverLoding;
	}

	/**
	 * @return the totDeathCoverExc
	 */
	public String getTotDeathCoverExc() {
		return totDeathCoverExc;
	}

	/**
	 * @param totDeathCoverExc the totDeathCoverExc to set
	 */
	public void setTotDeathCoverExc(String totDeathCoverExc) {
		this.totDeathCoverExc = totDeathCoverExc;
	}

	/**
	 * @return the glDeathFwdCover
	 */
	public String getGlDeathFwdCover() {
		return glDeathFwdCover;
	}

	/**
	 * @param glDeathFwdCover the glDeathFwdCover to set
	 */
	public void setGlDeathFwdCover(String glDeathFwdCover) {
		this.glDeathFwdCover = glDeathFwdCover;
	}

	/**
	 * @return the glTPDFwdCover
	 */
	public String getGlTPDFwdCover() {
		return glTPDFwdCover;
	}

	/**
	 * @param glTPDFwdCover the glTPDFwdCover to set
	 */
	public void setGlTPDFwdCover(String glTPDFwdCover) {
		this.glTPDFwdCover = glTPDFwdCover;
	}

	/**
	 * @return the glTraFwdCover
	 */
	public String getGlTraFwdCover() {
		return glTraFwdCover;
	}

	/**
	 * @param glTraFwdCover the glTraFwdCover to set
	 */
	public void setGlTraFwdCover(String glTraFwdCover) {
		this.glTraFwdCover = glTraFwdCover;
	}

	/**
	 * @return the sciFwdCover
	 */
	public String getSciFwdCover() {
		return sciFwdCover;
	}

	/**
	 * @param sciFwdCover the sciFwdCover to set
	 */
	public void setSciFwdCover(String sciFwdCover) {
		this.sciFwdCover = sciFwdCover;
	}

	/**
	 * @return the glPolicyNo
	 */
	public String getGlPolicyNo() {
		return glPolicyNo;
	}

	/**
	 * @param glPolicyNo the glPolicyNo to set
	 */
	public void setGlPolicyNo(String glPolicyNo) {
		this.glPolicyNo = glPolicyNo;
	}

	/**
	 * @return the sciPolicyNo
	 */
	public String getSciPolicyNo() {
		return sciPolicyNo;
	}

	/**
	 * @param sciPolicyNo the sciPolicyNo to set
	 */
	public void setSciPolicyNo(String sciPolicyNo) {
		this.sciPolicyNo = sciPolicyNo;
	}

	/**
	 * @return the requirements
	 */
	public String getRequirements() {
		return requirements;
	}

	/**
	 * @param requirements the requirements to set
	 */
	public void setRequirements(String requirements) {
		this.requirements = requirements;
	}

	/**
	 * @return the commMedium
	 */
	public String getCommMedium() {
		return commMedium;
	}

	/**
	 * @param commMedium the commMedium to set
	 */
	public void setCommMedium(String commMedium) {
		this.commMedium = commMedium;
	}

	/**
	 * @return the endorsementId
	 */
	public Long getEndorsementId() {
		return endorsementId;
	}

	/**
	 * @param endorsementId the endorsementId to set
	 */
	public void setEndorsementId(Long endorsementId) {
		this.endorsementId = endorsementId;
	}

	/**
	 * @return the statusCode
	 */
	public String getStatusCode() {
		return statusCode;
	}

	/**
	 * @param statusCode the statusCode to set
	 */
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	/**
	 * @return the extTraumaGroupCover
	 */
	public String getExtTraumaGroupCover() {
		return extTraumaGroupCover;
	}

	/**
	 * @param extTraumaGroupCover the extTraumaGroupCover to set
	 */
	public void setExtTraumaGroupCover(String extTraumaGroupCover) {
		this.extTraumaGroupCover = extTraumaGroupCover;
	}

	/**
	 * @return the underwritingAction
	 */
	public Set<UnderwritingAction> getUnderwritingAction() {
		return underwritingAction;
	}

	/**
	 * @param underwritingAction the underwritingAction to set
	 */
	public void setUnderwritingAction(Set<UnderwritingAction> underwritingAction) {
		this.underwritingAction = underwritingAction;
	}
	
	
	public String getUwName() {
		return uwName;
	}

	public void setUwName(String uwName) {
		this.uwName = uwName;
	}

	public String getRequestKind() {
		return requestKind;
	}

	public void setRequestKind(String requestKind) {
		this.requestKind = requestKind;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UnderwritingDetail [applicationNo=" + applicationNo + ", fundName=" + fundName + ", fundCode="
				+ fundCode + ", extMemberNo=" + clientReferenceNo + ", firstName=" + firstName + ", surName=" + surName
				+ ", memberDob=" + memberDob + ", memberGender=" + memberGender + ", memberType=" + memberType
				+ ", applicationDate=" + applicationDate + ", status=" + status + ", source=" + source
				+ ", totalDeathCover=" + totalDeathCover + ", totalTraumaCover=" + totalTraumaCover + ", totalTPDCover="
				+ totalTPDCover + ", totalSCICover=" + totalSCICover + ", requestWaitPeriod=" + requestWaitPeriod
				+ ", requestBenefitPeriod=" + requestBenefitPeriod + ", extDeathGroupCover=" + extDeathGroupCover
				+ ", extTPDGroupCover=" + extTPDGroupCover + ", extSCICover=" + extSCICover + ", lastActionDate="
				+ lastActionDate + ", sciBenefitPeriod=" + sciBenefitPeriod + ", sciWaitPeriod=" + sciWaitPeriod
				+ ", accTotalDeathCover=" + accTotalDeathCover + ", accTotalTPDCover=" + accTotalTPDCover
				+ ", accTotalTraumaCover=" + accTotalTraumaCover + ", accTotalSCICover=" + accTotalSCICover
				+ ", finalBenefitPeriod=" + finalBenefitPeriod + ", finalWaitPeriod=" + finalWaitPeriod
				+ ", glAALDeathCover=" + glAALDeathCover + ", glAALTPDCover=" + glAALTPDCover + ", glAALTraumaCover="
				+ glAALTraumaCover + ", sciAAL=" + sciAAL + ", totDeathCoverDecison=" + totDeathCoverDecison
				+ ", totTPDCoverDecison=" + totTPDCoverDecison + ", totTraCoverDecison=" + totTraCoverDecison
				+ ", totSCICoverDecison=" + totSCICoverDecison + ", totDeathCoverLoding=" + totDeathCoverLoding
				+ ", totTPDCoverLoding=" + totTPDCoverLoding + ", totTraCoverLoding=" + totTraCoverLoding
				+ ", totSCICoverLoding=" + totSCICoverLoding + ", totDeathCoverExc=" + totDeathCoverExc
				+ ", glDeathFwdCover=" + glDeathFwdCover + ", glTPDFwdCover=" + glTPDFwdCover + ", glTraFwdCover="
				+ glTraFwdCover + ", sciFwdCover=" + sciFwdCover + ", glPolicyNo=" + glPolicyNo + ", sciPolicyNo="
				+ sciPolicyNo + ", requirements=" + requirements + "]";
	}

}
