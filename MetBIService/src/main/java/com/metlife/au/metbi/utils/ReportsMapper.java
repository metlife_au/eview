/**
 * 
 */
package com.metlife.au.metbi.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.Conditions;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.metlife.au.metbi.dto.FundDto;
import com.metlife.au.metbi.dto.UWDrillDownApplicationInfo;
import com.metlife.au.metbi.dto.UWRequirementReportDto;
import com.metlife.au.metbi.dto.UWSLAReportDto;
import com.metlife.au.metbi.dto.UWSearchResult;
import com.metlife.au.metbi.dto.UWStatusReportDto;
import com.metlife.au.metbi.dto.UnderwritingActionDto;
import com.metlife.au.metbi.dto.UnderwritingDetailDto;
import com.metlife.au.metbi.entity.ClaimDetails;
import com.metlife.au.metbi.entity.Fund;
import com.metlife.au.metbi.entity.UnderwritingAction;
import com.metlife.au.metbi.entity.UnderwritingDetail;
import com.metlife.au.metbi.exception.ApplicationException;

/**
 * @author 672381
 *
 */
@Component
public class ReportsMapper {

	private final Logger logger = LoggerFactory.getLogger(ReportsMapper.class.getName());

	private ModelMapper modelMapper;

	/**
	 * 
	 */
	public ReportsMapper() {
		modelMapper = new ModelMapper();
		modelMapper.getConfiguration().setPropertyCondition(Conditions.isNotNull());
	}

	public List<UWStatusReportDto> uwStatusConversion(List<UnderwritingDetail> reportDetails)
			throws ApplicationException {
		logger.info("Inside the uwStatusConversion method from ReportsMapper");
		List<UWStatusReportDto> reportDetailsDTO = new ArrayList<UWStatusReportDto>();
		try {
			reportDetailsDTO = reportDetails.stream().map(detail -> {
				try {
					return convertuwEntityToStatusDto(detail);
				} catch (Exception e) {
					try {
						throw new ApplicationException("Exception in Mapper module");
					} catch (ApplicationException e1) {

					}
				}
				return null;

			}).collect(Collectors.toList());
		} catch (Exception e) {
			logger.info("In uwStatusConversion method catch block from ReportsMapper" + e.getMessage());
			throw new ApplicationException("Exception in Mapper module");
		}
		logger.info("end of the uwStatusConversion method from ReportsMapper");
		return reportDetailsDTO;
	}

	private UWStatusReportDto convertuwEntityToStatusDto(UnderwritingDetail underwritingDetail)
			throws ApplicationException {
		logger.info("Inside the convertuwEntityToStatusDto method from ReportsMapper");
		UWStatusReportDto uwSatusDto = new UWStatusReportDto();
		try {
			uwSatusDto = modelMapper.map(underwritingDetail, UWStatusReportDto.class);

		} catch (Exception e) {
			logger.info("In convertuwEntityToStatusDto method catch block from ReportsMapper" + e.getMessage());
			throw new ApplicationException("Exception in Mapper module");
		}
		logger.info("end of the convertuwEntityToStatusDto method from ReportsMapper");
		return uwSatusDto;
	}

	public List<UWSLAReportDto> uwSLAConversion(List<UnderwritingDetail> reportDetails) throws ApplicationException {
		logger.info("Inside the uwSLAConversion method from ReportsMapper");
		List<UWSLAReportDto> reportDetailsDTO = new ArrayList<UWSLAReportDto>();
		try {
			reportDetailsDTO = reportDetails.stream().map(detail -> {
				try {
					return convertuwEntityToSLADto(detail);
				} catch (Exception e) {
					try {
						throw new ApplicationException("Exception in Mapper module");
					} catch (ApplicationException e1) {

					}
				}
				return null;

			}).collect(Collectors.toList());
		} catch (Exception e) {
			logger.info("In uwSLAConversion method catch block from ReportsMapper" + e.getMessage());
			throw new ApplicationException("Exception in Mapper module");
		}
		logger.info("end of the uwSLAConversion method from ReportsMapper");
		return reportDetailsDTO;
	}

	private UWSLAReportDto convertuwEntityToSLADto(UnderwritingDetail underwritingDetail) throws ApplicationException {
		logger.info("Inside the convertuwEntityToSLADto method from ReportsMapper");
		UWSLAReportDto uwSLADetails = new UWSLAReportDto();
		try {
			uwSLADetails = modelMapper.map(underwritingDetail, UWSLAReportDto.class);
		} catch (Exception e) {
			logger.info("In convertuwEntityToSLADto method catch block from ReportsMapper" + e.getMessage());
			throw new ApplicationException("Exception in Mapper module");
		}
		logger.info("end of the convertuwEntityToSLADto method from ReportsMapper");
		return uwSLADetails;
	}

	public List<UWRequirementReportDto> uwRequirementConversion(List<UnderwritingDetail> reportDetails)
			throws ApplicationException {
		logger.info("Inside the uwRequirementConversion method from ReportsMapper");
		List<UWRequirementReportDto> reportDetailsDTO = new ArrayList<UWRequirementReportDto>();
		try {
			reportDetailsDTO = reportDetails.stream().map(detail -> {
				try {
					return convertuwEntityToRequirementDto(detail);
				} catch (Exception e) {
					try {
						throw new ApplicationException("Exception in Mapper module");
					} catch (ApplicationException e1) {

					}
				}
				return null;

			}).collect(Collectors.toList());
		} catch (Exception e) {
			logger.info("In uwRequirementConversion method catch block from ReportsMapper" + e.getMessage());
			throw new ApplicationException("Exception in Mapper module");
		}
		logger.info("end of the uwRequirementConversion method from ReportsMapper");
		return reportDetailsDTO;
	}

	private UWRequirementReportDto convertuwEntityToRequirementDto(UnderwritingDetail underwritingDetail)
			throws ApplicationException {
		logger.info("Inside the convertuwEntityToRequirementDto method from ReportsMapper");
		UWRequirementReportDto uwReqDetails = new UWRequirementReportDto();
		try {
			uwReqDetails = modelMapper.map(underwritingDetail, UWRequirementReportDto.class);
		} catch (Exception e) {
			logger.info("In convertuwEntityToRequirementDto method catch block from ReportsMapper" + e.getMessage());
			throw new ApplicationException("Exception in Mapper module");
		}
		logger.info("end of the convertuwEntityToRequirementDto method from ReportsMapper");
		return uwReqDetails;
	}

	public List<FundDto> fundConversion(List<Fund> funds)
			throws ApplicationException {
		logger.info("Inside the fundConversion method from ReportsMapper");
		List<FundDto> fundDtoList = new ArrayList<FundDto>();
		try {
			fundDtoList = funds.stream().map(detail -> {
				try {
					return fundEntitytoDto(detail);
				} catch (ApplicationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return null;
			}).collect(Collectors.toList());
		} catch (Exception e) {
			logger.info("In fundConversion method catch block from ReportsMapper" + e.getMessage());
			throw new ApplicationException("Exception in Mapper module");
		}
		logger.info("end of the fundConversion method from ReportsMapper");
		return fundDtoList;
	}

	private FundDto fundEntitytoDto(Fund fund)
			throws ApplicationException {
		logger.info("Inside the fundEntitytoDto method from ReportsMapper");
		FundDto fundDto = new FundDto();
		try {
			fundDto = modelMapper.map(fund, FundDto.class);

		} catch (Exception e) {
			logger.info("In fundEntitytoDto method catch block from ReportsMapper" + e.getMessage());
			throw new ApplicationException("Exception in Mapper module");
		}
		logger.info("end of the fundEntitytoDto method from ReportsMapper");
		return fundDto;
	}
	
	public UnderwritingActionDto uwActionmapper(UnderwritingAction eventHistoryDetails) throws ApplicationException {
		logger.info("Inside the convertuwEntityToRequirementDto method from ReportsMapper");
		UnderwritingActionDto uwReqDetails = new UnderwritingActionDto();
		try {
			uwReqDetails = modelMapper.map(eventHistoryDetails, UnderwritingActionDto.class);
		} catch (Exception e) {
			logger.info("In convertuwEntityToRequirementDto method catch block from ReportsMapper" + e.getMessage());
			throw new ApplicationException("Exception in Mapper module");
		}
		logger.info("end of the convertuwEntityToRequirementDto method from ReportsMapper");
		return uwReqDetails;
	}

	public UWDrillDownApplicationInfo uwApplicationInfo(UnderwritingDetail reportDetails) throws ApplicationException {
		logger.info("Inside the convertuwEntityToRequirementDto method from ReportsMapper");
		UWDrillDownApplicationInfo uwReqDetails = new UWDrillDownApplicationInfo();
		try {
			uwReqDetails = modelMapper.map(reportDetails, UWDrillDownApplicationInfo.class);
		} catch (Exception e) {
			logger.info("In convertuwEntityToRequirementDto method catch block from ReportsMapper" + e.getMessage());
			throw new ApplicationException("Exception in Mapper module");
		}
		logger.info("end of the convertuwEntityToRequirementDto method from ReportsMapper");
		return uwReqDetails;
	}

}
