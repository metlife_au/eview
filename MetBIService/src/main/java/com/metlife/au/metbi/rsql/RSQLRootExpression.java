package com.metlife.au.metbi.rsql;

import java.util.List;

public class RSQLRootExpression {
	
	private List<RSQLParameter> parameters;
	
	private String rootOperator;

	public String getRootOperator() {
		return rootOperator;
	}

	public void setRootOperator(String rootOperator) {
		this.rootOperator = rootOperator;
	}

	public List<RSQLParameter> getParameters() {
		return parameters;
	}

	public void setParameters(List<RSQLParameter> parameters) {
		this.parameters = parameters;
	}
	
	

}
