/**
 * 
 */
package com.metlife.au.metbi.dto;

import java.util.Set;

/**
 * @author 672381
 *
 */
public class UWSLAReportDto extends UnderwritingDetailDto {

	private long applicationNo;
	private String fundName;
	private String fundCode;
	private String clientReferenceNo = null;
	private String firstName;
	private String surName;
	private String memberDob;
	private String memberGender;
	private String memberType;
	private String applicationDate;
	private String status;
	private String source;
	private String lastActionDate;
	private Set<UnderwritingActionDto> underwritingAction;

	public UWSLAReportDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public long getApplicationNo() {
		return applicationNo;
	}

	public void setApplicationNo(long applicationNo) {
		this.applicationNo = applicationNo;
	}

	public String getFundName() {
		return fundName;
	}

	public void setFundName(String fundName) {
		this.fundName = fundName;
	}

	public String getFundCode() {
		return fundCode;
	}

	public void setFundCode(String fundCode) {
		this.fundCode = fundCode;
	}

	public String getClientReferenceNo() {
		return clientReferenceNo;
	}

	public void setClientReferenceNo(String clientReferenceNo) {
		this.clientReferenceNo = clientReferenceNo;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getSurName() {
		return surName;
	}

	public void setSurName(String surName) {
		this.surName = surName;
	}

	public String getMemberDob() {
		return memberDob;
	}

	public void setMemberDob(String memberDob) {
		this.memberDob = memberDob;
	}

	public String getMemberGender() {
		return memberGender;
	}

	public void setMemberGender(String memberGender) {
		this.memberGender = memberGender;
	}

	public String getMemberType() {
		return memberType;
	}

	public void setMemberType(String memberType) {
		this.memberType = memberType;
	}

	public String getApplicationDate() {
		return applicationDate;
	}

	public void setApplicationDate(String applicationDate) {
		this.applicationDate = applicationDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getLastActionDate() {
		return lastActionDate;
	}

	public void setLastActionDate(String lastActionDate) {
		this.lastActionDate = lastActionDate;
	}

	public Set<UnderwritingActionDto> getUnderwritingAction() {
		return underwritingAction;
	}

	public void setUnderwritingAction(Set<UnderwritingActionDto> underwritingAction) {
		this.underwritingAction = underwritingAction;
	}

}
