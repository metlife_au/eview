package com.metlife.au.metbi.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "EVIEW_ACTIVITY", schema = "DBUSDWW")
public class EviewActivity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6913750328462155724L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ACTIVITY_TYPE_ID")
	private Integer activityTypeId;

	@Column(name = "ACTIVITY_DESCRIPTION")
	private String description;

	public EviewActivity() {

	}

	/**
	 * @return the activityTypeId
	 */
	public Integer getActivityTypeId() {
		return activityTypeId;
	}

	/**
	 * @param activityTypeId the activityTypeId to set
	 */
	public void setActivityTypeId(Integer activityTypeId) {
		this.activityTypeId = activityTypeId;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

}
