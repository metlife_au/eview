package com.metlife.au.metbi.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "FCT_CLM_PAYMENTS", schema = "DBUSDWW")
public class ClaimPayment implements Serializable {

	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private ClaimPaymentEmbedded claimPaymentEmbedded;

	@Column(name = "CHEQUENO")
	private String chequeNo;

	@Column(name = "DISPATCH_DATE")
	private Date dispatchDate;

	@Column(name = "SOURCE_SYSTEM")
	private Long sourceSystem;

	@Column(name = "FROM_DATE")
	private Date fromDate;

	@Column(name = "TO_DATE")
	private Date toDate;

	@Column(name = "AMT")
	private BigDecimal amount;

	@OneToMany(mappedBy = "claimPayment")
	private Set<ClaimDetails> claimDetails;

	/**
	 * @return the dispatchDate
	 */
	public Date getDispatchDate() {
		return dispatchDate;
	}

	/**
	 * @param dispatchDate the dispatchDate to set
	 */
	public void setDispatchDate(Date dispatchDate) {
		this.dispatchDate = dispatchDate;
	}

	/**
	 * @return the sourceSystem
	 */
	public Long getSourceSystem() {
		return sourceSystem;
	}

	/**
	 * @param sourceSystem the sourceSystem to set
	 */
	public void setSourceSystem(Long sourceSystem) {
		this.sourceSystem = sourceSystem;
	}

	/**
	 * @return the fromDate
	 */
	public Date getFromDate() {
		return fromDate;
	}

	/**
	 * @param fromDate the fromDate to set
	 */
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	/**
	 * @return the toDate
	 */
	public Date getToDate() {
		return toDate;
	}

	/**
	 * @param toDate the toDate to set
	 */
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	/**
	 * @return the amount
	 */
	public BigDecimal getAmount() {
		return amount;
	}

	/**
	 * @param amount the amount to set
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	/**
	 * @return the chequeNo
	 */
	public String getChequeNo() {
		return chequeNo;
	}

	/**
	 * @param chequeNo the chequeNo to set
	 */
	public void setChequeNo(String chequeNo) {
		this.chequeNo = chequeNo;
	}

	/**
	 * @return the claimPaymentEmbedded
	 */
	public ClaimPaymentEmbedded getClaimPaymentEmbedded() {
		return claimPaymentEmbedded;
	}

	/**
	 * @param claimPaymentEmbedded the claimPaymentEmbedded to set
	 */
	public void setClaimPaymentEmbedded(ClaimPaymentEmbedded claimPaymentEmbedded) {
		this.claimPaymentEmbedded = claimPaymentEmbedded;
	}

	/**
	 * @return the claimDetails
	 */
	public Set<ClaimDetails> getClaimDetails() {
		return claimDetails;
	}

	/**
	 * @param claimDetails the claimDetails to set
	 */
	public void setClaimDetails(Set<ClaimDetails> claimDetails) {
		this.claimDetails = claimDetails;
	}

}
