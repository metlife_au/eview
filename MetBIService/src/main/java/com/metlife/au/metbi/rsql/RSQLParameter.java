package com.metlife.au.metbi.rsql;

import java.util.List;

public class RSQLParameter {
	
	private String parameterName;
	
	private String parameterExpression;
	
	private String parameterSingleValue;
	
	private List<String> parameterMultiValues;
	
	private String operator;

	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	public String getParameterName() {
		return parameterName;
	}

	public void setParameterName(String parameterName) {
		this.parameterName = parameterName;
	}

	public String getParameterExpression() {
		return parameterExpression;
	}

	public void setParameterExpression(String parameterExpression) {
		this.parameterExpression = parameterExpression;
	}

	public String getParameterSingleValue() {
		return parameterSingleValue;
	}

	public void setParameterSingleValue(String parameterSingleValue) {
		this.parameterSingleValue = parameterSingleValue;
	}

	public List<String> getParameterMultiValues() {
		return parameterMultiValues;
	}

	public void setParameterMultiValues(List<String> parameterMultiValues) {
		this.parameterMultiValues = parameterMultiValues;
	}
	
	

}
